#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Nov 15 10:31:51 2017


"""


def percent_below(y, l1, l2, threshold):
    if threshold == 1:
        #        threshold = (l2 - l1) / 2 + l1
        threshold = [l1, (l2 - l1) / 2 + l1, l2]
    visiblepercent = []
    for t in threshold:
        outofwater = y[y < t]
        visiblepercent.append((outofwater.sum() - l1 * outofwater.size) / y.sum() * 100)
    return visiblepercent, threshold
