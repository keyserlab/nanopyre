#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Sep 25 08:22:02 2017



Python peakfinder based on Nathanael Yoder's peakfinder.m
(http://uk.mathworks.com/matlabcentral/fileexchange/25500-peakfinder-x0--sel--thresh--extrema--includeendpoints--interpolate-)
"""

import numpy as np
from math import ceil
from scipy.optimize import curve_fit


def gauss(x, mean, amplitude, width):
    y = amplitude * np.exp(-np.square(x - mean) / 2.0 / width / width)
    return y


def peakfinder(
    x,
    y,
    sel,
    threshold,
    extrema,
    maxhrztlinddistance,
    maxhrztldistance,
    maxvertdistance,
):
    #    small = np.finfo(np.float).eps
    small = np.ptp(y) * 0.01
    y = y * extrema
    # make first and last extrema minima
    y = np.concatenate([np.array([y[0] + small]), y, np.array([y[-1] + small])])
    x = np.concatenate([np.array([0]), x, np.array([0])])
    dy = np.diff(y)
    dy[abs(dy) < small * 1e-4] = -small
    ind = np.nonzero(dy[0:-1] * dy[1:] < 0)[0] + 1
    y_extr = y[ind]
    min_mag = y_extr.min()
    left_min_ind = 0
    left_min = y_extr[0]
    leng = y_extr.size
    temp_mag_ind = 1e9
    temp_mag = min_mag
    peak_loc = np.zeros((3, ceil(leng / 2)), dtype=int)
    peak_ind = 0
    temp_loc = 0
    #    print('------------------------------------------')
    for ii in range(1, leng - 1, 2):
        #        print('---------')
        #        print(ii)
        #        print(ind[ii]-1)
        #        print(y_extr[ii])
        #        print(left_min + sel)
        # looking at a maximum
        # this prevents that a high temp_mag stops peakfinding later in the array
        if (ii - temp_mag_ind) > 10:
            temp_mag_ind = 1e9
            temp_mag = min_mag
        #            print('Temp mag correction: ' + str(ind[ii]-1))
        # trigger up if peak above threshold and larger than the last one (temp_mag)
        if (y_extr[ii] > left_min + sel) and (y_extr[ii] > temp_mag):
            temp_loc = ii
            temp_mag_ind = ii
            temp_mag = y_extr[ii]
            left_valley_ind = left_min_ind
        #            print('Up trigger: ' + str(x[ind[ii]]))
        # moving on to the next minimum
        ii += 1
        if (y_extr[ii] < temp_mag - sel) and (
            x[ind[ii]] - x[ind[temp_loc]] <= maxhrztldistance
        ):
            #            print('Down trigger: ' + str(x[ind[ii]]))
            peak_loc[0][peak_ind] = temp_loc
            peak_loc[1][peak_ind] = left_valley_ind
            peak_loc[2][peak_ind] = ii
            peak_ind += 1
            left_min_ind = ii
            left_min = y_extr[ii]
            temp_mag = min_mag
        elif y_extr[ii] < left_min:
            left_min_ind = ii
            left_min = y_extr[ii]
            left_valley_ind = left_min_ind
        #            print('Smaller left_min_ind :', str(x[ind[left_min_ind]]))
        elif (x[ind[ii]] - x[ind[left_min_ind]] > maxhrztldistance) or (
            ii - left_min_ind > maxhrztlinddistance
        ):
            left_min_ind += 2
            left_min = y_extr[left_min_ind]
            left_valley_ind = left_min_ind
    #            print('Dragged left_min_ind :', str(x[ind[left_min_ind]]))
    #    print('------------------------------------------')
    peak_loc = peak_loc[:, :peak_ind]
    peak_loc = peak_loc[
        :, (abs(y_extr[peak_loc[2, :]] - y_extr[peak_loc[1, :]]) < maxvertdistance)
    ]
    peakloc = peak_loc[0]
    peakinds = ind[peak_loc[0]]
    left_valleyinds = ind[peak_loc[1]]
    right_valleyinds = ind[peak_loc[2]]
    magnitudes = y[peakinds]
    if threshold:
        threshold = threshold * extrema
        choice = magnitudes > threshold
        peakinds = peakinds[choice]
        magnitudes = magnitudes[choice]
        peakloc = peakloc[choice]
    magnitudes = magnitudes * extrema
    peakinds -= 1
    left_valleyinds -= 1
    right_valleyinds -= 1
    ind -= 1
    return peakinds, left_valleyinds, right_valleyinds, magnitudes, ind, peakloc


def peakmask(
    func, x, y, peakinds, ind, peakloc, masksize, extrema, correct_baseline=True
):
    masksize = int(masksize)
    leftlist = []
    rightlist = []
    peakparams = []
    peakdata = []
    blines = []
    for pl in peakloc:
        left = np.arange(pl - 1, max(0, pl - 1 - masksize * 2), -2)
        left = ind[left]
        if left.size == 0:
            left = np.array([0])
        left = left[np.argmin(y[left] * extrema)]
        leftlist.append(left)
        right = np.arange(pl + 1, min(ind.size, pl + 1 + masksize * 2), 2)
        if right.size == 0:
            right = np.array([0])
        right = ind[right]
        right = right[np.argmin(y[right] * extrema)]
        rightlist.append(right)
    for i in range(len(leftlist) - 1):
        if rightlist[i] > leftlist[i + 1]:
            #            rightlist[i] = leftlist[i+1] = int((rightlist[i]-leftlist[i+1])/2+leftlist[i+1])
            overlap = ind[
                (ind >= max(leftlist[i + 1], ind[peakloc[i] + 1]))
                & (ind <= min(rightlist[i], ind[peakloc[i + 1] - 1]))
            ][::2]
            rightlist[i] = leftlist[i + 1] = overlap[np.argmin(y[overlap] * extrema)]
    for i in range(len(leftlist)):
        x_cut = x[leftlist[i] : rightlist[i] + 1].copy()
        y_cut = y[leftlist[i] : rightlist[i] + 1].copy()
        if extrema == 1:
            baseline = min(y_cut)
            ampguess = y_cut.max()
        elif extrema == -1:
            baseline = max(y_cut[0], y_cut[-1])
            ampguess = y_cut.min()
        blines.append(baseline)
        if correct_baseline:
            y_cut -= baseline
        #        p0 = [x[ind[peakloc[i]]], ampguess, np.ptp(x_cut)]
        #        try:
        #            params, _ = curve_fit(func, x_cut, y_cut, p0=p0)
        #        except:
        #            params = [-1, -1, -1]
        params = [-1, -1, -1]
        params[2] = abs(params[2])
        peakparams.append(params)
        data = np.zeros((3, x_cut.size))
        data[0, :] = x_cut
        data[2, :] = func(x_cut, *params)
        data[1, :] = data[2, :] + baseline * correct_baseline
        peakdata.append(data)
    return peakparams, peakdata, leftlist, rightlist, blines


def peakfinder_old(y, sel, threshold, extrema, maxinddistance, maxvertdistance):
    #    small = np.finfo(np.float).eps
    small = np.ptp(y) * 0.01
    y = y * extrema
    # make first and last extrema minima
    y = np.insert(y, 0, y[0] + small)
    y = np.append(y, y[-1] + small)
    dy = np.diff(y)
    dy[abs(dy) < small * 1e-4] = -small
    ind = np.nonzero(dy[0:-1] * dy[1:] < 0)[0] + 1
    y_extr = y[ind]
    min_mag = y_extr.min()
    left_min_ind = 0
    left_min = y_extr[0]
    leng = y_extr.size
    temp_mag_ind = 1e9
    temp_mag = min_mag
    peak_loc = np.zeros((3, ceil(leng / 2)), dtype=int)
    peak_ind = 0
    temp_loc = 0
    for ii in range(1, leng - 1, 2):
        #        print('---------')
        #        print(ii)
        #        print(ind[ii]-1)
        #        print(y_extr[ii])
        #        print(left_min + sel)
        # looking at a maximum
        # this prevents that a high temp_mag stops peakfinding later in the array
        if (ii - temp_mag_ind) > 10:
            temp_mag_ind = 1e9
            temp_mag = min_mag
        #            print('Temp mag correction: ' + str(ind[ii]-1))
        # trigger up if peak above threshold and larger than the last one (temp_mag)
        if (y_extr[ii] > left_min + sel) and (y_extr[ii] > temp_mag):
            temp_loc = ii
            temp_mag_ind = ii
            temp_mag = y_extr[ii]
            left_valley_ind = left_min_ind
        #            print('Up trigger: ' + str(ind[ii]-1))
        # moving on to the next minimum
        ii += 1
        if (y_extr[ii] < temp_mag - sel) and (ii - temp_loc <= maxinddistance):
            #            print('Down trigger: ' + str(ind[ii]-1))
            peak_loc[0][peak_ind] = temp_loc
            peak_loc[1][peak_ind] = left_valley_ind
            peak_loc[2][peak_ind] = ii
            peak_ind += 1
            left_min_ind = ii
            left_min = y_extr[ii]
            temp_mag = min_mag
        elif y_extr[ii] < left_min:
            left_min_ind = ii
            left_min = y_extr[ii]
        #            print('Smaller left_min_ind :', str(ind[left_min_ind]-1))
        elif ii - left_min_ind > maxinddistance:
            left_min_ind += 2
            left_min = y_extr[left_min_ind]
    #            print('Dragged left_min_ind :', str(ind[left_min_ind]-1))
    #    print('------------------------------------------')
    peak_loc = peak_loc[:, :peak_ind]
    peak_loc = peak_loc[
        :, (abs(y_extr[peak_loc[2, :]] - y_extr[peak_loc[1, :]]) < maxvertdistance)
    ]
    peakloc = peak_loc[0]
    peakinds = ind[peak_loc[0]]
    left_valleyinds = ind[peak_loc[1]]
    right_valleyinds = ind[peak_loc[2]]
    magnitudes = y[peakinds]
    if threshold:
        threshold = threshold * extrema
        choice = magnitudes > threshold
        peakinds = peakinds[choice]
        magnitudes = magnitudes[choice]
        peakloc = peakloc[choice]
    magnitudes = magnitudes * extrema
    peakinds -= 1
    left_valleyinds -= 1
    right_valleyinds -= 1
    ind -= 1
    return peakinds, left_valleyinds, right_valleyinds, magnitudes, ind, peakloc


def peakmask_old(
    func, x, y, peakinds, ind, peakloc, masksize, extrema, correct_baseline=True
):
    masksize = int(masksize)
    leftlist = []
    rightlist = []
    peakparams = []
    peakdata = []
    blines = []
    for pl in peakloc:
        left = np.arange(pl - 1, max(0, pl - 1 - masksize * 2), -2)
        left = ind[left]
        if left.size == 0:
            left = np.array([0])
        left = left[np.argmin(y[left] * extrema)]
        leftlist.append(left)
        right = np.arange(pl + 1, min(ind.size, pl + 1 + masksize * 2), 2)
        if right.size == 0:
            right = np.array([0])
        right = ind[right]
        right = right[np.argmin(y[right] * extrema)]
        rightlist.append(right)
    for i in range(len(leftlist) - 1):
        if rightlist[i] > leftlist[i + 1]:
            #            rightlist[i] = leftlist[i+1] = int((rightlist[i]-leftlist[i+1])/2+leftlist[i+1])
            overlap = ind[
                (ind >= max(leftlist[i + 1], ind[peakloc[i] + 1]))
                & (ind <= min(rightlist[i], ind[peakloc[i + 1] - 1]))
            ][::2]
            rightlist[i] = leftlist[i + 1] = overlap[np.argmin(y[overlap] * extrema)]
    for i in range(len(leftlist)):
        x_cut = x[leftlist[i] : rightlist[i] + 1].copy()
        y_cut = y[leftlist[i] : rightlist[i] + 1].copy()
        if extrema == 1:
            baseline = min(y_cut)
            ampguess = y_cut.max()
        elif extrema == -1:
            baseline = max(y_cut)
            ampguess = y_cut.min()
        blines.append(baseline)
        if correct_baseline:
            y_cut -= baseline
        p0 = [x[ind[peakloc[i]]], ampguess, np.ptp(x_cut)]
        params, _ = curve_fit(func, x_cut, y_cut, p0=p0)
        params[2] = abs(params[2])
        peakparams.append(params)
        data = np.zeros((3, x_cut.size))
        data[0, :] = x_cut
        data[2, :] = func(x_cut, *params)
        data[1, :] = data[2, :] + baseline * correct_baseline
        peakdata.append(data)
    return peakparams, peakdata, leftlist, rightlist, blines
