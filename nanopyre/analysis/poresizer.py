#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Oct 22 14:00:16 2017


"""

import numpy as np
from scipy.interpolate import interp1d

import pkg_resources


class SizerError(Exception):
    pass


def conductivity(salt):
    path = pkg_resources.resource_filename("nanopyre", "analysis/conductivity.csv")
    data = np.loadtxt(path, skiprows=2)
    if salt == "LiCl":
        conc = data[:9, 0]
        cond = data[:9, 1]
        f = interp1d(conc, cond, kind="cubic")
    elif salt == "KCl":
        conc = data[:6, 2]
        cond = data[:6, 3]
        # fit = np.polyfit(conc, cond, 1)
        # f = lambda x: fit[1] + fit[0]*x
        fit, _, _, _ = np.linalg.lstsq(conc[:, np.newaxis], cond, rcond=None)
        f = lambda x: x * fit
    elif salt == "NHSO":
        conc = data[:8, 4]
        cond = data[:8, 5]
        f = interp1d(conc, cond, kind="quadratic")
    elif salt == "NaCl":
        conc = data[:8, 6]
        cond = data[:8, 7]
        f = interp1d(conc, cond, kind="quadratic")
    elif salt == "CsCl":
        conc = data[:7, 8]
        cond = data[:7, 9]
        # fit = np.polyfit(conc, cond, 1)
        # f = lambda x: fit[1] + fit[0]*x
        fit, _, _, _ = np.linalg.lstsq(conc[:, np.newaxis], cond, rcond=None)
        f = lambda x: x * fit
    else:
        print("Unknown salt")
        raise SizerError
    return f


def capres(d, cond, psi1=0.092, psi2=0.046, L=100e-9):
    d = d * 1e-9
    r1 = 4 * L / (cond * np.pi * d * (d + 2 * L * np.tan(psi1)))
    r2 = 2 / (cond * np.pi * np.tan(psi2) * (d + 2 * L * np.tan(psi1)))
    r3 = 1 / (2 * cond * d)
    res = r1 + r2 + r3
    return res


def capsize(res, cond):
    dia = np.arange(0.5, 10000, 0.1)
    r = capres(dia, cond, 0.092, 0.046)
    d = dia[np.where(r < res)][0]
    return d


def memsize(res, cond, thickness):
    p = [np.pi * res * cond, -np.pi, -4 * thickness]
    roots = np.roots(p)
    d = roots[0] * 1e9
    return d


def size(res, salt, salt_conc, pore, thickness=None):
    condfunc = conductivity(salt)
    cond = condfunc(salt_conc)
    if pore == "glass":
        d = capsize(res, cond)
    elif pore == "Si":
        d = memsize(res, cond, thickness * 1e-9)
    else:
        print("Unknown pore type")
        raise SizerError
    return d


def poresizer(current, voltage, pore, salt, salt_conc, axes=None):
    current = current - current[int((current.size - 1) / 2)]
    try:
        if pore == "glass_0.2" or pore == "glass_0.3":
            checkpoint = 600
            pore = "glass"
            thickness = None
        elif (pore.split("_", 1)[0] == "SiO") or (pore.split("_", 1)[0] == "SiN"):
            checkpoint = 100
            thickness = pore.split("_", 1)[1]
            thickness = float(thickness.split("nm", 1)[0])
            pore = "Si"
    except IndexError:
        raise SizerError
    try:
        ineg = current[np.where(abs(voltage + checkpoint) < 5)][0]
        ipos = current[np.where(abs(voltage - checkpoint) < 5)][0]
    except IndexError:
        recratio = -1
    else:
        recratio = abs(ineg / ipos)
    if recratio > 1.1:
        coeff = np.polyfit(
            voltage[voltage.round() <= -300], current[voltage.round() <= -300], 1
        )
        col = "--r"
    else:
        coeff = np.polyfit(voltage, current, 1)
        col = "--m"
    resohm = 1 / (coeff[0] * 1e-6)
    try:
        poresize = size(resohm, salt, salt_conc, pore, thickness)
    except SizerError:
        print("Poresizer error")
        raise SizerError
    if axes:
        axes.clear()
        axes.set_autoscale_on(True)
        axes.set_xlabel("voltage (mV)")
        axes.set_ylabel("current (nA)")
        axes.plot(voltage, current, linestyle="", marker=".", markersize=8)
        axes.plot(voltage, coeff[1] + coeff[0] * voltage, col)
    return resohm, poresize, recratio
