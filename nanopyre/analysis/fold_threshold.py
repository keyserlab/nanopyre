#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Nov  8 11:32:13 2017


"""

import numpy as np


def find_first_instance(y, threshold, slope, extrema=-1):
    y = y * extrema
    threshold = threshold * extrema
    if slope == 1:
        ind = (y > threshold).nonzero()[0][0]
    else:
        ind = (y < threshold).nonzero()[0][0]
    return ind


def fold_threshold(y, eventthreshold, foldthreshold):
    out = np.zeros(y.shape)
    try:
        eventstart = find_first_instance(y, eventthreshold, 1, -1)
        eventstop = y.size - find_first_instance(y[::-1], eventthreshold, 1, -1)
    except IndexError:
        return out
    try:
        foldstart = eventstart + find_first_instance(
            y[eventstart:], foldthreshold, 1, -1
        )
        foldstop = eventstop - find_first_instance(
            y[:eventstop][::-1], foldthreshold, 1, -1
        )
    except IndexError:
        foldstart = None
        foldstop = None
    #    else:
    #        foldstart = triggerleft - findFirstInstance(y[:triggerleft][::-1], l1, -1, -1)
    #        try:
    #            foldstop = triggerleft + findFirstInstance(y[triggerleft:], l1, -1, -1)
    #        except IndexError:
    #            return out

    #        try:
    #            foldstop = triggerleft + findFirstInstance(y[triggerleft:], foldthreshold, -1, -1)
    #        except IndexError:
    #            return out
    f = lambda a, b: b if b is not None else a
    out[f(0, foldstart) : f(0, foldstop)] = min(
        y[f(0, foldstart) : f(0, foldstop)].mean(), 0
    )
    out[f(eventstart, foldstop) : eventstop] = y[
        f(eventstart, foldstop) : eventstop
    ].mean()
    out[eventstart : f(0, foldstart)] = min(
        y[f(0, foldstart) : f(0, foldstop)].mean(), 0
    )
    inds = (eventstart, eventstop, foldstart, foldstop)
    return (out, inds)
