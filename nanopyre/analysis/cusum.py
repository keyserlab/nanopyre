#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Oct  9 19:20:51 2017



Python CUSUM implementation based on Raillon et al., Nanoscale 2012 (http://dx.doi.org/10.1039/c2nr30951c)
and MOSAIC (https://pages.nist.gov/mosaic/) by Forstater et al., Analytical Chemistry 2016 
(http://dx.doi.org/10.1021/acs.analchem.6b03725)
"""

import numpy as np


def cusum_steps(
    x,
    y,
    threshold,
    delta=0.05,
    sigma=0.005,
    stepdirections=[-1.0, 1.0, 1.0],
    debug=False,
):
    mu = y.cumsum() / np.arange(1, y.size + 1)
    var = np.power(sigma, 2)
    leftind = 0
    stepinds = []
    if debug:
        Glist = []
        Slist = []
        threshlist = []
    for direc in stepdirections:
        s = delta * direc / var * (y[leftind:] - mu - delta * direc / 2)
        S = s.cumsum()
        s[s <= 0] = 0
        G = s.cumsum()
        if debug:
            Glist.append([x[leftind:], G])
            Slist.append([x[leftind:], S])
            threshlist.append(threshold)
        threshind = np.argwhere(G > threshold)
        try:
            threshind = threshind[0][0]
        except IndexError:
            break
        else:
            si = S[: threshind - 1].argmin() + 1
            stepinds.append(si + leftind)
            leftind = leftind + si
            yview = y[leftind:]
            mu = yview.cumsum() / np.arange(1, yview.size + 1)
    stepinds.insert(0, 0)
    stepinds.append(y.size - 1)
    stepy = np.zeros(y.shape, dtype=float)
    for i in range(len(stepinds) - 1):
        stepy[stepinds[i] : stepinds[i + 1] + 1] = y[
            stepinds[i] : stepinds[i + 1] + 1
        ].mean()
    if not debug:
        return stepinds, stepy
    else:
        return stepinds, stepy, Glist, Slist, threshlist
