#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Feb 8 00:00:00 2019


"""

import numpy as np
from scipy.signal import find_peaks, find_peaks_cwt


def scipy_find_peaks(
    x,
    min_heigth,
    max_height,
    min_threshold,
    max_threshold,
    distance,
    min_prominence,
    max_prominence,
    min_width,
    max_width,
    wlen,
    rel_height,
    min_plateau_size,
    max_plateau_size,
):
    height = (np.ones(x.shape) * -max_height, np.ones(x.shape) * -min_heigth)
    threshold = (np.ones(x.shape) * -max_threshold, np.ones(x.shape) * -min_threshold)
    prominence = (
        np.ones(x.shape) * -max_prominence,
        np.ones(x.shape) * -min_prominence,
    )
    width = (np.ones(x.shape) * min_width, np.ones(x.shape) * max_width)
    plateau_size = (
        np.ones(x.shape) * min_plateau_size,
        np.ones(x.shape) * max_plateau_size,
    )
    peaks, properties = find_peaks(
        x,
        height,
        threshold,
        distance,
        prominence,
        width,
        wlen,
        rel_height,
        plateau_size,
    )
    return peaks, properties
