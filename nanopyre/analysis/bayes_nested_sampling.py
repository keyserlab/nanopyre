#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Oct  9 19:14:22 2017


"""

import numpy as np
import inspect

from nanopyre.analysis.multinest_wrapper import run_multinest
from nanopyre.analysis.multinest_analyser import Analyzer


class BayesRun:
    def __init__(self, **kwargs):
        self.algorithm_settings = {
            "n_params": kwargs.pop("n_params", None),
            "n_clustering_params": kwargs.pop("n_clustering_params", None),
            "importance_nested_sampling": kwargs.pop(
                "importance_nested_sampling", False
            ),
            "multimodal": kwargs.pop("multimodal", True),
            "const_efficiency_mode": kwargs.pop("const_efficiency_mode", False),
            "n_live_points": kwargs.pop("n_live_points", 200),
            "evidence_tolerance": kwargs.pop("evidence_tolerance", 0.1),
            "sampling_efficiency": kwargs.pop("sampling_efficiency", 0.3),
            "n_iter_before_update": kwargs.pop("n_iter_before_update", 10000),
            "null_log_evidence": kwargs.pop("null_log_evidence", -1e90),
            "max_modes": kwargs.pop("max_modes", 20),
            "mode_tolerance": kwargs.pop("mode_tolerance", -1e90),
            "outputfiles_basename": kwargs.pop(
                "outputfiles_basename", "/home/user/code/multinest/chains/result_"
            ),
            "seed": kwargs.pop("seed", -1),
            "verbose": kwargs.pop("verbose", False),
            "resume": kwargs.pop("resume", False),
            "context": kwargs.pop("context", 0),
            "write_output": kwargs.pop("write_output", True),
            "log_zero": kwargs.pop("log_zero", -1e90),
            "max_iter": kwargs.pop("max_iter", 50000),
            "init_MPI": kwargs.pop("init_MPI", False),
            "dump_callback": kwargs.pop("dump_callback", None),
        }
        self.settings = {
            "multinest_libpath": kwargs.pop("multinest_libpath", ""),
            "func_base_void_parameters": kwargs.pop("func_base_void_parameters", 1),
        }

    def run(self, basefunc, loglikelihood, priorfunc):
        ndims = (
            len(inspect.signature(basefunc).parameters)
            - self.settings["func_base_void_parameters"]
        )
        run_multinest(
            self.settings["multinest_libpath"],
            loglikelihood,
            priorfunc,
            ndims,
            **self.algorithm_settings
        )
        analyzer = Analyzer(ndims, self.algorithm_settings["outputfiles_basename"])
        if (analyzer.get_NevalMeasure() - 100) == self.algorithm_settings["max_iter"]:
            print("NO CONVERGENCE, RETRY")
            run_multinest(
                self.settings["multinest_libpath"],
                loglikelihood,
                priorfunc,
                ndims,
                **self.algorithm_settings
            )
        self.lastrun_basefunc = basefunc
        self.get_results()

    def get_results(self):
        try:
            ndims = (
                len(inspect.signature(self.lastrun_basefunc).parameters)
                - self.settings["func_base_void_parameters"]
            )
        except AttributeError:
            print("No results have been produced, run first")
            return
        analyzer = Analyzer(ndims, self.algorithm_settings["outputfiles_basename"])
        self.posteriors = analyzer.get_data()
        stats = analyzer.get_mode_stats()
        logz = stats["global evidence"]
        logzerr = stats["global evidence error"]
        params = []
        paramerrs = []
        locallogzlist = []
        for i in range(len(stats["modes"])):
            locallogzlist.append(stats["modes"][i]["local log-evidence"])
        modeindex = locallogzlist.index(max(locallogzlist))
        for i in range(0, ndims):
            params.append(stats["modes"][modeindex]["mean"][i])
            paramerrs.append(stats["modes"][modeindex]["sigma"][i])
        self.result = {
            "global_logz": logz,
            "global_logzerr": logzerr,
            "parameters": params,
            "parameter_errors": paramerrs,
        }


class BayesFuncs:
    def __init__(self, **kwargs):
        self.data = kwargs.pop("data", np.zeros(100))
        self.xvalues = kwargs.pop("xvalues", np.arange(self.data.size, dtype=float))
        self.noise = kwargs.pop("noise", 0.01)
        self.level1 = kwargs.pop("level1", -0.1)
        self.level2 = kwargs.pop("level2", -0.2)
        self.update()

    def update(self):
        self.ECD = self.data.sum() * np.diff(self.xvalues)[0]
        self.priors = {
            "OneEvent": [[self.xvalues.max() * 0.5, 0.0], [self.xvalues.max(), 0]],
            "OneEvent_": [
                [self.xvalues.max() * 0.5, 0.0],
                [self.xvalues.max(), 0],
                [self.level1 * 0.5, self.level1 * 0.75],
            ],
            "TwoOneEvent": [
                [self.xvalues.max() * 0.5, 0.0],
                [self.xvalues.max(), 0],
                [self.xvalues.max(), 0],
                [self.level1 * 0.4, self.level1],
            ],
            "TwoOneEvent_": [
                [self.xvalues.max() * 0.5, 0.0],
                [self.xvalues.max(), 0],
                [self.xvalues.max(), 0],
                [(self.level2 - self.level1) * 1.5, self.level1 * 0.75],
                [self.level1 * 0.5, self.level1 * 0.75],
            ],
            "fold_slope": [
                [self.data.size * 0.5, 0.0],
                [self.data.size * 0.5, self.data.size * 0.5],
                [self.data.size * 0.5, 1.0],
                [-0.25, -0.05],
                [0.5, 1.0],
            ],
        }

    def logL(self, fk):
        logL = self.gaussnoise(fk)
        #        logL = self.linear_penalty(fk)
        return logL

    def gaussnoise(self, fk):
        logL = fk - self.data
        logL = -np.power(logL, 2)
        logL = np.sum(logL) / 2 / np.power(self.noise, 2)
        return logL

    def linear_penalty(self, fk):
        #        logL = -abs(fk - self.data)
        logL = -np.sqrt(np.abs(fk - self.data))
        logL = np.sum(logL) / 2 / np.power(self.noise, 2)
        return logL

    def fill_priors(self, ndim, inp, priors):
        for i in range(ndim):
            inp[i] = inp[i] * priors[i][0] + priors[i][1]
        return inp

    def gauss(x, mean, amplitude, width):
        y = amplitude * np.exp(-np.square(x - mean) / 2.0 / width / width)
        return y

    def heaviside(self, x, position, stepsize):
        y = 0.5 * (np.sign(x - position) + 1) * stepsize
        return y

    # Unfolded event

    def OneEvent_base_(self, x, cutoffa, increment, level1):
        cutoffc = cutoffa + increment
        y = self.heaviside(x, cutoffa, level1) + self.heaviside(x, cutoffc, -level1)
        return y

    def OneEvent_(self, theta, ndim, nparams):
        fk = self.OneEvent_base_(self.xvalues, theta[0], theta[1], theta[2])
        logL = self.logL(fk)
        return logL

    def OneEvent_prior_(self, inp, ndim, nparams):
        priors = self.priors["OneEvent_"]
        inp = self.fill_priors(ndim, inp, priors)

    def OneEvent_base(self, x, cutoffa, increment):
        cutoffc = cutoffa + increment
        level1 = self.ECD / increment
        y = self.heaviside(x, cutoffa, level1) + self.heaviside(x, cutoffc, -level1)
        return y

    def OneEvent(self, theta, ndim, nparams):
        fk = self.OneEvent_base(self.xvalues, theta[0], theta[1])
        logL = self.logL(fk)
        return logL

    def OneEvent_prior(self, inp, ndim, nparams):
        priors = self.priors["OneEvent"]
        inp = self.fill_priors(ndim, inp, priors)

    # Folded event

    def TwoOneEvent_base_(self, x, cutoffa, incrementb, incrementc, level2, level1):
        cutoffb = cutoffa + incrementb
        cutoffc = cutoffb + incrementc
        y = (
            self.heaviside(x, cutoffa, level2)
            + self.heaviside(x, cutoffb, level1 - level2)
            + self.heaviside(x, cutoffc, -level1)
        )
        return y

    def TwoOneEvent_(self, theta, ndim, nparams):
        fk = self.TwoOneEvent_base_(
            self.xvalues, theta[0], theta[1], theta[2], theta[3], theta[4]
        )
        logL = self.logL(fk)
        return logL

    def TwoOneEvent_prior_(self, inp, ndim, nparams):
        priors = self.priors["TwoOneEvent_"]
        inp = self.fill_priors(ndim, inp, priors)

    def TwoOneEvent_base(self, x, cutoffa, incrementb, incrementc, level1):
        cutoffb = cutoffa + incrementb
        cutoffc = cutoffb + incrementc
        #        level2 = min((1/incrementb)*(self.ECD - (cutoffc-cutoffb)*level1), level1)
        level2 = (1 / incrementb) * (self.ECD - (cutoffc - cutoffb) * level1)
        y = (
            self.heaviside(x, cutoffa, level2)
            + self.heaviside(x, cutoffb, (level1 - level2))
            + self.heaviside(x, cutoffc, -level1)
        )
        return y

    def TwoOneEvent(self, theta, ndim, nparams):
        fk = self.TwoOneEvent_base(self.xvalues, theta[0], theta[1], theta[2], theta[3])
        logL = self.logL(fk)
        return logL

    def TwoOneEvent_prior(self, inp, ndim, nparams):
        priors = self.priors["TwoOneEvent"]
        inp = self.fill_priors(ndim, inp, priors)

    # Folded event, sloped intra-event current

    def fold_slope_base(self, x, cutoffa, cutoffc, incrementb, constc, slope):
        y = np.zeros(x.size, dtype=float)
        cutoffa = int(cutoffa)
        cutoffc = int(cutoffc)
        cutoffb = min(cutoffc, int(cutoffa + incrementb))
        levelc = constc * np.linspace(1.0, slope, cutoffc - cutoffb)
        y[0:cutoffa] = 0.0
        y[cutoffa:cutoffb] = min((1 / incrementb) * (self.ECD - levelc.sum()), constc)
        y[cutoffb:cutoffc] = levelc
        y[cutoffc:] = 0.0
        return y

    def fold_slope(self, theta, ndim, nparams):
        fk = self.fold_slope_base(
            self.xvalues, theta[0], theta[1], theta[2], theta[3], theta[4]
        )
        logL = self.logL(fk)
        return logL

    def fold_slope_prior(self, inp, ndim, nparams):
        priors = self.priors["fold_slope"]
        inp = self.fill_priors(ndim, inp, priors)
