# -*- coding: utf-8 -*-
"""
Created on Tue Feb 21 08:24:28 2017


"""

import numpy as np
from glob import glob
from collections import deque

import pandas as pd
import h5py as h5
from datetime import date

from nanopyre.io.fileio import read_file


class CurrentTrace:
    def __init__(self, inputpath, outputpath, **kwargs):
        self.filelist = sorted(glob(inputpath + "*.tdms"))
        if len(self.filelist) == 0:
            self.filelist = sorted(glob(inputpath + "*.hdf5"))
        self.outputpath = outputpath
        self.framerate = kwargs.pop("framerate_Hz", 250000)
        self.watchlen = self.ms2frames(kwargs.pop("watchlen_ms", 500))
        self.corrlen = self.ms2frames(kwargs.pop("corrlen_ms", 3000))
        if self.watchlen >= self.corrlen:
            self.pointsneeded = self.watchlen + self.watchlen % self.corrlen
        else:
            self.pointsneeded = self.corrlen
        self.correctedlen = 0
        self.watchedlen = 0
        self.netwatchedlen = 0
        self.droppedlen = 0
        self.overlap = np.array([])
        self.endreached = False

        self.eventno = 0
        self.startlookinghere = 0
        self.chunksize = 10000
        self.chunksizesmall = self.ms2frames(2)
        self.findpeaks = kwargs.pop("findpeaks", False)
        self.threshold = kwargs.pop("threshold_nA", -0.1)
        self.maxduration = kwargs.pop("maxduration_ms", 2.5)
        self.minduration = kwargs.pop("minduration_ms", 0.2)
        self.maxarea = kwargs.pop("maxarea_fC", 400.0)
        self.minarea = kwargs.pop("maxarea_fC", 5.0)
        self.boundarythreshold = 0.0
        self.pointseitherside = 20
        self.foundone = False
        self.eventloaded = False
        self.timebetweenevents = 10000

        self.columns = [
            "eventno",
            "start_ms",
            "start_net_ms",
            "duration_ms",
            "area_fC",
            "startinwatchwindow_frame",
            "start_frame",
            "currentbaseline_nA",
            "rms_nA",
            "file",
        ]
        self.eventdata = pd.DataFrame()
        self.settingslabels = [
            "threshold_nA",
            "minduration_ms",
            "minarea_fC",
            "returnthreshold_nA",
            "maxduration_ms",
            "maxarea_fC",
            "pointsadded",
            "samplefreq_Hz",
        ]
        self.datalist = [0] * len(
            self.columns
        )  # deque([0]*len(self.columns), len(self.columns))
        self.fill_deques()

    def update_settings(self, dic):
        for key, value in dic.items():
            setattr(self, key, value)
        self.update_thresh_funcs()

    def update_thresh_funcs(self):
        if not self.findpeaks:
            self.trigfunc = lambda x: x < self.threshold
            self.edgefunc = lambda x: x >= self.boundarythreshold
        else:
            self.trigfunc = lambda x: x > self.threshold
            self.edgefunc = lambda x: x <= self.boundarythreshold

    def frames2ms(self, frames):
        return frames / self.framerate * 1000.0

    def ms2frames(self, ms):
        return round(ms * self.framerate / 1000.0)

    def readfile(self, path):
        data = read_file(path)
        return data

    def fill_deques(self):
        filecontainer = []
        filelengths = []
        filenames = []
        fileends = []
        self.filllevel = 0
        counter = 0
        startcurrent_fileindex = 0
        while self.filllevel < self.pointsneeded:
            try:
                path = self.filelist[counter]
            except IndexError:
                raise IndexError
            filenames.append(path)
            data = self.readfile(path)
            if counter == startcurrent_fileindex and not data.size == 0:
                self.start_current = np.mean(data)
            else:
                startcurrent_fileindex += 1
            filecontainer.append(data)
            self.filllevel += data.size
            fileends.append(self.filllevel)
            filelengths.append(data.size)
            counter += 1
        self.lastfileindex = counter - 1
        self.filecontainer = deque(filecontainer)
        self.filelengths = deque(filelengths)
        self.filenames = deque(filenames)
        self.fileends = deque(fileends)
        self.previousend = 0
        self.fullarray = np.concatenate(self.filecontainer)

    def update_deques(self):
        #        print(self.filenames)
        #        print('------------------------')
        lastend = self.fileends[-1]
        firstend = self.fileends[0]
        while firstend <= self.watchedlen:
            #            print('dropped')
            self.droppedlen += self.filelengths[0]
            self.filllevel -= self.filelengths[0]
            self.previousend = self.fileends[0]
            lastend = self.fileends[-1]
            self.filecontainer.popleft()
            self.filelengths.popleft()
            self.filenames.popleft()
            self.fileends.popleft()
            try:
                firstend = self.fileends[0]
            except IndexError:
                break
        offset = max(self.watchedlen - self.previousend, 0)
        #        print('watchedlen ' + str(self.watchedlen))
        #        print('previousend ' + str(self.previousend))
        #        print('lastend ' + str(lastend))
        #        print('offset ' + str(offset))
        #        print('filllevel ' + str(self.filllevel))
        while (self.filllevel - offset) < self.pointsneeded:
            try:
                path = self.filelist[self.lastfileindex + 1]
            except IndexError:
                self.endreached = True
                #                print('END')
                break
            else:
                self.lastfileindex += 1
                data = self.readfile(path)
                #                print('added')
                self.filllevel += data.size
                self.filecontainer.append(data)
                self.filelengths.append(data.size)
                self.filenames.append(path)
                lastend += data.size
                self.fileends.append(lastend)
                self.fullarray = np.concatenate(self.filecontainer)

    #                print(self.filllevel - offset)
    #                print(self.pointsneeded)
    #        print('------------------------')

    def correct_one_window(self, skipped=False, mode="mean"):
        corarray = self.overlap
        self.xtrace = self.frames2ms(
            np.arange(self.watchedlen, self.watchedlen + self.watchlen)
        )
        self.startlookinghere = self.watchedlen
        statarray = self.fullarray[
            self.watchedlen
            - self.droppedlen : self.watchedlen
            + self.watchlen
            - self.droppedlen
        ]
        self.mean = np.mean(statarray)
        self.rms = np.std(statarray)
        self.watchedlen += self.watchlen
        if not skipped:
            self.netwatchedlen += self.watchlen

        counter = 0
        while self.correctedlen < self.watchedlen:
            counter += 1
            frm = self.correctedlen - self.droppedlen
            to = self.correctedlen - self.droppedlen + self.corrlen
            #            print('from ' + str(frm))
            #            print('to ' + str(to))
            #            print('arrlen ' + str(self.fullarray.size))
            corrchunk = self.fullarray[frm:to]
            corarray = np.append(corarray, self.correct_mean(corrchunk))
            self.correctedlen += corrchunk.size
        self.overlap = corarray[self.watchlen :]
        self.ytrace = corarray[0 : self.watchlen]
        self.update_deques()

    def correct_mean(self, array):
        array = array - np.mean(array)
        return array

    def one_event(self):
        startofwatchwindow = self.watchedlen - self.watchlen
        offset = self.startlookinghere - startofwatchwindow
        try:
            trigger = self.find_first_instance(
                self.ytrace[offset:], self.trigfunc, int(self.timebetweenevents * 1.5)
            )
        except ValueError:
            raise EndOfLineError
        try:
            self.stop = (
                offset
                + trigger
                + self.find_first_instance(
                    self.ytrace[offset + trigger :], self.edgefunc, self.chunksizesmall
                )
            )
        except ValueError:
            raise RightEndError
        try:
            self.start = (
                offset
                + trigger
                - self.find_first_instance(
                    self.ytrace[: offset + trigger][::-1],
                    self.edgefunc,
                    self.chunksizesmall,
                )
            )
        except ValueError:
            raise LeftEndError
        self.yevent = self.ytrace[
            max(self.start - int(self.pointseitherside), 0) : min(
                self.stop + int(self.pointseitherside), self.ytrace.size
            )
        ]
        self.xevent = self.frames2ms(np.arange(len(self.yevent)))
        self.duration = self.frames2ms(self.stop - self.start)
        self.area = abs(
            np.sum(self.ytrace[self.start : self.stop]) * self.frames2ms(1) * 1000.0
        )
        self.timebetweenevents = self.start + startofwatchwindow - self.datalist[5]
        if (
            (self.duration > self.minduration)
            & (self.duration < self.maxduration)
            & (self.area > self.minarea)
            & (self.area < self.maxarea)
        ):
            self.foundone = True
            self.eventloaded = True
            self.datalist[1] = self.frames2ms(self.start + startofwatchwindow)
            self.datalist[2] = self.frames2ms(
                self.start + startofwatchwindow - (self.watchedlen - self.netwatchedlen)
            )
            self.datalist[3] = self.duration
            self.datalist[4] = self.area
            self.datalist[5] = self.start
            self.datalist[6] = self.start + startofwatchwindow
            self.datalist[7] = self.mean
            self.datalist[8] = self.rms
            self.datalist[9] = self.filenames[0].rsplit("/", 1)[1]
        self.startlookinghere = startofwatchwindow + self.stop

    def find_first_instance(self, array, condition, chunksize=10000):
        a = 0
        chunkindex = range(chunksize, array.size + chunksize, chunksize)
        for b in chunkindex:
            chunk = array[a:b]
            for i in condition(chunk).nonzero()[0]:
                return i + a
            a = b
        raise ValueError("Condition not satisfied anywhere")

    def accept_current_event(self, write, maxrms):
        if not self.eventloaded:
            raise TraceError
        self.eventno += 1
        self.datalist[0] = self.eventno
        add = pd.DataFrame([self.datalist], columns=self.columns)
        self.eventdata = self.eventdata.append(add, ignore_index=True)
        settings = [
            self.threshold,
            self.minduration,
            self.minarea,
            self.boundarythreshold,
            self.maxduration,
            self.maxarea,
            self.pointseitherside,
            self.framerate,
        ]
        if write:
            try:
                f = self.eventfile
            except AttributeError:
                self.eventfile = h5.File(self.outputpath + "events.hdf5", "w")
                f = self.eventfile
            grp = f.create_group(self.make_outputname(self.eventno))
            ds = grp.create_dataset("current_nA", data=self.yevent)
            for i, x in enumerate(self.columns[:-1]):
                grp.attrs.create(x, self.datalist[i])
            grp.attrs.create(self.columns[-1], np.array(self.datalist[-1], dtype="S"))
            for i, x in enumerate(self.settingslabels):
                grp.attrs.create(x, settings[i])
            grp.attrs.create("maxrms_nA", maxrms)

    def save_summary(self, write, sql):
        frequ = self.eventno / (self.frames2ms(self.netwatchedlen) / 1000.0)
        if write and (self.eventno != 0):
            self.eventfile["/"].attrs.create(
                "lastchange", np.array(str(date.today()), dtype="S")
            )
            self.eventfile["/"].attrs.create("frequency_Hz", frequ)
            self.eventfile["/"].attrs.create("totallength_frames", self.watchedlen)
            self.eventfile["/"].attrs.create("netlength_frames", self.netwatchedlen)
            with pd.HDFStore(self.outputpath + "finder_summary.hdf5", "w") as f:
                try:
                    f.put("summary", self.eventdata, format="t")
                except TypeError:
                    f.put(
                        "summary", self.eventdata.loc[:, self.columns[:-1]], format="t"
                    )
                    print("Unable to save filelist in run summary, saving without")
            with h5.File(self.outputpath + "finder_summary.hdf5", "r+") as f:
                try:
                    f["summary"].attrs.create(
                        "lastchange", np.array(str(date.today()), dtype="S")
                    )
                    f["summary"].attrs.create("frequency_Hz", frequ)
                    f["summary"].attrs.create("totallength_frames", self.watchedlen)
                    f["summary"].attrs.create("netlength_frames", self.netwatchedlen)
                except KeyError:
                    pass
            if sql[0]:
                login = sql[1]
                lis = [
                    date.today(),
                    login.usn,
                    True,
                    self.eventno,
                    self.start_current,
                    frequ,
                ]
                login.insertorupdate(
                    "alist",
                    [
                        "ddatea",
                        "whoa",
                        "eventsfound",
                        "noevents",
                        "start_current",
                        "frequency",
                    ],
                    lis,
                    sql[2],
                    (False, "mlist"),
                )
        self.closefiles()

    def make_outputname(self, eventno):
        add = (6 - len(str(eventno))) * "0"
        filename = "T" + add + str(eventno)
        return filename

    def closefiles(self):
        try:
            self.eventfile.close()
        except:
            pass


class TraceError(Exception):
    pass


class EndOfLineError(Exception):
    pass


class RightEndError(Exception):
    pass


class LeftEndError(Exception):
    pass
