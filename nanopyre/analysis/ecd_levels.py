#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Oct  6 12:03:16 2017


"""

import numpy as np
from scipy.stats import gaussian_kde
import warnings
from scipy.optimize import OptimizeWarning

from nanopyre.analysis.peakfinder import peakfinder_old, peakmask_old, gauss


class EcdLevelError(Exception):
    pass


def find_gaussians(data, method="kde"):
    if method == "kde":
        kde = gaussian_kde(data, bw_method="scott")
        points = np.linspace(data.min(), data.max(), 1000)
        destimate = kde(points)
    elif method == "hist":
        destimate, points = np.histogram(data, bins=70, density=True)
        points = points[:-1] + (points[1] - points[0]) / 2
    peakthreshold = np.ptp(destimate) * 0.01
    (
        peakinds,
        left_valleyinds,
        right_valleyinds,
        magnitudes,
        ind,
        peakloc,
    ) = peakfinder_old(destimate, peakthreshold, 0.0, 1, 9, 100.0)
    with warnings.catch_warnings():
        warnings.simplefilter("error", OptimizeWarning)
        try:
            peakparams, peakdata, leftlist, rightlist, blines = peakmask_old(
                gauss,
                points,
                destimate,
                peakinds,
                ind,
                peakloc,
                3,
                1,
                correct_baseline=False,
            )
        except (OptimizeWarning, RuntimeError):
            peakparams, peakdata, leftlist, rightlist, blines = (None,) * 5
    return points, destimate, peakinds, ind, peakparams, peakdata, leftlist, rightlist


def fit_one_gaussian(data, method="kde"):
    (
        points,
        destimate,
        peakinds,
        ind,
        peakparams,
        peakdata,
        leftlist,
        rightlist,
    ) = find_gaussians(data, method)
    usinghwhm = False
    if peakparams is None:
        usinghwhm = True
        gaussfit = -np.ones((3,))
        largest_peakind = peakinds[np.argmax(destimate[peakinds])]
        gaussfit[0] = points[largest_peakind]
        gaussfit[1] = destimate[largest_peakind]
        hwhm = np.where(destimate > gaussfit[1] * 0.5)[0]
        hwhm = min(largest_peakind - hwhm[0], hwhm[-1] - largest_peakind)
        gaussfit[2] = np.ptp(points[:hwhm])
    elif len(peakinds) != 0:
        largest_peakind_index = np.argmax(destimate[peakinds])
        gaussfit = peakparams
        gaussfit = gaussfit[largest_peakind_index]
    else:
        gaussfit = -np.ones((3,))
    return points, destimate, peakinds, ind, peakparams, peakdata, gaussfit, usinghwhm


def fit_many_gaussians(data, method="kde", n=3, threshold=0.015):
    """
    Finds n descending levels, flip data to find ascending ones.
    """
    (
        points,
        destimate,
        peakinds,
        ind,
        peakparams,
        peakdata,
        leftlist,
        rightlist,
    ) = find_gaussians(data, method)
    if peakparams is None:
        peakinds_filtered = np.sort(peakinds[points[peakinds] < threshold])
        levels = np.array(
            [
                points[peakinds_filtered[-n:]],
                destimate[peakinds_filtered[-n:]],
                -np.ones((peakinds_filtered[-n:].size,)),
            ]
        ).T
        levels = np.flip(levels, 0)
    else:
        peakparams = sorted(peakparams, key=lambda x: x[0], reverse=True)
        levels = np.array(peakparams)
    if len(levels) != 0:
        levels = levels[levels[:, 0] < threshold]
    else:
        levels = -np.ones((n, 3))
    if len(levels) > n:
        levels = levels[:n]
    elif (n == 3) and len(levels) == 2:
        levels = np.append(levels, [[1.75 * levels[1][0], -1.0, levels[1][2]]], axis=0)
    elif (n == 3) and len(levels) == 1:
        levels = np.array(
            [[-1.0, -1.0, -1.0], levels[0], [1.75 * levels[0][0], -1.0, levels[0][2]]]
        )
    return points, destimate, peakinds, ind, peakparams, peakdata, levels


def filter_ecd(dataframe, axes=None):
    s = dataframe["current_nA"]
    ecd = s.apply(np.sum).values / dataframe["samplefreq_Hz"].values * 1e6
    cutmultiplier = 1.5
    (
        points,
        destimate,
        peakinds,
        ind,
        peakparams,
        peakdata,
        ecd_gaussfit,
        usinghwhm,
    ) = fit_one_gaussian(ecd)
    cutmultiplier = 1.0 if usinghwhm else cutmultiplier
    (mean, amplitude, width) = ecd_gaussfit
    # index = np.arange(0, ecd.size)
    index = dataframe["eventno"].apply(int) - 1
    #    if peakparams:
    #        peakparams = max(peakparams, key=lambda x: x[1])
    #        mean = peakparams[0]
    #        amplitude = peakparams[1]
    #        width = peakparams[2]
    #        ecd_gaussfit = np.array([mean, amplitude, width])
    #    else:
    #        raise EcdLevelError
    leftcut = mean - cutmultiplier * width
    rightcut = mean + cutmultiplier * width
    filtered_index = index[((ecd > leftcut) & (ecd < rightcut))]
    #    components = 3
    #    gm = GaussianMixture(n_components=components, covariance_type='spherical', tol=1e-6, max_iter=10000)
    #    gm.fit(ecd.reshape(-1,1))
    #    points = np.linspace(ecd.min(), ecd.max(), 500)
    #    largest = max(enumerate(gm.weights_), key= lambda x: x[1])
    #    mean = gm.means_[largest[0]][0]
    #    amplitude = largest[1]
    #    width = np.sqrt(gm.covariances_[largest[0]])
    #    ecd_gaussfit = np.array([mean, amplitude, width])
    #    leftcut = mean - 1.5*width
    #    rightcut = mean + 1.5*width
    #    filtered_index = np.arange(0, ecd.size)[((ecd > leftcut) & (ecd < rightcut))]
    if axes:
        axes.clear()
        axes.set_autoscale_on(True)
        axes.set_xlabel("ECD (fC)")
        axes.set_ylabel("prob. density")
        #        axes.hist(ecd, bins='auto', normed=True)
        #        fit = np.zeros((points.size,components))
        #        for i, _ in enumerate(gm.means_):
        ##            print(gm.means_[i], np.sqrt(gm.covariances_[i]), gm.weights_[i])
        #            fit[:,i] = norm(gm.means_[i], np.sqrt(gm.covariances_[i])).pdf(points) * gm.weights_[i]
        #            axes.plot(points, fit[:,i], c='k', linewidth=2)
        #        fitted = norm(mean, width).pdf(points) * amplitude
        #        axes.plot(points, fitted, 'r', linewidth=2)
        axes.plot(points, destimate, "b", marker=".", markersize=3, linewidth=0.5)
        #        axes.plot(points[ind], destimate[ind], marker='x', markersize=5, linestyle='None', color='g')
        axes.plot(
            points[peakinds],
            destimate[peakinds],
            marker="x",
            markersize=10,
            linestyle="None",
            color="m",
            mew=3,
        )
        #    reduced = destimate.copy()
        if peakparams is not None:
            for i, arr in enumerate(peakdata):
                axes.plot(
                    arr[0, :],
                    arr[1, :],
                    color="r",
                    marker=".",
                    markersize=3,
                    linewidth=0.5,
                )
        #            destimate[leftlist[i]:rightlist[i]+1] -= arr[2,:]
        #            axes.plot(points, destimate, color='g', marker='.', markersize=3, linewidth=0.5)
        axes.plot(
            ecd_gaussfit[0],
            ecd_gaussfit[1],
            marker="x",
            markersize=10,
            linestyle="None",
            color="k",
            mew=3,
        )
        axes.axvline(leftcut, 0, 100, color="g", linewidth=2)
        axes.axvline(rightcut, 0, 100, color="g", linewidth=2)
    return ecd_gaussfit, filtered_index


def detect_levels(dataframe, filtered_index, findpeaks, axes=None):
    alldata = np.concatenate(dataframe.iloc[filtered_index, :]["current_nA"].values)
    if findpeaks:
        alldata = -alldata
    points, destimate, peakinds, ind, peakparams, peakdata, levels = fit_many_gaussians(
        alldata
    )
    if axes:
        axes.clear()
        axes.set_autoscale_on(True)
        axes.set_xlabel("current (nA)")
        axes.set_ylabel("Prob. density")
        axes.plot(points, destimate, "b", marker=".", markersize=3, linewidth=0.5)
        #        axes.plot(points[ind], destimate[ind], marker='x', markersize=5, linestyle='None', color='g')
        axes.plot(
            points[peakinds],
            destimate[peakinds],
            marker="x",
            markersize=10,
            linestyle="None",
            color="m",
            mew=3,
        )
        #    reduced = destimate.copy()
        if peakparams is not None:
            for i, arr in enumerate(peakdata):
                axes.plot(
                    arr[0, :],
                    arr[1, :],
                    color="r",
                    marker=".",
                    markersize=3,
                    linewidth=0.5,
                )
        #                destimate[leftlist[i]:rightlist[i]+1] -= arr[2,:]
        #                axes.plot(points, destimate, color='g', marker='.', markersize=3, linewidth=0.5)
        axes.plot(
            levels[:, 0],
            levels[:, 1],
            marker="x",
            markersize=10,
            linestyle="None",
            color="k",
            mew=3,
        )
    if findpeaks:
        levels[:, 0] = -levels[:, 0]
        levels = np.array(sorted(levels, key=lambda x: x[0], reverse=False))
    return levels
