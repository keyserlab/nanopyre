#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Oct 15 19:37:13 2017


"""

from importlib import reload
import numpy as np

from nanopyre.analysis import (
    cusum,
    peakfinder,
    peakfinder_scipy,
    fold_threshold,
    threshold_volume,
)
from nanopyre.io.eventset import EventSet
from nanopyre.io.configio import read_config

if read_config()["Bayes nested sampling"]["On/Off"]:
    from nanopyre.analysis import bayes_nested_sampling

    analysis_methods = (
        cusum,
        peakfinder,
        peakfinder_scipy,
        fold_threshold,
        threshold_volume,
        bayes_nested_sampling,
    )
else:
    analysis_methods = (
        cusum,
        peakfinder,
        peakfinder_scipy,
        fold_threshold,
        threshold_volume,
    )


def analysis_wrapper(
    y,
    attrs,
    levels,
    analysis1,
    analysis2,
    eventset: EventSet,
    no,
    settings={},
    x=None,
    axesA=None,
    axesB=None,
    Reload=False,
    plotlevels=False,
):
    if Reload:
        for method in analysis_methods:
            reload(method)
    for axes in [axesA, axesB]:
        if axes:
            #            axes.clear()
            #            axes.set_autoscale_on(False)
            for line in axes.get_lines():
                line.remove()
            axes.set_xlabel("time (ms)")
            axes.set_ylabel("current (nA)")
        if plotlevels:
            l1 = levels[1][0]
            l2 = levels[2][0]
            std1 = levels[1][2]
            std2 = levels[2][2]
            axes.axhline(l1, 0, 1, color="m", linewidth=0.5)
            axes.axhline(l2, 0, 1, color="c", linewidth=0.5)
    if x is None:
        x = np.arange(y.size)
    try:
        std = eventset.get_dataframe(dsnames=[], attributes=["rms_nA"]).mean()
    except KeyError:
        misc = {}
    else:
        misc = {
            "std": std,
            "l1": levels[1][0],
            "l2": levels[2][0],
            "area_fC": attrs.get("area_fC", -1.0),
        }
    y, save1 = analysis_run(
        x, y, analysis1, eventset, no, misc=misc, settings=settings, axes=axesA
    )
    y, save2 = analysis_run(
        x, y, analysis2, eventset, no, misc=misc, settings=settings, axes=axesB
    )
    save = (save1, save2)
    return save


def analysis_run(
    x, y, analysis, eventset: EventSet, no, misc={}, settings={}, axes=None
):
    save = {}

    if analysis == "peaks":
        setts = settings.get("peaks", {})
        (
            peakinds,
            left_valleyinds,
            right_valleyinds,
            magnitudes,
            ind,
            peakloc,
        ) = peakfinder.peakfinder(
            x,
            y,
            setts.get("sel", 0.05),
            setts.get("threshold", 0.0),
            int(setts.get("extrema", -1)),
            setts.get("maxhrztlinddistance", 2),
            setts.get("maxhrztldistance", 5),
            setts.get("maxvertdistance", 0.1),
        )
        peakparams, peakdata, leftlist, rightlist, blines = peakfinder.peakmask(
            peakfinder.gauss,
            x,
            y,
            peakinds,
            ind,
            peakloc,
            int(setts.get("masksize", 5)),
            int(setts.get("extrema", -1)),
        )
        save["peaks"] = {
            "npeaks": peakinds.size,
            "peakinds": peakinds,
            "peakpos": x[peakinds],
            "magnitudes": magnitudes,
            "peakparams": peakparams,
        }
        if axes:
            axes.plot(x, y, marker=".", markersize=3, linewidth=0.5, color="b")
            axes.plot(
                x[peakinds],
                y[peakinds],
                marker="x",
                markersize=5,
                linestyle="None",
                color="r",
            )
            ycopy = y.copy()
            for i, arr in enumerate(peakdata):
                #                axes.plot(arr[0,:], arr[1,:], color='g', marker='.', markersize=3, linewidth=0.5)
                #        ccopy[left_valleyinds[i]:right_valleyinds[i]+1] -= arr[2,:]
                #        ccopy[leftlist[i]:rightlist[i]+1] -= arr[2,:]
                ycopy[leftlist[i] : rightlist[i] + 1] -= (
                    ycopy[leftlist[i] : rightlist[i] + 1] - blines[i]
                )
            axes.plot(x, ycopy, color="r", marker=".", markersize=3, linewidth=0.5)
            y = ycopy

    elif analysis == "scipy_peaks":
        setts = settings.get("scipy_peaks", {})
        peaks, properties = peakfinder_scipy.scipy_find_peaks(
            y * -1.0,
            setts["min_height"],
            setts["max_height"],
            setts["min_threshold"],
            setts["max_threshold"],
            setts["distance"],
            setts["min_prominence"],
            setts["max_prominence"],
            setts["min_width"],
            setts["max_width"],
            setts["wlen"],
            setts["rel_height"],
            setts["min_plateau_size"],
            setts["max_plateau_size"],
        )
        save["scipy_peaks"] = properties
        if axes:
            axes.plot(x, y, marker=".", markersize=3, linewidth=0.5, color="b")
            axes.plot(
                x[peaks],
                y[peaks],
                marker="x",
                markersize=5,
                linestyle="None",
                color="r",
            )

    elif analysis == "fold_threshold":
        setts = settings.get("fold_threshold", {})
        out, inds = fold_threshold.fold_threshold(
            y,
            setts.get("eventthreshold", misc.get("l1", -0.05)),
            setts.get("foldthreshold", misc.get("l2", -0.1)),
        )
        save["fold_threshold"] = {"indices": inds}
        if axes:
            axes.plot(x, y, marker=".", markersize=3, linewidth=0.5, color="b")
            axes.plot(x, out, color="r")

    elif analysis == "bayes one":
        setts = settings.get("bayes one", {})
        bayrun = bayes_nested_sampling.BayesRun(
            **read_config()["Bayes nested sampling"]["Algorithm settings"]
        )
        bayfuncs = bayes_nested_sampling.BayesFuncs(
            xvalues=x,
            data=y,
            level1=misc.get("l1", -0.05),
            level2=misc.get("l2", -0.1),
            noise=misc.get("std", 0.01),
        )
        bayrun.run(
            bayfuncs.OneEvent_base_, bayfuncs.OneEvent_, bayfuncs.OneEvent_prior_
        )
        out = bayrun.lastrun_basefunc(x, *bayrun.result["parameters"])
        save["bayes one"] = bayrun.result
        if axes:
            axes.plot(x, y, marker=".", markersize=3, linewidth=0.5, color="b")
            axes.plot(x, out, color="r")

    elif analysis == "bayes twoone":
        setts = settings.get("bayes twoone", {})
        bayrun = bayes_nested_sampling.BayesRun(
            **read_config()["Bayes nested sampling"]["Algorithm settings"]
        )
        bayfuncs = bayes_nested_sampling.BayesFuncs(
            xvalues=x,
            data=y,
            level1=misc.get("l1", -0.05),
            level2=misc.get("l2", -0.1),
            noise=misc.get("std", 0.01),
        )
        bayrun.run(
            bayfuncs.TwoOneEvent_base_,
            bayfuncs.TwoOneEvent_,
            bayfuncs.TwoOneEvent_prior_,
        )
        out = bayrun.lastrun_basefunc(x, *bayrun.result["parameters"])
        save["bayes twoone"] = bayrun.result
        if axes:
            axes.plot(x, y, marker=".", markersize=3, linewidth=0.5, color="b")
            axes.plot(x, out, color="r")

    elif analysis == "bayes one twoone":
        setts = settings.get("bayes one twoone", {})
        bayrun = bayes_nested_sampling.BayesRun(
            **read_config()["Bayes nested sampling"]["Algorithm settings"]
        )
        bayfuncs = bayes_nested_sampling.BayesFuncs(
            xvalues=x,
            data=y,
            level1=misc.get("l1", -0.05),
            level2=misc.get("l2", -0.1),
            noise=misc.get("std", 0.01),
        )
        bayrun.run(
            bayfuncs.OneEvent_base_, bayfuncs.OneEvent_, bayfuncs.OneEvent_prior_
        )
        out_one = bayrun.lastrun_basefunc(x, *bayrun.result["parameters"])
        result_one = bayrun.result["parameters"]
        logZ_one = bayrun.result["global_logz"]
        bayrun.run(
            bayfuncs.TwoOneEvent_base_,
            bayfuncs.TwoOneEvent_,
            bayfuncs.TwoOneEvent_prior_,
        )
        out_twoone = bayrun.lastrun_basefunc(x, *bayrun.result["parameters"])
        result_twoone = bayrun.result["parameters"]
        logZ_twoone = bayrun.result["global_logz"]
        save["bayes one twoone"] = {
            "One event logZ": logZ_one,
            "One event parameters": result_one,
            "Two-one event logZ": logZ_twoone,
            "Two-one parameters": result_twoone,
        }
        if axes:
            axes.plot(x, y, marker=".", markersize=3, linewidth=0.5, color="b")
            if (logZ_twoone - logZ_one) > setts.get("logZdiff", 10.0):
                axes.plot(x, out_one, color="k")
                axes.plot(x, out_twoone, color="r")
            else:
                axes.plot(x, out_twoone, color="k")
                axes.plot(x, out_one, color="r")

    elif analysis == "cusum":
        setts = settings.get("cusum", {})
        csm = cusum.cusum_steps(
            x,
            y,
            setts.get("threshold", 4000.0),
            delta=setts.get("delta", 0.05),
            sigma=setts.get("sigma", 0.005),
            debug=setts.get("debug", False),
        )
        save["cusum"] = {"stepinds": csm[0]}
        if axes:
            axes.plot(x, y, marker=".", markersize=3, linewidth=0.5, color="b")
            axes.plot(x, csm[1], color="r")

    elif analysis == "user":
        setts = settings.get("user", {})
        out, threshold = threshold_volume.percent_below(
            y, misc.get("l1", -0.05), misc.get("l2", -0.1), setts.get("sett1", 1)
        )
        if axes:
            axes.plot(x, y, marker=".", markersize=3, linewidth=0.5, color="b")
            axes.axhline(threshold[1], 0, 1, color="k", linewidth=0.5)
        save["user"] = {"Visible %": out}

    elif analysis == "blank":
        save["blank"] = {}

    elif analysis == "showfold":
        with eventset.get_bayes_file() as bayesfile:
            fit = eventset.get_bayes_fit(bayesfile, no)
        if axes:
            axes.plot(x, y, marker=".", markersize=3, linewidth=0.5, color="b")
            axes.plot(x, fit, color="r")
        save["showfold"] = {}

    elif analysis == "showfoldpeaks":
        with eventset.get_bayes_file() as bayesfile:
            fit = eventset.get_bayes_fit(bayesfile, no)
            peakpos = eventset.get_bayes_entry(bayesfile, no, "peak_pos")
            peakmag = eventset.get_bayes_entry(bayesfile, no, "peak_mag")
        #            orientation = eventset.getBayesEntry(bayesfile, no, 'orientation')
        if axes:
            axes.plot(x, y, marker=".", markersize=3, linewidth=0.5, color="b")
            axes.plot(x, fit, color="r")
            axes.plot(
                peakpos,
                peakmag,
                marker="x",
                markersize=5,
                linewidth=2,
                linestyle="None",
                color="r",
            )
        #        save['showfoldpeaks'] = {'Orientation': orientation}
        save["showfoldpeaks"] = {}

    elif analysis == "showfoldcluspeaks":
        with eventset.get_bayes_file() as bayesfile:
            fit = eventset.get_bayes_fit(bayesfile, no)
            peakpos = eventset.get_bayes_entry(bayesfile, no, "peak_pos")
            peakmag = eventset.get_bayes_entry(bayesfile, no, "peak_mag")
            bindex = eventset.get_bayes_entry(bayesfile, no, "clustered_peaks_bindex")
            folded = eventset.get_bayes_entry(bayesfile, no, "folded")
            #            above_logZ_cutoff = eventset.getBayesEntry(bayesfile, no, 'above_logZ_cutoff')
            n_clustered_peaks = eventset.get_bayes_entry(
                bayesfile, no, "n_clustered_peaks"
            )
        #            orientation = eventset.getBayesEntry(bayesfile, no, 'orientation')
        if axes:
            axes.plot(x, y, marker=".", markersize=3, linewidth=0.5, color="b")
            axes.plot(x, fit, color="r")
            axes.plot(
                peakpos[bindex],
                peakmag[bindex],
                marker="x",
                markersize=5,
                linewidth=2,
                linestyle="None",
                color="r",
            )
        #        save['showfoldpeaks'] = {'Orientation': orientation}
        save["showfoldcluspeaks"] = {
            "Folded": folded,
            "N clustered peaks": n_clustered_peaks,
        }

    return y, save
