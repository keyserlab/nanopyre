#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Sep 21 09:25:23 2017


"""

from PyQt5.QtWidgets import QSizePolicy, QVBoxLayout, QToolBar

from matplotlib.backends.backend_qt5agg import (
    FigureCanvasQTAgg as FigureCanvas,
    NavigationToolbar2QT as NavigationToolbar,
)
from matplotlib.figure import Figure


class MplFigure(FigureCanvas):
    def __init__(self, xlim=(-0.25, 3.0), limupdate=(False, False)):
        fig = Figure()
        fig.set_frameon(False)
        self.axes = fig.add_axes([0.15, 0.15, 0.8, 0.8])
        self.axes.set_xlabel("time (ms)")
        self.axes.set_ylabel("current (nA)")
        self.axes.set_xlim(*xlim)
        self.axes.set_ylim(-0.5, 0.05)
        self.axes.set_autoscalex_on(limupdate[0])
        self.axes.set_autoscaley_on(limupdate[1])

        (self.line,) = self.axes.plot(
            -1.0, 0
        )  # linewidth=1., color='b', marker='.', markersize=3)
        self.vline1 = self.axes.axvline(-1.0, 0, 1.0, color="r", linewidth=0.5)
        self.vline2 = self.axes.axvline(-1.0, 0, 1.0, color="r", linewidth=0.5)
        FigureCanvas.__init__(self, fig)
        FigureCanvas.setSizePolicy(self, QSizePolicy.Expanding, QSizePolicy.Expanding)

    def updateline(self, x, y):
        self.axes.draw_artist(self.axes.patch)
        self.line.set_data(x, y)
        self.axes.draw_artist(self.line)
        self.update()
        self.flush_events()

    def updatelinexlim(self, x, y):
        self.axes.set_xlim(x.min(), x.max())
        self.axes.draw_artist(self.axes.xaxis)
        self.line.set_data(x, y)
        self.draw()

    def vlines(self, x1, x2):
        self.vline1.set_xdata([x1, x1])
        self.vline2.set_xdata([x2, x2])
        self.axes.draw_artist(self.vline1)
        self.axes.draw_artist(self.vline2)
        self.update()
        self.flush_events()

    def blank(self):
        self.updateline(0.0, 0.0)
        self.vlines(-1.0, -1.0)


class MplBox(QToolBar):
    def __init__(self, xlim=(-0.25, 3.0), limupdate=(False, False)):
        super().__init__()
        self.canvas = MplFigure(xlim, limupdate)
        self.canvas.setMinimumSize(200, 150)
        toolbar = NavigationToolbar(self.canvas, self)
        self.vbox = QVBoxLayout()
        self.vbox.addWidget(self.canvas)
        self.vbox.addWidget(toolbar)


class MplCanvas(FigureCanvas):
    def __init__(self, subp):
        self.fig = Figure()
        self.fig.set_frameon(True)
        self.axlist = []
        xlimlist = [(-0.25, 2.5), (-0.25, 2.5), (-0.25, 2.5), (-0.25, 2.5)]
        ylimlist = [(-0.5, 0.01), (-0.5, 0.01), (-0.5, 0.01), (-0.5, 0.01)]
        for k, no in enumerate(subp):
            axes = self.fig.add_subplot(no)
            axes.set_xlim(xlimlist[k])
            axes.set_ylim(ylimlist[k])
            axes.set_autoscale_on(False)
            self.axlist.append(axes)
        FigureCanvas.__init__(self, self.fig)
        FigureCanvas.setSizePolicy(self, QSizePolicy.Expanding, QSizePolicy.Expanding)
        self.draw()

    def pick_axes(self, whichax):
        axes = self.axlist[whichax - 1]
        return axes

    def subplots_adjust(self, layout):
        self.fig.subplots_adjust(**layout)

    def clear_axes(self, whichax):
        axes = self.pick_axes(whichax)
        axes.clear()
        self.draw()

    def clear_all(self):
        for i in range(len(self.axlist)):
            self.clear_axes(i)
        self.draw()


class MplCanvasBox(QToolBar):
    def __init__(self, subp=[221, 222, 223, 224]):
        super().__init__()
        self.canvas = MplCanvas(subp)
        self.canvas.setMinimumSize(300, 200)
        toolbar = NavigationToolbar(self.canvas, self)
        self.box = QVBoxLayout()
        self.box.addWidget(self.canvas)
        self.box.addWidget(toolbar)
