#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Apr 12 13:16:07 2017


"""

from PyQt5.QtWidgets import (
    QDialog,
    QVBoxLayout,
    QFormLayout,
    QLineEdit,
    QPushButton,
    QMessageBox,
)
from PyQt5.QtCore import QSettings

import pyodbc as pyo

from nanopyre.io.sqlio import SqlIO
from nanopyre.io.configio import read_config


class SqlLoginDialog(QDialog, SqlIO):
    def __init__(self, parent, settingstag):
        QDialog.__init__(self, parent)
        self.setWindowTitle("Login")
        self.setts = QSettings("ProjectZ", settingstag)
        self.setts.setFallbacksEnabled(False)
        vbox = QVBoxLayout()
        loginform = QFormLayout()
        usn = QLineEdit(self.setts.value("usn", ""))
        pwd = QLineEdit()
        pwd.setEchoMode(2)
        loginform.addRow("Username", usn)
        loginform.addRow("Password", pwd)
        vbox.addLayout(loginform)
        ok = QPushButton("OK")
        ok.pressed.connect(lambda: self.loginandaccept(usn.text(), pwd.text()))
        vbox.addWidget(ok)
        self.setLayout(vbox)

    def loginandaccept(self, usn, pwd):
        try:
            loginstr = (
                "DSN=" + read_config()["SQL"]["DNS"] + ";UID=" + usn + ";PWD=" + pwd
            )
            self.cxn = pyo.connect(loginstr)
            self.usn = usn
            self.setts.setValue("usn", usn)
            self.accept()
        except (pyo.Error, pyo.ProgrammingError):
            QMessageBox.warning(
                self,
                "Login Error",
                "Can't log in. Possible reasons: firewall not open to IP address, username wrong, password wrong",
            )

    def error(self, errortype, message):
        QMessageBox.warning(self, errortype, message)

    def open(self):
        QDialog.open(self)
