#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Mar 28 10:19:43 2017


"""

from sys import argv

from PyQt5.QtWidgets import (
    QApplication,
    QMainWindow,
    QWidget,
    QVBoxLayout,
    QGroupBox,
    QHBoxLayout,
    QTableWidget,
    QTableWidgetItem,
    QSizePolicy,
    QPushButton,
    QFormLayout,
    QLineEdit,
    QMessageBox,
    QCheckBox,
    QFileDialog,
)
from PyQt5.QtCore import QSettings, QDir

from datetime import datetime
from os.path import splitext

from nanopyre.gui.sql_login_dialog import SqlLoginDialog
from nanopyre.io.sqlio import SqlError
from nanopyre.io.configio import read_config


class SqlConnector(QMainWindow):
    def __init__(self):
        super().__init__()
        self.main_widget = QWidget(self)
        self.setCentralWidget(self.main_widget)
        self.setWindowTitle("sqlconnector")
        self.activated = False
        self.login = SqlLoginDialog(self, "sqlconnector")
        self.login.accepted.connect(self.postlogin)
        self.login.rejected.connect(self.close)
        self.login.open()

    def postlogin(self):
        self.statusBar().showMessage("Logged in as " + self.login.usn)
        self.config = read_config()["SQL"]
        self.mlist = self.config["Measurement table"]
        self.alist = self.config["Analysis table"]
        self.headers, types = self.login.db_cols(self.mlist)
        self.headersa, _ = self.login.db_cols(self.alist)
        self.ioheaders = self.config["IO Headers"]
        self.ioheaderlist = self.ioheaders.split(",")
        self.ioheaderlist = [x.strip() for x in self.ioheaderlist]
        self.ioheadertypes = [types[self.headers.index(x)] for x in self.ioheaderlist]
        if not self.activated:
            self.conversiondict = {
                "varchar": lambda x: x,
                "date": lambda x: datetime.strptime(x, "%Y-%m-%d").date(),
                "smallint": int,
                "int": int,
                "float": float,
            }
            self.setWindowTitle("sqlconnector")
            self.statusBar()
            mainvbox = QVBoxLayout()
            dbio = QGroupBox(title="Database i/o")
            self.show_dbio(dbio)
            dbio.setSizePolicy(dbio.sizePolicy().horizontalPolicy(), QSizePolicy.Fixed)
            dbview = QGroupBox(title="Database viewer")
            self.show_dbview(dbview)
            mainvbox.addWidget(dbio)
            mainvbox.addWidget(dbview)
            self.main_widget.setLayout(mainvbox)
            self.read_settings()
            self.fill_tableview()
            self.grayout_switch_confirm()
        self.activated = True

    # GUI top bar

    def show_dbio(self, groupbox):
        self.tableio = QTableWidget(1, len(self.ioheaderlist), self)
        self.tableio.setHorizontalHeaderLabels(self.ioheaderlist)
        self.tableio.setVerticalHeaderItem(0, QTableWidgetItem(""))
        self.tableio.setSizePolicy(
            self.tableio.sizePolicy().horizontalPolicy(), QSizePolicy.Minimum
        )
        #        self.tableio.setMinimumHeight(self.tableio.verticalHeader().length()+self.tableio.horizontalHeader().height())
        #        self.tableio.setVerticalScrollBarPolicy()
        self.tableio.setFixedHeight(
            self.tableio.rowHeight(0) + self.tableio.horizontalHeader().height() + 10
        )
        self.clear_tableio()

        topbox = QHBoxLayout()
        findbutton = QPushButton("Find tag:")
        findbutton.clicked.connect(self.findfunc)
        self.find = QLineEdit()
        self.find.setFixedWidth(80)
        clear = QPushButton("Clear")
        clear.clicked.connect(self.clear_tableio)
        insert = QPushButton("Insert")
        insert.clicked.connect(self.insertfunc)
        update = QPushButton("Update")
        update.clicked.connect(self.updatefunc)
        remove = QPushButton("Remove")
        remove.clicked.connect(self.removefunc)
        autocomp = QPushButton("Autocomplete")
        autocomp.clicked.connect(self.autocompfunc)
        self.datepath = QCheckBox("Date in path")
        confirm = QPushButton("Confirm")
        confirm.clicked.connect(self.confirmfunc)
        cancel = QPushButton("Cancel")
        cancel.clicked.connect(self.cancelfunc)
        self.changebuttons = [findbutton, clear, insert, update, remove]
        self.confirmbuttons = [confirm, cancel]
        topbox.addWidget(findbutton)
        topbox.addWidget(self.find)
        topbox.addWidget(clear)
        topbox.addStretch(5)
        topbox.addWidget(insert)
        topbox.addWidget(update)
        topbox.addWidget(remove)
        topbox.addWidget(autocomp)
        topbox.addWidget(self.datepath)
        topbox.addStretch(5)
        topbox.addWidget(confirm)
        topbox.addWidget(cancel)
        bottombox = QHBoxLayout()
        bottombox.addWidget(self.tableio)
        vbox = QVBoxLayout()
        vbox.addLayout(topbox)
        vbox.addLayout(bottombox)
        groupbox.setLayout(vbox)

    def clear_tableio(self):
        for i in range(self.tableio.columnCount()):
            self.tableio.setItem(0, i, QTableWidgetItem(""))

    # SQL bits

    def findfunc(self):
        qstr = (
            "SELECT "
            + self.ioheaders
            + " FROM "
            + self.mlist
            + " WHERE tag = '"
            + self.find.text()
            + "';"
        )
        try:
            data = self.login.db_query(qstr)
        except SqlError:
            return
        else:
            for j, k in enumerate(data[0]):
                item = QTableWidgetItem(str(k))
                self.tableio.setItem(0, j, item)

    def insertfunc(self):
        try:
            parameters = self.changechecks("insert")
        except ValueError:
            return
        else:
            qstr = "INSERT INTO " + self.mlist + "(" + self.ioheaders + ") VALUES ("
            qstr += "?, " * len(self.ioheaderlist)
            qstr = qstr[:-2] + ")"
            try:
                self.login.db_input(qstr, parameters)
            except SqlError:
                return
            else:
                self.operation = "INSERT"
                self.postchecks()

    def updatefunc(self):
        try:
            parameters = self.changechecks("update")
        except ValueError:
            return
        else:
            qstr = "UPDATE " + self.mlist + " SET "
            for i, x in enumerate(self.ioheaderlist):
                qstr = qstr + x + "=?, "
            qstr = qstr[:-2] + " WHERE tag = ?"
            #            print(self.tableio.item(0,0).text())
            parameters.append(self.currenttag)
            try:
                self.login.db_input(qstr, parameters)
            except SqlError:
                return
            else:
                self.operation = "UPDATE"
                self.postchecks()

    def removefunc(self):
        try:
            self.changechecks("remove")
        except ValueError:
            return
        else:
            qstr = (
                "SELECT unid FROM "
                + self.mlist
                + " WHERE tag = '"
                + self.currenttag
                + "'"
            )
            try:
                unid = self.login.db_query(qstr)
            except SqlError:
                print(qstr)
                return
            else:
                unid = unid[0][0]
                qstr = (
                    "DELETE FROM "
                    + self.mlist
                    + " WHERE unid = ?; DELETE FROM "
                    + self.alist
                    + " WHERE unid = ?"
                )
                try:
                    self.login.db_input(qstr, [unid, unid])
                except SqlError:
                    return
                else:
                    self.operation = "DELETE"
                    self.postchecks()

    def postchecks(self):
        self.grayout_switch_change()
        self.grayout_switch_confirm()
        self.statusBar().showMessage(
            "Logged in as "
            + self.login.usn
            + ', "CONFIRM" OR "CANCEL" '
            + self.operation
            + ' OPERATION ON ENTRY WITH TAG "'
            + self.currenttag
            + '"'
        )

    def changechecks(self, mode):
        if (
            self.tableio.item(0, self.ioheaderlist.index("who")).text()
            != self.login.usn
            and self.login.usn != self.config["Admin user"]
        ):
            QMessageBox.warning(
                self, "Database Error", "Can't create entries for other users"
            )
            raise ValueError
        qstr = "SELECT DISTINCT tag FROM " + self.mlist
        try:
            tags = self.login.db_query(qstr)
        except SqlError:
            raise ValueError
        tags = [x[0] for x in tags]
        if mode == "insert":
            if self.tableio.item(0, self.ioheaderlist.index("tag")).text() in tags:
                QMessageBox.warning(
                    self,
                    "Database Error",
                    "An entry with this tag already exists, make it unique",
                )
                raise ValueError
        elif mode == "update" or mode == "remove":
            if self.tableio.item(0, self.ioheaderlist.index("tag")).text() not in tags:
                QMessageBox.warning(
                    self,
                    "Database Error",
                    "No entry with this tag can be found, make sure it exists",
                )
                raise ValueError
        parameters = []
        for i in range(len(self.ioheaderlist)):
            func = self.conversiondict[self.ioheadertypes[i]]
            try:
                parameters.append(func(self.tableio.item(0, i).text()))
            except ValueError:
                QMessageBox.warning(
                    self,
                    "Input Error",
                    'The entry in "'
                    + self.ioheaderlist[i]
                    + '" does not have the right type/format',
                )
                raise ValueError
        self.currenttag = parameters[0]
        return parameters

    def confirmfunc(self):
        self.login.db_commit()
        self.statusBar().showMessage(
            "Logged in as "
            + self.login.usn
            + ", "
            + self.operation
            + ' OPERATION SUCCESSFUL ON ENTRY WITH TAG "'
            + self.currenttag
            + '"'
        )
        self.grayout_switch_confirm()
        self.grayout_switch_change()
        self.fill_tableview()

    def cancelfunc(self):
        self.login.db_rollback()
        self.statusBar().showMessage(
            "Logged in as "
            + self.login.usn
            + ", "
            + self.operation
            + " OPERATION CANCELLED"
        )
        self.grayout_switch_confirm()
        self.grayout_switch_change()

    def autocompfunc(self):
        try:
            chipid = self.tableio.item(0, self.ioheaderlist.index("chip_id")).text()
            channel = self.tableio.item(0, self.ioheaderlist.index("channel")).text()
            voltage = self.tableio.item(0, self.ioheaderlist.index("voltage")).text()
            samplingrate = self.tableio.item(
                0, self.ioheaderlist.index("sampling_rate")
            ).text()
            chipid = chipid.split("_")
            tag = chipid[1] + chipid[3] + "_" + channel + "_" + voltage
            if not self.datepath.isChecked():
                path = (
                    "/"
                    + chipid[1]
                    + chipid[3]
                    + "/channel"
                    + channel
                    + "/"
                    + voltage
                    + "_"
                    + samplingrate
                    + "k/"
                )
            else:
                date = self.tableio.item(0, self.ioheaderlist.index("ddate")).text()
                date = date.split("-")
                date = date[0][2:] + date[1] + date[2]
                path = (
                    "/"
                    + date
                    + "/"
                    + chipid[1]
                    + chipid[3]
                    + "/channel"
                    + channel
                    + "/"
                    + voltage
                    + "_"
                    + samplingrate
                    + "k/"
                )
            self.tableio.setItem(
                0, self.ioheaderlist.index("tag"), QTableWidgetItem(str(tag))
            )
            self.tableio.setItem(
                0, self.ioheaderlist.index("ppath"), QTableWidgetItem(str(path))
            )
        except:
            return

    def dbview_query(self):
        colstr = self.colsline.text()
        orderby = self.sortline.text()
        wherestr = self.wherestr.text()
        if wherestr:
            wherestr = "WHERE " + wherestr + " "
        if colstr == "*":
            collist = self.headers[1:]
            collist.extend(self.headersa[2:])
            colstr = ""
            for x in self.headers[1:]:
                colstr = colstr + x + ", "
            for x in self.headersa[2:]:
                colstr = colstr + x + ", "
            colstr = colstr[:-2]
        else:
            collist = colstr.split(",")
            collist = [x.strip() for x in collist]
        qstr = (
            "SELECT "
            + colstr
            + " FROM "
            + self.mlist
            + " LEFT JOIN "
            + self.alist
            + " ON "
            + self.mlist
            + ".unid = "
            + self.alist
            + ".unid "
            + wherestr
            + "ORDER BY "
            + orderby
            + ";"
        )
        qstr = qstr.replace(" tag", " " + self.mlist + ".tag")
        try:
            data = self.login.db_query(qstr)
        except SqlError:
            return False, [], collist
        else:
            return True, data, collist

    # GUI table

    def show_dbview(self, groupbox):
        self.tableview = QTableWidget(0, 17, self)
        self.tableview.setAlternatingRowColors(True)
        left = QFormLayout()
        self.colsline = QLineEdit("*")
        self.colsline.returnPressed.connect(self.fill_tableview)
        self.sortline = QLineEdit("")
        self.sortline.returnPressed.connect(self.fill_tableview)
        left.addRow("Columns", self.colsline)
        left.addRow("Sort by", self.sortline)
        right = QFormLayout()
        self.wherestr = QLineEdit("")
        self.wherestr.returnPressed.connect(self.fill_tableview)
        right.addRow("Filter", self.wherestr)
        buttons = QHBoxLayout()
        tags2clipboard = QPushButton("Tags2Clipboard")
        tags2clipboard.clicked.connect(self.tags_to_clipboard)
        save2file = QPushButton("Save2File")
        save2file.clicked.connect(self.save_to_file)
        buttons.addStretch(10)
        buttons.addWidget(tags2clipboard)
        buttons.addWidget(save2file)
        right.addRow(buttons)

        topbox = QHBoxLayout()
        topbox.addLayout(left)
        topbox.addLayout(right)
        bottombox = QHBoxLayout()
        bottombox.addWidget(self.tableview)
        vbox = QVBoxLayout()
        vbox.addLayout(topbox)
        vbox.addLayout(bottombox)
        groupbox.setLayout(vbox)

    def fill_tableview(self):
        success, data, collist = self.dbview_query()
        if success:
            self.tableview.setRowCount(0)
            self.tableview.setColumnCount(len(collist))
            self.tableview.setHorizontalHeaderLabels(collist)
            for i, x in enumerate(data):
                self.tableview.insertRow(i)
                self.tableview.setVerticalHeaderItem(i, QTableWidgetItem(str(i + 1)))
                for j, k in enumerate(x):
                    if k is None:
                        k = ""
                    item = QTableWidgetItem(str(k))
                    self.tableview.setItem(i, j, item)

    # Export functions

    def tags_to_clipboard(self):
        copystr = ""
        for i in range(self.tableview.rowCount()):
            copystr = copystr + "'" + self.tableview.item(i, 0).text() + "'" + ", "
        copystr = copystr[:-2]
        clipboard = QApplication.clipboard()
        clipboard.setText(copystr)

    def save_to_file(self):
        success, data, _ = self.dbview_query()
        if success:
            container = []
            for row in data:
                line = ""
                for entry in row:
                    line = line + str(entry) + "\t"
                line = line[:-1] + "\n"
                container.append(line)
            savepath = QFileDialog.getSaveFileName(
                self, "Save as", QDir.homePath(), "Text files (*.txt)"
            )
            savepath = splitext(savepath[0])[0] + ".txt"
            try:
                with open(savepath, "w") as file:
                    file.writelines(container)
            except TypeError:
                #                print('Invalid path, file not saved')
                QMessageBox.warning(self, "I/O Error", "Invalid path, file not saved")

    # Persistence (settings) & GUI buttons active/inactive state

    def grayout_switch_change(self):
        for x in self.changebuttons:
            if x.isEnabled():
                x.setEnabled(False)
            else:
                x.setEnabled(True)

    def grayout_switch_confirm(self):
        for x in self.confirmbuttons:
            if x.isEnabled():
                x.setEnabled(False)
            else:
                x.setEnabled(True)

    def read_settings(self):
        setts = QSettings("nanopyre", "sqlconnector")
        setts.setFallbacksEnabled(False)
        fields = [self.colsline, self.sortline, self.wherestr]
        labels = ["columns", "sortby", "filter"]
        defaultfields = ["*", "ddate", ""]
        for i, x in enumerate(labels):
            x = self.login.usn + "_" + x
            fields[i].setText(setts.value(x, defaultfields[i]))

    def write_settings(self):
        setts = QSettings("nanopyre", "sqlconnector")
        setts.setFallbacksEnabled(False)
        fields = [self.colsline, self.sortline, self.wherestr]
        labels = ["columns", "sortby", "filter"]
        for i, x in enumerate(labels):
            x = self.login.usn + "_" + x
            setts.setValue(x, fields[i].text())

    # Close action

    def closeEvent(self, event):
        try:
            self.write_settings()
        except AttributeError:
            pass
        event.accept()


def run():
    app = QApplication(argv)
    bw = SqlConnector()
    bw.show()
    app.exec_()
