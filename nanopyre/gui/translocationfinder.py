#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Feb 19 08:17:31 2017


"""

from sys import argv
from PyQt5.QtWidgets import (
    QWidget,
    QGroupBox,
    QHBoxLayout,
    QFileDialog,
    QApplication,
    QSizePolicy,
    QVBoxLayout,
    QMainWindow,
    QMessageBox,
    QPushButton,
    QLineEdit,
    QLabel,
    QCheckBox,
    QFormLayout,
    QComboBox,
    QDialog,
    QProgressBar,
)
from PyQt5.QtCore import QTimer, QSettings
from PyQt5.QtGui import QFont, QKeySequence

from os import remove, makedirs
from os.path import exists, join, split
from glob import glob
import numpy as np
import h5py as h5
from datetime import date

from nanopyre.gui.sql_login_dialog import SqlLoginDialog
from nanopyre.gui.qt_matplotlib_wrapper import MplBox, MplCanvasBox
from nanopyre.analysis.current_trace import CurrentTrace as ct
from nanopyre.analysis.current_trace import (
    TraceError,
    EndOfLineError,
    RightEndError,
    LeftEndError,
)
from nanopyre.analysis.ecd_levels import filter_ecd, detect_levels, EcdLevelError
from nanopyre.analysis.poresizer import poresizer, SizerError
from nanopyre.io.eventset import EventSet, EventError
from nanopyre.io.configio import read_config
from nanopyre.io.sqlio import SqlError, TOError


class TranslocationFinder(QMainWindow):
    width = 80

    def __init__(self):
        super().__init__()
        self.corr_running = False
        self.displaylist = []
        self.main_widget = QWidget(self)
        self.setCentralWidget(self.main_widget)
        self.setWindowTitle("translocationfinder")
        self.statusBar()

        mainvbox = QVBoxLayout()
        self.io_buttons()
        self.fileio = QGroupBox(title="File i/o")
        self.show_fileio(self.fileio)
        self.fileio.setSizePolicy(
            self.fileio.sizePolicy().horizontalPolicy(), QSizePolicy.Fixed
        )
        self.sqlio = QGroupBox(title="SQL i/o")
        self.show_sqlio(self.sqlio)
        self.sqlio.setSizePolicy(
            self.sqlio.sizePolicy().horizontalPolicy(), QSizePolicy.Fixed
        )
        self.sqlio.setVisible(False)
        prog = QGroupBox(title="% analysed")
        self.show_prog(prog)
        prog.setSizePolicy(prog.sizePolicy().horizontalPolicy(), QSizePolicy.Fixed)
        topbox = QHBoxLayout()
        topbox.addWidget(self.fileio, stretch=3)
        topbox.addWidget(self.sqlio, stretch=3)
        topbox.addWidget(prog, stretch=1)
        traceplot = QGroupBox(title="Baseline")
        self.show_traceplot(traceplot)
        eventplot = QGroupBox(title="Event viewer")
        self.show_eventplot(eventplot)
        midbox = QHBoxLayout()
        midbox.addWidget(traceplot)
        midbox.addWidget(eventplot)
        tracesettings = QGroupBox(title="Baseline settings")
        self.show_tracesettings(tracesettings)
        tracesettings.setSizePolicy(
            tracesettings.sizePolicy().horizontalPolicy(), QSizePolicy.Fixed
        )
        eventsettings = QGroupBox(title="Event detection settings")
        self.show_eventsettings(eventsettings)
        eventsettings.setSizePolicy(
            eventsettings.sizePolicy().horizontalPolicy(), QSizePolicy.Fixed
        )
        bottombox = QHBoxLayout()
        bottombox.addWidget(tracesettings)
        bottombox.addWidget(eventsettings)
        mainvbox.addLayout(topbox)
        mainvbox.addLayout(midbox)
        mainvbox.addLayout(bottombox)
        self.main_widget.setLayout(mainvbox)

        self.grayout_switch_event()
        self.read_settings()
        self.login = SqlLoginDialog(self, "translocationfinder")
        self.login.rejected.connect(lambda: self.sqlchecksql.setChecked(False))
        self.ecd_levels_window = ExtraWindow(self, [211, 212])
        self.ecd_levels_window.setModal(True)
        self.poresize_window = ExtraWindow(self, [111])
        self.poresize_window.setModal(True)
        self.config = read_config()["SQL"]
        self.mlist = self.config["Measurement table"]
        self.alist = self.config["Analysis table"]

    # I/O bits

    def set_sourcedir(self):
        if self.sender().text() == self.input_button.text():
            inputpath = QFileDialog.getExistingDirectory(
                self, "Select directory containing TDMS files", self.input_line.text()
            )
            inputpath = join(inputpath, "")
            if inputpath:
                self.input_line.setText(inputpath)
        else:
            inputpath = QFileDialog.getExistingDirectory(
                self,
                "Select base directory for input folders",
                self.input_line_sql.text(),
            )
            inputpath = join(inputpath, "")
            if inputpath:
                self.input_line_sql.setText(inputpath)

    #        self.inputpath = inputpath

    def set_targetdir(self):
        if self.sender().text() == self.output_button.text():
            outputpath = QFileDialog.getExistingDirectory(
                self, "Select directory for output hdf5 files", self.output_line.text()
            )
            outputpath = join(outputpath, "")
            if outputpath:
                self.output_line.setText(outputpath)
        else:
            outputpath = QFileDialog.getExistingDirectory(
                self,
                "Select base directory for output folders",
                self.output_line_sql.text(),
            )
            outputpath = join(outputpath, "")
            if outputpath:
                self.output_line_sql.setText(outputpath)

    #        self.outputpath = outputpath

    def handle_outputdir(self):
        files = glob(self.outputpath + "*.hdf5")
        if exists(self.outputpath) and (files != []):
            answer = QMessageBox.question(
                self,
                "I/O",
                "Output directory already contains analysis files, " "clear contents?",
                QMessageBox.Yes | QMessageBox.No,
                QMessageBox.No,
            )
            if answer == QMessageBox.Yes:
                for x in files:
                    remove(x)
            else:
                return
        elif exists(self.outputpath) and (files == []):
            return
        else:
            makedirs(self.outputpath)

    # SQL bits

    def sql_login(self):
        #        self.login.exec_()
        #        self.login.show()
        self.login.open()

    def sql_close(self):
        self.login.closeconn()
        self.login.close()

    # Current trace logic

    def start_correction(self):
        if self.corr_running:
            return
        if self.sqlcheckfile.isChecked():
            self.tag = self.sqltag.text().strip()
            try:
                ppath = self.login.query(
                    self.mlist, [self.config["Filepath column"]], self.tag
                )
            except (SqlError, TOError):
                return
            else:
                ppath = ppath[0]
            self.inputpath = self.input_line_sql.text()[:-1] + ppath
            self.outputpath = (
                self.output_line_sql.text()
                + self.tag
                + "/"
                + self.output_line_sql_folder.text()
            )
        #            print(self.outputpath)
        else:
            self.tag = ""
            self.inputpath = join(self.input_line.text(), "")
            self.outputpath = join(self.output_line.text(), "")
        try:
            ipt = {
                "watchlen_ms": int(self.watchleninms.text()),
                "corrlen_ms": int(self.corrleninms.text()),
                "framerate_Hz": int(self.framerate.text()) * 1000,
            }
            self.trace = ct(self.inputpath, self.outputpath, **ipt)
        except (ValueError, IndexError):
            QMessageBox.warning(
                self,
                "I/O Error",
                "Can't read TDMS files, make sure input directory is valid, "
                "baseline settings are given and there are no corrupted .tmds "
                "files at the start of the recording",
            )
        else:
            try:
                self.handle_outputdir()
            except Exception as p:
                print(p)
                QMessageBox.warning(
                    self, "I/O Error", "Can't write to output directory, check path"
                )
                return
            self.corr_running = True
            self.update_event_settings()
            self.update_coarsegrain()
            self.progbar.setMaximum(len(self.trace.filelist))
            self.progbar.setMinimum(0)
            self.correct_one_window()
            self.grayout_switch_corr()
            self.grayout_switch_event()

    def next_correction(self):
        if not self.corr_running:
            QMessageBox.warning(
                self,
                "Runtime error",
                "Correction run has not been initialized, press 'Start' first",
            )
        elif self.autorun.isChecked():
            QMessageBox.warning(
                self,
                "Runtime error",
                "Autorun is still on, turn off for manual control",
            )
        else:
            self.correct_one_window()

    def loop_correction(self):
        if not self.corr_running:
            QMessageBox.warning(
                self,
                "Runtime error",
                "Correction run has not been initialized, press 'Start' first",
            )
        elif self.autorun.isChecked():
            self.corrtimer = QTimer()
            self.corrtimer.timeout.connect(self.correct_one_window)
            self.corrtimer.start()
        else:
            try:
                self.corrtimer.stop()
            except:
                QMessageBox.warning(
                    self, "Runtime error", "Check 'Autorun' again to start loop"
                )

    def correct_one_window(self, skip=False):
        if self.trace.endreached:
            self.trace.foundone = True
            self.statusBar().showMessage(
                self.statusBar().currentMessage()
                + ", END OF CORRECTION RUN, EXITED WITHOUT ERRORS"
            )
            self.stop()
            self.ecd_levels()
            self.poresize()
        else:
            self.trace.correct_one_window(skipped=skip)
            self.progbar.setValue(self.trace.lastfileindex + 1)
            self.statusBar().showMessage(
                "File "
                + str(self.trace.lastfileindex + 1)
                + " of "
                + str(len(self.trace.filelist))
                + ", "
                + str(self.trace.eventno)
                + " event(s) accepted"
            )
            self.mean.setText("{:.2f}".format(self.trace.mean))
            self.rms.setText("{:.4f}".format(self.trace.rms))
            if not self.notraceplot.isChecked():
                self.tracewidget.canvas.updatelinexlim(
                    self.trace.xtrace[:: self.coarsepts],
                    self.trace.ytrace[:: self.coarsepts],
                )

    def event_correction(self, atleastonce=True, noiselimit=False):
        if atleastonce:
            self.correct_one_window()
        if noiselimit:
            while self.trace.rms > float(self.maxrms.text()):
                self.correct_one_window(skip=True)

    # Event logic

    def one_event(self):
        try:
            if self.trace.rms > float(self.maxrms.text()):
                self.event_correction(False, True)
            while not self.trace.foundone and not self.trace.endreached:
                try:
                    self.trace.one_event()
                except (EndOfLineError, RightEndError):
                    self.event_correction(True, True)
                    continue
                except LeftEndError:
                    self.trace.startlookinghere += self.trace.stop
                    continue
            if self.trace.endreached:
                self.event_correction(True, False)

            self.area.setText("{:.2f}".format(self.trace.area))
            self.duration.setText("{:.2f}".format(self.trace.duration))
            self.trace.foundone = False
            if not self.noeventplot.isChecked():
                self.eventwidget.canvas.updateline(self.trace.xevent, self.trace.yevent)
            if not self.notraceplot.isChecked():
                self.tracewidget.canvas.vlines(
                    self.trace.xtrace[self.trace.start],
                    self.trace.xtrace[self.trace.stop],
                )
        except AttributeError:
            return

    def accept_event(self):
        try:
            self.trace.accept_current_event(
                self.writetofile.isChecked(), float(self.maxrms.text())
            )
        except TraceError:
            QMessageBox.warning(
                self,
                "Runtime error",
                "Press 'Next (Reject)' first to initialize event search",
            )
            try:
                self.acctimer.stop()
            except AttributeError:
                return
            return
        except AttributeError:
            return
        else:
            self.statusBar().showMessage(
                "File "
                + str(self.trace.lastfileindex + 1)
                + " of "
                + str(len(self.trace.filelist))
                + ", "
                + str(self.trace.eventno)
                + " event(s) accepted"
            )
            self.one_event()

    def reject_event(self):
        self.one_event()

    def keep_accepting(self):
        if self.keepaccepting.isChecked():
            self.acctimer = QTimer()
            self.acctimer.timeout.connect(self.accept_event)
            self.acctimer.start()
        else:
            self.acctimer.stop()

    def keep_rejecting(self):
        if self.keeprejecting.isChecked():
            self.rejtimer = QTimer()
            self.rejtimer.timeout.connect(self.reject_event)
            self.rejtimer.start()
        else:
            self.rejtimer.stop()

    # Loop convenience functions

    def fastloop(self):
        self.stop()
        self.notraceplot.setChecked(True)
        self.start_correction()
        self.noeventplot.setChecked(True)
        self.writetofile.setChecked(True)
        self.reject_event()
        self.keepaccepting.setChecked(True)

    def stop(self):
        self.autorun.setChecked(False)
        try:
            self.trace.save_summary(
                self.writetofile.isChecked(),
                (self.sqlcheckfile.isChecked(), self.login, self.tag),
            )
            del self.trace
        except AttributeError:
            pass
        else:
            self.corr_running = False
            self.grayout_switch_corr()
            self.grayout_switch_event()
            self.tracewidget.canvas.blank()
            self.eventwidget.canvas.blank()
            self.notraceplot.setChecked(False)
            self.noeventplot.setChecked(False)
            self.writetofile.setChecked(False)
            self.keepaccepting.setChecked(False)
            self.keeprejecting.setChecked(False)
            for x in self.displaylist:
                x.setText("")

    # ECD filter & current level calculation

    def ecd_levels(self):
        try:
            es = EventSet(self.outputpath + "events.hdf5", filterindex=False)
            df = es.get_dataframe(
                dsnames=["current_nA"], attributes=["eventno", "samplefreq_Hz"]
            )
            es.close()
            ecd_gaussfit, filtered_index = filter_ecd(
                df, axes=self.ecd_levels_window.canvas.pick_axes(1)
            )
        except EventError:
            QMessageBox.warning(
                self,
                "I/O Error",
                "Can't find events.hdf5 file, make sure you have loaded the right output directory",
            )
        except EcdLevelError:
            QMessageBox.warning(
                self,
                "Analysis Error",
                "Can't filter ECD, check the thresholds in file tf_ecd_levels.py",
            )
        else:
            self.ecd_levels_window.canvas.draw()
            if not self.ecd_levels_window.isVisible():
                self.ecd_levels_window.show()
            write = self.write_porechar_h5(
                self.outputpath + "pore_characterisation.hdf5",
                "ecd_gaussfit",
                ecd_gaussfit,
                '"ecd_gaussfit" entry already exists, overwrite?: ',
            )
            write = write & self.write_porechar_h5(
                self.outputpath + "pore_characterisation.hdf5",
                "filtered_index",
                filtered_index,
                '"filtered_index" entry already exists, overwrite?: ',
                resizablelen=True,
            )
            if write != 0 and self.sqlcheckfile.isChecked():
                try:
                    lis = [date.today(), self.login.usn, True]
                    lis.extend(ecd_gaussfit.tolist())
                    self.login.insertorupdate(
                        self.alist,
                        [
                            "ddatea",
                            "whoa",
                            "ecdfiltered",
                            "ecdmean",
                            "ecdamp",
                            "ecdwidth",
                        ],
                        lis,
                        self.tag,
                        (False, self.mlist),
                    )
                except TypeError as p:
                    print(p)
            try:
                levels = detect_levels(
                    df,
                    filtered_index,
                    self.findpeaks.isChecked(),
                    axes=self.ecd_levels_window.canvas.pick_axes(2),
                )
            except EcdLevelError:
                QMessageBox.warning(
                    self,
                    "Analysis Error",
                    "Can't detect levels, check the thresholds in file tf_ecd_levels.py",
                )
            except Exception as p:
                print(p)
                QMessageBox.warning(
                    self, "Analysis Error", "Can't detect levels, no output written"
                )
            else:
                self.ecd_levels_window.canvas.draw()
                write = self.write_porechar_h5(
                    self.outputpath + "pore_characterisation.hdf5",
                    "levels",
                    levels,
                    '"levels" entry already exists, overwrite?: ',
                )
                if write != 0 and self.sqlcheckfile.isChecked():
                    lis = [date.today(), self.login.usn, True]
                    lis.extend([y for x in levels[1:] for y in x])
                    outcome = self.login.insertorupdate(
                        self.alist,
                        [
                            "ddatea",
                            "whoa",
                            "levelsfound",
                            "level1mean",
                            "level1amp",
                            "level1width",
                            "level2mean",
                            "level2amp",
                            "level2width",
                        ],
                        lis,
                        self.tag,
                        (False, self.mlist),
                    )
                    if outcome == -1:
                        QMessageBox.warning(
                            self,
                            "SQL Error",
                            "Last entry not written to SQL database: timeout",
                        )

    def poresize(self):
        inputpath = split(self.inputpath[:-1])[0]
        paths = [
            glob(join(inputpath, "**/IV curve and noise.dat"), recursive=True),
            glob(join(inputpath, "**/IV_curve.txt"), recursive=True),
            sorted(glob(join(inputpath, "IV_*.hdf5"))),
        ]
        if len(paths[0]) != 0:
            try:
                data = np.loadtxt(paths[0][0], skiprows=1)
            except IOError:
                QMessageBox.warning(self, "I/O Error", "Can't find IV curve data file")
                return
            else:
                voltage = data[:, 0]
                current = data[:, 1]
        elif len(paths[1]) != 0:
            try:
                data = np.loadtxt(paths[1][0], skiprows=0)
            except IOError:
                QMessageBox.warning(self, "I/O Error", "Can't find IV curve data file")
                return
            else:
                voltage = data[:, 0]
                current = data[:, 1]
        elif len(paths[2]) != 0:
            try:
                file = h5.File(paths[2][0], "r")
                voltage = file["IV"][:, 0]
                current = file["IV"][:, 1]
                file.close()
            except IOError:
                QMessageBox.warning(self, "I/O Error", "Can't find IV curve data file")
                return
        else:
            QMessageBox.warning(self, "I/O Error", "Can't find IV curve data file")
            return
        if not self.poresize_window.isVisible():
            self.poresize_window.show()
        if self.sqlcheckfile.isChecked():
            pore, salt, salt_conc = self.login.query(
                self.mlist, ["pore", "salt", "salt_conc"], self.tag
            )
        else:
            qelements = {
                "combo": {"Salt": ("LiCl", "KCl", "NHSO", "NaCl")},
                "line": ("Pore", "Salt conc. (M)"),
            }
            qwindow = QuestionWindow(self, qelements)
            result = qwindow.exec()
            if result:
                try:
                    pore, salt, salt_conc = (
                        qwindow.dict["line"]["Pore"][1],
                        qwindow.dict["combo"]["Salt"][1],
                        float(qwindow.dict["line"]["Salt conc. (M)"][1]),
                    )
                except ValueError:
                    print("Wrong pore size inputs")
                    return
            else:
                return
        try:
            resohm, poresize, recratio = poresizer(
                current,
                voltage,
                pore,
                salt,
                salt_conc,
                axes=self.poresize_window.canvas.pick_axes(1),
            )
        except SizerError:
            QMessageBox.warning(
                self,
                "Pore Sizing Error",
                "IV curve not found or errors in pore size estimation",
            )
        else:
            self.poresize_window.canvas.draw()
            write = self.write_porechar_h5(
                self.outputpath + "pore_characterisation.hdf5",
                "resistance_ohm",
                resohm,
                '"resistance_ohm" entry already exists, overwrite?: ',
            )
            if write != 0 and self.sqlcheckfile.isChecked():
                self.login.insertorupdate(
                    self.alist,
                    ["ddatea", "whoa", "resistance"],
                    [date.today(), self.login.usn, resohm],
                    self.tag,
                    (False, self.mlist),
                )
            write = self.write_porechar_h5(
                self.outputpath + "pore_characterisation.hdf5",
                "poresize_nm",
                poresize,
                '"poresize_nm" entry already exists, overwrite?: ',
            )
            if write != 0 and self.sqlcheckfile.isChecked():
                self.login.insertorupdate(
                    self.alist,
                    ["ddatea", "whoa", "poresize"],
                    [date.today(), self.login.usn, poresize],
                    self.tag,
                    (False, self.mlist),
                )
            write = self.write_porechar_h5(
                self.outputpath + "pore_characterisation.hdf5",
                "recratio",
                recratio,
                '"recratio" entry already exists, overwrite?: ',
            )
            if write != 0 and self.sqlcheckfile.isChecked():
                outcome = self.login.insertorupdate(
                    self.alist,
                    ["ddatea", "whoa", "recratio"],
                    [date.today(), self.login.usn, recratio],
                    self.tag,
                    (False, self.mlist),
                )
                if outcome == -1:
                    QMessageBox.warning(
                        self,
                        "SQL Error",
                        "Last entry not written to SQL database: timeout",
                    )

    def write_porechar_h5(self, filepath, dspath, data, msg, resizablelen=False):
        write = 0
        try:
            file = h5.File(filepath, "r+")
        except IOError:
            file = h5.File(filepath, "a")
            write = 1
        else:
            try:
                file[dspath]
            except KeyError:
                write = 1
            else:
                answer = QMessageBox.question(
                    self,
                    "Pore Characterisation",
                    msg,
                    QMessageBox.Yes | QMessageBox.No,
                    QMessageBox.No,
                )
                if answer == QMessageBox.Yes:
                    write = 2
        if write == 1:
            if not resizablelen:
                dataset = file.create_dataset(dspath, data=data)
            else:
                dataset = file.create_dataset(dspath, data=data, maxshape=(None,))
        elif write == 2:
            dataset = file[dspath]
            if resizablelen:
                dataset.resize(data.shape)
            dataset[...] = data
        try:
            file.close()
        except NameError:
            pass
        return write

    # Communicate current trace settings

    def update_event_settings(self):
        update = [
            "threshold",
            "minduration",
            "minarea",
            "boundarythreshold",
            "maxduration",
            "maxarea",
            "pointseitherside",
        ]
        update = {
            item: float(self.eventsettings[i].text())
            for i, item in enumerate(update)
            if (self.eventsettings[i].text() != "")
        }
        update["findpeaks"] = self.findpeaks.isChecked()
        self.trace.update_settings(update)

    def update_coarsegrain(self):
        self.coarsepts = max(int(self.coarsegrain.text()), 1)

    # GUI buttons active/inactive state

    def grayout_switch_corr(self):
        add = [
            self.sqlcheckfile,
            self.sqlchecksql,
            self.sqltag,
            self.input_button,
            self.input_button_sql,
            self.output_button,
            self.output_button_sql,
            self.input_line,
            self.output_line,
            self.input_line_sql,
            self.output_line_sql,
            self.output_line_sql_folder,
        ]
        graylist = self.tracesettings + add
        for x in graylist:
            if x.isEnabled():
                x.setEnabled(False)
            else:
                x.setEnabled(True)

    def grayout_switch_event(self):
        for x in self.eventcontrols + self.eventsettings + [self.findpeaks]:
            if x.isEnabled():
                x.setEnabled(False)
            else:
                x.setEnabled(True)

    # Persistence (settings)

    def read_settings(self):
        setts = QSettings("nanopyre", "translocationfinder")
        setts.setFallbacksEnabled(False)
        fields = (
            self.tracesettings[0:2]
            + self.tracesettings[3:]
            + self.eventsettings
            + self.iosettings
        )
        labels = (
            self.tracesettingslabels[0:2]
            + self.tracesettingslabels[3:]
            + self.eventsettingslabels
            + self.iosettingslabels
        )
        for i, x in enumerate(labels):
            s = x.replace(" ", "_")
            fields[i].setText(setts.value(s, ""))

    def write_settings(self):
        setts = QSettings("nanopyre", "translocationfinder")
        setts.setFallbacksEnabled(False)
        fields = (
            self.tracesettings[0:2]
            + self.tracesettings[3:]
            + self.eventsettings
            + self.iosettings
        )
        labels = (
            self.tracesettingslabels[0:2]
            + self.tracesettingslabels[3:]
            + self.eventsettingslabels
            + self.iosettingslabels
        )
        for i, x in enumerate(labels):
            s = x.replace(" ", "_")
            setts.setValue(s, fields[i].text())

    # Close action

    def closeEvent(self, event):
        try:
            self.trace.closefiles()
        except AttributeError:
            pass
        try:
            self.ecd_levels_window.close()
        except:
            pass
        self.write_settings()
        event.accept()

    # GUI functions

    def show_fileio(self, groupbox):
        hbox = QHBoxLayout()
        hbox.addWidget(self.sqlcheckfile)
        hbox.addWidget(self.input_button, stretch=1)
        hbox.addWidget(self.input_line, stretch=3)
        hbox.addWidget(self.output_button, stretch=1)
        hbox.addWidget(self.output_line, stretch=3)
        groupbox.setLayout(hbox)

    def show_sqlio(self, groupbox):
        hbox = QHBoxLayout()
        hbox.addWidget(self.sqlchecksql)
        hbox.addWidget(self.sqltag)
        hbox.addWidget(self.input_button_sql, stretch=1)
        hbox.addWidget(self.input_line_sql, stretch=3)
        hbox.addWidget(self.output_button_sql, stretch=1)
        hbox.addWidget(self.output_line_sql, stretch=3)
        hbox.addWidget(self.taglabel)
        hbox.addWidget(self.output_line_sql_folder, stretch=2)
        groupbox.setLayout(hbox)

    def show_traceplot(self, groupbox):
        vbox = QVBoxLayout()
        topbox = QHBoxLayout()
        bottombox = QHBoxLayout()

        self.start = QPushButton("Start")
        self.start.clicked.connect(self.start_correction)
        stop = QPushButton("Stop")
        stop.clicked.connect(self.stop)
        nxt = QPushButton("Next")
        nxt.clicked.connect(self.next_correction)
        nxt.setShortcut(QKeySequence.MoveToNextChar)
        fastloop = QPushButton("Fastloop")
        fastloop.clicked.connect(self.fastloop)
        fastloop.setShortcut(QKeySequence.MoveToPreviousChar)
        self.autorun = QCheckBox("Autorun")
        self.autorun.stateChanged.connect(self.loop_correction)
        self.autorun.setFixedWidth(self.width * 1.3)
        self.notraceplot = QCheckBox("Turn off plot")
        self.notraceplot.setFixedWidth(self.width * 1.3)

        coarsedisp = QFormLayout()
        self.coarsegrain = QLineEdit("1")
        self.coarsegrain.editingFinished.connect(self.update_coarsegrain)
        self.coarsegrain.setFixedWidth(self.width * 0.5)
        label = QLabel("Coarsegrain")
        coarsedisp.addRow(label, self.coarsegrain)
        font = QFont(self.start.fontInfo().family())
        font.setBold(True)
        meandisp = QFormLayout()
        self.mean = QLineEdit("")
        self.mean.setReadOnly(True)
        self.mean.setFixedWidth(self.width)
        label = QLabel("MEAN (nA)")
        self.mean.setFont(font)
        label.setFont(font)
        meandisp.addRow(label, self.mean)
        self.displaylist.append(self.mean)

        rmsdisp = QFormLayout()
        self.rms = QLineEdit("")
        self.rms.setReadOnly(True)
        self.rms.setFixedWidth(self.width)
        label = QLabel("RMS (nA)")
        self.rms.setFont(font)
        label.setFont(font)
        rmsdisp.addRow(label, self.rms)
        self.displaylist.append(self.rms)

        topbox.addWidget(self.start)
        topbox.addWidget(nxt)
        topbox.addWidget(self.notraceplot)
        topbox.addLayout(coarsedisp)
        topbox.addStretch(10)
        topbox.addLayout(meandisp)
        bottombox.addWidget(stop)
        bottombox.addWidget(fastloop)
        bottombox.addWidget(self.autorun)
        bottombox.addStretch(10)
        bottombox.addLayout(rmsdisp)

        vbox.addLayout(topbox)
        vbox.addLayout(bottombox)
        vbox.addSpacing(10)
        self.tracewidget = MplBox((0.0, 1000.0), (False, False))
        vbox.addLayout(self.tracewidget.vbox)
        groupbox.setLayout(vbox)

    def show_eventplot(self, groupbox):
        vbox = QVBoxLayout()
        topbox = QHBoxLayout()
        bottombox = QHBoxLayout()

        accept = QPushButton("Accept")
        accept.clicked.connect(self.accept_event)
        accept.setShortcut(QKeySequence.MoveToPreviousLine)
        accept.setFixedWidth(self.width * 1.3)
        self.keepaccepting = QCheckBox("Keep accepting")
        self.keepaccepting.setFixedWidth(self.width * 1.4)
        self.keepaccepting.stateChanged.connect(self.keep_accepting)
        reject = QPushButton("Next (Reject)")
        reject.clicked.connect(self.reject_event)
        reject.setShortcut(QKeySequence.MoveToNextLine)
        reject.setFixedWidth(self.width * 1.3)
        self.keeprejecting = QCheckBox("Keep rejecting")
        self.keeprejecting.setFixedWidth(self.width * 1.4)
        self.keeprejecting.stateChanged.connect(self.keep_rejecting)
        self.noeventplot = QCheckBox("Turn off plot")
        self.noeventplot.setFixedWidth(self.width * 1.3)
        self.writetofile = QCheckBox("Write to file")
        self.writetofile.setFixedWidth(self.width * 1.3)
        ecd_levels = QPushButton("ECD / levels")
        ecd_levels.clicked.connect(self.ecd_levels)
        poresize = QPushButton("Pore size")
        poresize.clicked.connect(self.poresize)
        font = QFont(accept.fontInfo().family())
        font.setBold(True)
        areadisp = QFormLayout()
        self.area = QLineEdit("")
        self.area.setReadOnly(True)
        self.area.setFixedWidth(self.width)
        label = QLabel("AREA (fC)")
        self.area.setFont(font)
        label.setFont(font)
        areadisp.addRow(label, self.area)
        self.displaylist.append(self.area)

        durdisp = QFormLayout()
        self.duration = QLineEdit("")
        self.duration.setReadOnly(True)
        self.duration.setFixedWidth(self.width)
        label = QLabel("DURATION (ms)")
        self.duration.setFont(font)
        label.setFont(font)
        durdisp.addRow(label, self.duration)
        self.displaylist.append(self.duration)

        topbox.addWidget(accept)
        topbox.addWidget(self.keepaccepting)
        topbox.addWidget(self.noeventplot)
        topbox.addWidget(ecd_levels)
        topbox.addStretch(10)
        topbox.addLayout(areadisp)
        bottombox.addWidget(reject)
        bottombox.addWidget(self.keeprejecting)
        bottombox.addWidget(self.writetofile)
        bottombox.addWidget(poresize)
        bottombox.addStretch(10)
        bottombox.addLayout(durdisp)

        self.eventcontrols = [
            accept,
            reject,
            self.keepaccepting,
            self.keeprejecting,
            self.noeventplot,
            self.writetofile,
            ecd_levels,
            poresize,
        ]
        vbox.addLayout(topbox)
        vbox.addLayout(bottombox)
        vbox.addSpacing(10)
        self.eventwidget = MplBox((-0.25, 3.0), (False, False))
        vbox.addLayout(self.eventwidget.vbox)
        groupbox.setLayout(vbox)

    def show_tracesettings(self, groupbox):
        vbox = QVBoxLayout()
        setts = QHBoxLayout()
        leftsett = QFormLayout()

        self.watchleninms = QLineEdit("500")
        self.corrleninms = QLineEdit("3000")
        leftsettslist = [self.watchleninms, self.corrleninms]
        leftsettslabels = ["Display (ms)", "Correction duration (ms)"]
        counter = 0
        for x in leftsettslist:
            x.setFixedWidth(self.width)
            leftsett.addRow(leftsettslabels[counter], x)
            counter += 1
        for i in [1, 2]:
            placeholder = QLineEdit("")
            placeholder.setFixedWidth(self.width)
            placeholder.setEnabled(False)
            leftsett.addRow("", placeholder)
        setts.addLayout(leftsett)

        rightsett = QFormLayout()
        self.mode = QComboBox()
        self.mode.addItem("Mean")
        self.framerate = QLineEdit("250")
        rightsettslist = [self.mode, self.framerate]
        rightsettslabels = ["Correction mode", "Framerate (kHz)"]
        counter = 0
        for x in rightsettslist:
            x.setFixedWidth(self.width)
            rightsett.addRow(rightsettslabels[counter], x)
            counter += 1
        for i in [1, 2]:
            placeholder = QLineEdit("")
            placeholder.setFixedWidth(self.width)
            placeholder.setEnabled(False)
            rightsett.addRow("", placeholder)
        setts.addLayout(rightsett)

        self.tracesettings = leftsettslist + rightsettslist
        self.tracesettingslabels = leftsettslabels + rightsettslabels
        vbox.addLayout(setts)
        groupbox.setLayout(vbox)

    def show_eventsettings(self, groupbox):
        vbox = QVBoxLayout()
        setts = QHBoxLayout()
        self.findpeaks = QCheckBox("Detect peaks")
        self.findpeaks.setFixedWidth(self.width * 1.7)
        self.findpeaks.stateChanged.connect(self.update_event_settings)
        setts.addWidget(self.findpeaks)

        leftsett = QFormLayout()
        leftsettslist = [QLineEdit("-0.2"), QLineEdit("0.2"), QLineEdit("50")]
        leftsettslabels = ["Threshold (nA)", "Min duration (ms)", "Min area (fC)"]
        counter = 0
        for x in leftsettslist:
            x.setFixedWidth(self.width)
            x.editingFinished.connect(self.update_event_settings)
            leftsett.addRow(leftsettslabels[counter], x)
            counter += 1
        self.maxrms = QLineEdit("0.1")
        self.maxrms.setFixedWidth(self.width)
        leftsett.addRow("Max RMS (nA)", self.maxrms)
        setts.addLayout(leftsett)

        rightsett = QFormLayout()
        rightsettslist = [
            QLineEdit("0."),
            QLineEdit("2.5"),
            QLineEdit("400"),
            QLineEdit("20"),
        ]
        rightsettslabels = [
            "Return threshold (nA)",
            "Max duration (ms)",
            "Max area (fC)",
            "Points added (#)",
        ]
        counter = 0
        for x in rightsettslist:
            x.setFixedWidth(self.width)
            x.editingFinished.connect(self.update_event_settings)
            rightsett.addRow(rightsettslabels[counter], x)
            counter += 1
        setts.addLayout(rightsett)

        self.eventsettings = leftsettslist + rightsettslist + [self.maxrms]
        self.eventsettingslabels = leftsettslabels + rightsettslabels + ["Max RMS (nA)"]
        vbox.addLayout(setts)
        groupbox.setLayout(vbox)

    def io_buttons(self):
        self.sqlcheckfile = QCheckBox("Use SQL")
        self.sqlcheckfile.stateChanged.connect(self.switch_fileio)
        self.input_button = QPushButton("Input dir")
        self.input_button.clicked.connect(self.set_sourcedir)
        self.input_line = QLineEdit()
        self.output_button = QPushButton("Output dir")
        self.output_button.clicked.connect(self.set_targetdir)
        self.output_line = QLineEdit()
        self.sqlchecksql = QCheckBox("Use SQL")
        self.sqlchecksql.stateChanged.connect(self.switch_sqlio)
        self.sqltag = QLineEdit()
        self.sqltag.setFixedWidth(80)
        self.sqltag.textChanged.connect(self.tag_update)
        self.input_button_sql = QPushButton("Input base dir")
        self.input_button_sql.clicked.connect(self.set_sourcedir)
        self.input_line_sql = QLineEdit()
        self.output_button_sql = QPushButton("Output base dir")
        self.output_button_sql.clicked.connect(self.set_targetdir)
        self.output_line_sql = QLineEdit()

        self.taglabel = QLabel()
        self.output_line_sql_folder = QLineEdit()
        self.iosettings = [
            self.input_line,
            self.output_line,
            self.input_line_sql,
            self.output_line_sql,
            self.output_line_sql_folder,
            self.sqltag,
        ]
        self.iosettingslabels = [
            "input line",
            "output line",
            "input line sql",
            "output line sql",
            "output line sql folder",
            "sqltag",
        ]

    def switch_fileio(self, state):
        if state == 2:
            self.sqlchecksql.setChecked(True)
            self.fileio.setVisible(False)
            self.sqlio.setVisible(True)
            self.sql_login()

    def switch_sqlio(self, state):
        if state == 0:
            self.sqlcheckfile.setChecked(False)
            self.fileio.setVisible(True)
            self.sqlio.setVisible(False)
            self.sql_close()

    def tag_update(self, text):
        self.taglabel.setText(text + "/")

    def show_prog(self, groupbox):
        self.progbar = QProgressBar(self.main_widget)
        self.progbar.setValue(0)
        hbox = QHBoxLayout()
        hbox.addWidget(self.progbar)
        groupbox.setLayout(hbox)


class ExtraWindow(QDialog):
    def __init__(self, parent, subp):
        super().__init__(parent)
        self.plotwidget = MplCanvasBox(subp)
        self.canvas = self.plotwidget.canvas
        self.setLayout(self.plotwidget.box)
        self.setWindowTitle("translocationfinder - plot")

    def closeEvent(self, event):
        self.canvas.clear_all()
        event.accept()


class QuestionWindow(QDialog):
    def __init__(self, parent, elements):
        super().__init__(parent)
        self.dict = {}
        layout = self.fillwindow(elements)
        self.setLayout(layout)
        self.setWindowTitle("translocationfinder - pore size settings")

    def fillwindow(self, elements):
        box = QVBoxLayout()
        self.dict["combo"] = {}
        for key, values in sorted(elements["combo"].items()):
            hbox = QHBoxLayout()
            label = QLabel(key)
            combobox = QComboBox()
            for v in values:
                combobox.addItem(v)
            self.dict["combo"][key] = [combobox, None]
            hbox.addWidget(label)
            hbox.addWidget(combobox)
            box.addLayout(hbox)
        self.dict["line"] = {}
        form = QFormLayout()
        for item in sorted(elements["line"]):
            label = QLabel(item)
            line = QLineEdit("")
            self.dict["line"][item] = [line, None]
            form.addRow(label, line)
        box.addLayout(form)

        ok = QPushButton("OK")
        ok.clicked.connect(self.accept)
        box.addWidget(ok)
        return box

    def accept(self):
        for key in self.dict["combo"]:
            t = self.dict["combo"][key]
            t[1] = t[0].currentText()
        for key in self.dict["line"]:
            t = self.dict["line"][key]
            t[1] = t[0].text()
        super().accept()


def run():
    app = QApplication(argv)
    bw = TranslocationFinder()
    bw.show()
    app.exec_()
