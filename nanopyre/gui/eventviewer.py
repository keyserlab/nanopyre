#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed May 17 08:21:35 2017


"""

from sys import argv
from PyQt5.QtWidgets import (
    QWidget,
    QGroupBox,
    QHBoxLayout,
    QFileDialog,
    QApplication,
    QSizePolicy,
    QVBoxLayout,
    QMainWindow,
    QMessageBox,
    QPushButton,
    QLineEdit,
    QLabel,
    QFormLayout,
    QTextEdit,
    QCheckBox,
    QProgressBar,
    QComboBox,
    QStackedWidget,
)
from PyQt5.QtCore import QSettings
from PyQt5.QtGui import QFont, QKeySequence

from os.path import join
import numpy as np

from nanopyre.gui.qt_matplotlib_wrapper import MplBox, MplCanvasBox
from nanopyre.analysis.analysis_wrapper import analysis_wrapper
from nanopyre.io.configio import read_config, exprt
from nanopyre.io.eventset import EventSet, EventError


class EventViewer(QMainWindow):
    def __init__(self):
        super().__init__()
        self.displaylist = []
        self.main_widget = QWidget(self)
        self.setCentralWidget(self.main_widget)
        self.setWindowTitle("eventviewer")
        self.statusBar()

        mainvbox = QVBoxLayout()
        self.io_buttons()
        self.graylist = [
            self.sqlcheckfile,
            self.input_button,
            self.input_line,
            self.sqlchecksql,
            self.sqltag,
            self.input_button_sql,
            self.input_line_sql,
            self.output_line_sql_folder,
        ]
        self.fileio = QGroupBox(title="File i/o")
        self.show_fileio(self.fileio)
        self.fileio.setSizePolicy(
            self.fileio.sizePolicy().horizontalPolicy(), QSizePolicy.Fixed
        )
        self.sqlio = QGroupBox(title="SQL i/o")
        self.show_sqlio(self.sqlio)
        self.sqlio.setSizePolicy(
            self.sqlio.sizePolicy().horizontalPolicy(), QSizePolicy.Fixed
        )
        self.sqlio.setVisible(False)
        io_options = QGroupBox("i/o options")
        self.show_io_options(io_options)
        io_options.setSizePolicy(
            self.sqlio.sizePolicy().horizontalPolicy(), QSizePolicy.Fixed
        )
        prog = QGroupBox(title="% of files")
        self.show_prog(prog)
        prog.setSizePolicy(prog.sizePolicy().horizontalPolicy(), QSizePolicy.Fixed)

        topbox = QHBoxLayout()
        topbox.addWidget(self.fileio, stretch=2.8)
        topbox.addWidget(self.sqlio, stretch=2.8)
        topbox.addWidget(io_options, stretch=0.2)
        topbox.addWidget(prog, stretch=1)
        eventplot = QGroupBox(title="Event viewer")
        self.show_eventviewer(eventplot)
        eventdet = QGroupBox(title="Event details")
        self.show_eventdetails(eventdet)
        midbox = QHBoxLayout()
        midbox.addWidget(eventplot, stretch=3)
        midbox.addWidget(eventdet, stretch=1)
        mainvbox.addLayout(topbox)
        mainvbox.addLayout(midbox)
        self.main_widget.setLayout(mainvbox)
        self.fileisopen = False

        for x in self.viewercontrols:
            if x.isEnabled():
                x.setEnabled(False)
            else:
                x.setEnabled(True)
        self.analysis_window = AnalysisWindow(
            self.nextevent, self.previousevent, self.get_fileroot
        )
        self.read_settings()

    # I/O and SQL bits

    def set_sourcedir(self):
        if self.sender().text() == self.input_button.text():
            inputpath = QFileDialog.getOpenFileName(
                self,
                "Select events.hdf5 file",
                self.input_line.text(),
                ".hdf5 files (*.hdf5)",
            )
            #            inputpath = join(inputpath[0], '')
            if inputpath[0]:
                self.input_line.setText(inputpath[0])
        else:
            inputpath = QFileDialog.getExistingDirectory(
                self, "Select root path for tag-folders", self.input_line_sql.text()
            )
            inputpath = join(inputpath, "")
            if inputpath:
                self.input_line_sql.setText(inputpath)

    def set_targetdir(self):
        pass

    def sql_login(self):
        pass

    def sql_close(self):
        pass

    def open_file(self):
        path = self.input_line.text()
        self.open_events(path)

    def open_sql(self):
        path = (
            self.input_line_sql.text()
            + self.sqltag.text()
            + "/"
            + self.output_line_sql_folder.text()
        )
        self.open_events(path)

    def close_file(self):
        if self.fileisopen:
            self.eventset.close()
            self.grayout_switch_io()
            self.fileisopen = False
            self.eventwidget.canvas.blank()
            self.eventdetails.clear()
            self.area.clear()
            self.duration.clear()
        else:
            pass

    def get_fileroot(self):
        if self.fileisopen:
            return self.eventset.root
        else:
            return None

    # Event sequence logic

    def open_events(self, path):
        try:
            self.eventset = EventSet(path, self.usefindex.isChecked())
        except EventError:
            QMessageBox.warning(
                self,
                "I/O error",
                "events.hdf5 or pore_characterisation.hdf5 does not exist or is "
                "open in a different application.",
            )
            return
        else:
            self.fileisopen = True
            self.currenteventno = 0
            self.grayout_switch_io()
            self.progbar.setMaximum(self.eventset.noevents)
            self.progbar.setMinimum(0)
            try:
                self.frequency = self.eventset.get_event_frequency()
            except EventError:
                self.frequency = (
                    self.eventset.noevents / self.eventset.laststart_ms * 1000.0
                )
            self.one_event(1)

    def one_event(self, no):
        try:
            current, attrs = self.eventset.get_event_data(no)
        except EventError:
            QMessageBox.information(
                self, "Out of bounds", str(no) + " is not a valid event number."
            )
            return
        else:
            self.currenteventno = no
            self.progbar.setValue(no)
            self.statusBar().showMessage(
                "Showing event "
                + str(no)
                + " of "
                + str(self.eventset.noevents)
                + ", index in file is "
                + str(self.eventset.get_event_index(no))
            )
            self.goto.setText(str(no))
            dets = ""
            for a in sorted(attrs.keys()):
                dets = dets + a + ": " + str(attrs[a]) + "\n"
            dets += "\n"
            try:
                self.area.setText("{:.2f}".format(attrs["area_fC"]))
                self.duration.setText("{:.2f}".format(attrs["duration_ms"]))
                xevent = np.arange(len(current)) / attrs["samplefreq_Hz"] * 1000.0
                dets += (
                    "Event frequency (Hz): " + "{:.3f}".format(self.frequency) + "\n"
                )
            except KeyError:
                # File lacks metadata, assuming sampling frequency of 250 kHz
                xevent = np.arange(len(current)) / 250.0
            if self.analysis_window.isVisible():
                try:
                    self.analysis_window.analyse(
                        self.eventset, no, xevent, current, attrs, self.eventset.levels
                    )
                except Exception as p:
                    print(p)
            self.eventdetails.clear()
            self.eventdetails.insertPlainText(dets[:-2])
            self.eventwidget.canvas.updateline(xevent, current)

    def gotoevent(self):
        no = max(int(self.goto.text()), 1)
        self.one_event(no)

    def nextevent(self):
        no = max(self.currenteventno + 1, 1)
        self.one_event(no)

    def previousevent(self):
        no = max(self.currenteventno - 1, 1)
        self.one_event(no)

    def load_analysis(self):
        if not self.analysis_window.isVisible():
            self.analysis_window.show()
        try:
            self.analysis_window.reload = True
            no = max(int(self.goto.text()), 1)
            self.one_event(no)
        except Exception as p:
            print(p)

    # Persistence (settings)

    def read_settings(self):
        setts = QSettings("nanopyre", "eventviewer")
        setts.setFallbacksEnabled(False)
        fields = self.iosettings
        labels = self.iosettingslabels
        for i, x in enumerate(labels):
            s = x.replace(" ", "_")
            fields[i].setText(setts.value(s, ""))

    def write_settings(self):
        setts = QSettings("nanopyre", "eventviewer")
        setts.setFallbacksEnabled(False)
        fields = self.iosettings
        labels = self.iosettingslabels
        for i, x in enumerate(labels):
            s = x.replace(" ", "_")
            setts.setValue(s, fields[i].text())

    # GUI buttons active/inactive state

    def grayout_switch_io(self):
        gray = self.graylist + self.viewercontrols
        for x in gray:
            if x.isEnabled():
                x.setEnabled(False)
            else:
                x.setEnabled(True)

    # Close action

    def closeEvent(self, event):
        self.close_file()
        self.write_settings()
        try:
            self.analysis_window.close()
        except:
            pass
        event.accept()

    # GUI functions

    def show_fileio(self, groupbox):
        hbox = QHBoxLayout()
        self.sqlcheckfile.setText("Use tag:")
        hbox.addWidget(self.sqlcheckfile)
        self.input_button.setText("Input file")
        hbox.addWidget(self.input_button, stretch=1)
        hbox.addWidget(self.input_line, stretch=3)
        op = QPushButton("Open")
        op.clicked.connect(self.open_file)
        hbox.addWidget(op)
        cl = QPushButton("Close")
        cl.setEnabled(False)
        cl.clicked.connect(self.close_file)
        hbox.addWidget(cl)
        self.graylist.append(op)
        self.graylist.append(cl)
        groupbox.setLayout(hbox)

    def show_sqlio(self, groupbox):
        hbox = QHBoxLayout()
        self.sqlchecksql.setText("Use tag:")
        hbox.addWidget(self.sqlchecksql)
        hbox.addWidget(self.sqltag)
        hbox.addWidget(self.input_button_sql, stretch=1)
        hbox.addWidget(self.input_line_sql, stretch=3)
        hbox.addWidget(self.taglabel, stretch=1)
        hbox.addWidget(self.output_line_sql_folder, stretch=3)
        op = QPushButton("Open")
        op.clicked.connect(self.open_sql)
        hbox.addWidget(op)
        cls = QPushButton("Close")
        cls.setEnabled(False)
        cls.clicked.connect(self.close_file)
        hbox.addWidget(cls)
        self.graylist.append(op)
        self.graylist.append(cls)
        groupbox.setLayout(hbox)

    def show_io_options(self, groupbox):
        hbox = QHBoxLayout()
        self.usefindex = QCheckBox("Use filtered index")
        hbox.addWidget(self.usefindex)
        self.graylist.append(self.usefindex)
        groupbox.setLayout(hbox)

    def show_eventviewer(self, groupbox):
        width = 80
        vbox = QVBoxLayout()
        topbox = QHBoxLayout()
        bottombox = QHBoxLayout()

        nxt = QPushButton("Next")
        nxt.clicked.connect(self.nextevent)
        nxt.setShortcut(QKeySequence.MoveToNextChar)
        previous = QPushButton("Previous")
        previous.clicked.connect(self.previousevent)
        previous.setShortcut(QKeySequence.MoveToPreviousChar)
        reload = QPushButton("(Re)load analysis module")
        reload.clicked.connect(self.load_analysis)

        gotodisp = QFormLayout()
        self.goto = QLineEdit("1")
        self.goto.returnPressed.connect(self.gotoevent)
        self.goto.setFixedWidth(width)
        label = QLabel("Go to event ")
        gotodisp.addRow(label, self.goto)
        font = QFont(nxt.fontInfo().family())
        font.setBold(True)
        areadisp = QFormLayout()
        self.area = QLineEdit("")
        self.area.setReadOnly(True)
        self.area.setFixedWidth(width)
        label = QLabel("AREA (fC)")
        self.area.setFont(font)
        label.setFont(font)
        areadisp.addRow(label, self.area)
        self.displaylist.append(self.area)

        durdisp = QFormLayout()
        self.duration = QLineEdit("")
        self.duration.setReadOnly(True)
        self.duration.setFixedWidth(width)
        label = QLabel("DURATION (ms)")
        self.duration.setFont(font)
        label.setFont(font)
        durdisp.addRow(label, self.duration)
        self.displaylist.append(self.duration)

        topbox.addWidget(nxt)
        topbox.addLayout(gotodisp)
        topbox.addStretch(10)
        topbox.addLayout(areadisp)
        bottombox.addWidget(previous)
        bottombox.addWidget(reload)
        bottombox.addStretch(10)
        bottombox.addLayout(durdisp)

        self.viewercontrols = [nxt, self.goto, previous, reload]
        vbox.addLayout(topbox)
        vbox.addLayout(bottombox)
        vbox.addSpacing(10)
        self.eventwidget = MplBox()
        vbox.addLayout(self.eventwidget.vbox)
        groupbox.setLayout(vbox)

    def show_eventdetails(self, groupbox):
        self.eventdetails = QTextEdit()
        self.eventdetails.setReadOnly(True)
        vbox = QVBoxLayout()
        vbox.addWidget(self.eventdetails)
        groupbox.setLayout(vbox)

    def io_buttons(self):
        self.sqlcheckfile = QCheckBox("Use SQL")
        self.sqlcheckfile.stateChanged.connect(self.switch_fileio)
        self.input_button = QPushButton("Input dir")
        self.input_button.clicked.connect(self.set_sourcedir)
        self.input_line = QLineEdit()
        self.output_button = QPushButton("Output dir")
        self.output_button.clicked.connect(self.set_targetdir)
        self.output_line = QLineEdit()
        self.sqlchecksql = QCheckBox("Use SQL")
        self.sqlchecksql.stateChanged.connect(self.switch_sqlio)
        self.sqltag = QLineEdit()
        self.sqltag.setFixedWidth(80)
        self.sqltag.textChanged.connect(self.tag_update)
        self.input_button_sql = QPushButton("Input base dir")
        self.input_button_sql.clicked.connect(self.set_sourcedir)
        self.input_line_sql = QLineEdit()
        self.output_button_sql = QPushButton("Output base dir")
        self.output_button_sql.clicked.connect(self.set_targetdir)
        self.output_line_sql = QLineEdit()

        self.taglabel = QLabel()
        self.output_line_sql_folder = QLineEdit()
        self.iosettings = [
            self.input_line,
            self.output_line,
            self.input_line_sql,
            self.output_line_sql,
            self.output_line_sql_folder,
            self.sqltag,
        ]
        self.iosettingslabels = [
            "input line",
            "output line",
            "input line sql",
            "output line sql",
            "output line sql folder",
            "sqltag",
        ]

    def switch_fileio(self, state):
        if state == 2:
            self.sqlchecksql.setChecked(True)
            self.fileio.setVisible(False)
            self.sqlio.setVisible(True)
            self.sql_login()

    def switch_sqlio(self, state):
        if state == 0:
            self.sqlcheckfile.setChecked(False)
            self.fileio.setVisible(True)
            self.sqlio.setVisible(False)
            self.sql_close()

    def tag_update(self, text):
        self.taglabel.setText(text + "/")

    def show_prog(self, groupbox):
        self.progbar = QProgressBar(self.main_widget)
        self.progbar.setValue(0)
        hbox = QHBoxLayout()
        hbox.addWidget(self.progbar)
        groupbox.setLayout(hbox)


class AnalysisWindow(QWidget):
    def __init__(self, forward, backwards, getfileroot):
        self.forward = forward
        self.backwards = backwards
        self.getfileroot = getfileroot
        super().__init__()
        peaksett = {
            "Peak height selectivity": ["sel", None, float],
            "Global threshold": ["threshold", None, float],
            "Extrema": ["extrema", None, int],
            "Max hor. index dist. peak / valley": ["maxhrztlinddistance", None, int],
            "Max hor. distance peak / valley": ["maxhrztldistance", None, float],
            "Max vert. distance lvalley / rvalley": ["maxvertdistance", None, float],
            "Masksize": ["masksize", None, int],
        }
        scipy_peaksett = {
            "Min. height": ["min_height", None, float],
            "Max. height": ["max_height", None, float],
            "Min. threshold": ["min_threshold", None, float],
            "Max. threshold": ["max_threshold", None, float],
            "Distance": ["distance", None, int],
            "Min. prominence": ["min_prominence", None, float],
            "Max. prominence": ["max_prominence", None, float],
            "Min. width": ["min_width", None, float],
            "Max. width": ["max_width", None, float],
            "Wlen": ["wlen", None, float],
            "Rel. height": ["rel_height", None, float],
            "Min. plateau size": ["min_plateau_size", None, float],
            "Max. plateau size": ["max_plateau_size", None, float],
        }
        cusumsett = {
            "Threshold": ["threshold", None, float],
            "Stepsize": ["delta", None, float],
            "Standard deviation": ["sigma", None, float],
        }
        foldthressett = {
            "Event threshold": ["eventthreshold", None, float],
            "Fold threshold": ["foldthreshold", None, float],
        }
        usersett = {
            "Setting 1": ["sett1", None, float],
            "Setting 2": ["sett2", None, float],
            "Setting 3": ["sett3", None, float],
        }
        blanksett = {}
        self.analysis_methods = {
            "Peaks": ("peaks", peaksett),
            "Scipy peaks": ("scipy_peaks", scipy_peaksett),
            "Fold cusum": ("cusum", cusumsett),
            "User add": ("user", usersett),
            "Blank": ("blank", blanksett),
            "Fold threshold": ("fold_threshold", foldthressett),
            "Show folding": ("showfold", {}),
            "Show folding + peaks": ("showfoldpeaks", {}),
            "Show folding + clusterpeaks": ("showfoldcluspeaks", {}),
        }
        if read_config()["Bayes nested sampling"]["On/Off"]:
            bayessett = {}
            self.analysis_methods["Fold bayes one"] = ("bayes one", bayessett)
            self.analysis_methods["Fold bayes two-one"] = ("bayes twoone", bayessett)
            bayessett = {"logZ difference threshold": ["logZdiff", None, float]}
            self.analysis_methods["Fold bayes compare"] = (
                "bayes one twoone",
                bayessett,
            )

        mainbox = QVBoxLayout()
        upper = QGroupBox(title="Options")
        self.show_options(upper)
        upper.setSizePolicy(upper.sizePolicy().horizontalPolicy(), QSizePolicy.Fixed)
        lowerbox = QHBoxLayout()
        leftbox = QVBoxLayout()
        settings = QGroupBox(title="Settings")
        settbox = QVBoxLayout()
        settstack = QStackedWidget()
        settcombo = QComboBox()
        settcombo.activated.connect(settstack.setCurrentIndex)
        self.fill_settingstabs(settcombo, settstack)
        settbox.addWidget(settcombo)
        settbox.addWidget(settstack)
        settings.setLayout(settbox)
        output = QGroupBox(title="Output")
        outbox = QVBoxLayout()
        self.outtext = QTextEdit()
        self.outtext.setReadOnly(True)
        outbox.addWidget(self.outtext)
        output.setLayout(outbox)
        leftbox.addWidget(settings)
        leftbox.addWidget(output)
        plotbox = QGroupBox(title="Analysis")
        canvasbox = MplCanvasBox([211, 212])
        self.canvas = canvasbox.canvas
        self.canvas.subplots_adjust(
            {
                "left": 0.15,
                "bottom": 0.1,
                "right": 0.975,
                "top": 0.975,
                "wspace": 0.2,
                "hspace": 0.25,
            }
        )
        plotbox.setLayout(canvasbox.box)
        lowerbox.addLayout(leftbox, stretch=1)
        lowerbox.addWidget(plotbox, stretch=3)
        mainbox.addWidget(upper)
        mainbox.addLayout(lowerbox)
        self.setLayout(mainbox)
        self.read_settings()
        self.reload = True
        self.setWindowTitle("eventviewer - analysis window")

    def show_options(self, groupbox):
        hbox = QHBoxLayout()
        self.write = QCheckBox("Write to file")
        label1 = QLabel("Analysis 1")
        self.analysis1 = QComboBox()
        self.analysis1.addItems(sorted(list(self.analysis_methods)))
        #        self.analysis1.activated.connect(self.combobox)
        label2 = QLabel("Analysis 2")
        self.analysis2 = QComboBox()
        self.analysis2.addItems(sorted(list(self.analysis_methods)))
        #        self.analysis2.activated.connect(self.combobox)
        self.levels = QCheckBox("Plot levels")
        self.levels.setChecked(True)
        savesetts = QPushButton("Save settings")
        savesetts.clicked.connect(self.save_settings)

        nxt = QPushButton("Next")
        #        nxt.setVisible(False)
        nxt.clicked.connect(self.forward)
        nxt.setShortcut(QKeySequence.MoveToNextChar)
        previous = QPushButton("Previous")
        #        previous.setVisible(False)
        previous.clicked.connect(self.backwards)
        previous.setShortcut(QKeySequence.MoveToPreviousChar)

        hbox.addWidget(previous)
        hbox.addWidget(nxt)
        hbox.addWidget(self.write)
        hbox.addWidget(label1)
        hbox.addWidget(self.analysis1)
        hbox.addWidget(label2)
        hbox.addWidget(self.analysis2)
        hbox.addWidget(self.levels)
        hbox.addWidget(savesetts)
        hbox.addStretch(10)
        groupbox.setLayout(hbox)

    def fill_settingstabs(self, combobox, stack):
        for method, value in sorted(self.analysis_methods.items()):
            widget = QWidget()
            form = QFormLayout()
            setts = value[1]
            for setting in sorted(setts):
                label = QLabel(setting)
                line = QLineEdit("")
                form.addRow(label, line)
                setts[setting][1] = line
            widget.setLayout(form)
            stack.addWidget(widget)
            combobox.addItem(method)

    def save_settings(self):
        fileroot = self.getfileroot()
        if fileroot is not None:
            path = join(fileroot, "peakfinder_settings.txt")
            exprt(path, self.get_settings())

    def get_settings(self):
        settings = {}
        for key, value in self.analysis_methods.items():
            newkey = value[0]
            dic = value[1]
            nested = {}
            for key2, entry in dic.items():
                try:
                    nested[entry[0]] = entry[2](entry[1].text())
                except Exception as e:
                    print("Can't convert " + key + " into right type.")
                    raise e
            settings[newkey] = nested
        return settings

    def analyse(self, eventset, no, x, y, attrs, levels):
        axesA = self.canvas.pick_axes(1)
        axesB = self.canvas.pick_axes(2)
        settings = self.get_settings()
        analysis1 = self.analysis_methods[self.analysis1.currentText()][0]
        analysis2 = self.analysis_methods[self.analysis2.currentText()][0]

        save = analysis_wrapper(
            y,
            attrs,
            levels,
            analysis1,
            analysis2,
            eventset,
            no,
            settings=settings,
            x=x,
            axesA=axesA,
            axesB=axesB,
            Reload=self.reload,
            plotlevels=self.levels.isChecked(),
        )
        if self.reload:
            self.reload = False
        self.canvas.draw()
        out = (
            "********************************\n" + self.analysis1.currentText() + "\n\n"
        )
        for key, value in sorted(save[0][analysis1].items()):
            out += key + ": \t" + str(value) + "\n"
        out += (
            "\n\n\n"
            + "********************************\n"
            + self.analysis2.currentText()
            + "\n\n"
        )
        for key, value in sorted(save[1][analysis2].items()):
            out += key + ": \t" + str(value) + "\n"
        self.outtext.clear()
        self.outtext.insertPlainText(out)
        if self.write.isChecked():
            for key, value in sorted(save[0][analysis1].items()):
                name = analysis1 + "_" + key
                eventset.write_ds(no, name, value, (100, 100))
            for key, value in sorted(save[1][analysis2].items()):
                name = analysis2 + "_" + key
                eventset.write_ds(no, name, value, (100, 100))

    def read_settings(self):
        setts = QSettings("ProjectZ", "eventviewer_analysis")
        setts.setFallbacksEnabled(False)
        for key in self.analysis_methods:
            method = self.analysis_methods[key][1]
            for entry in method:
                s = (key + entry).replace(" ", "_")
                method[entry][1].setText(setts.value(s, ""))

    def write_settings(self):
        setts = QSettings("ProjectZ", "eventviewer_analysis")
        setts.setFallbacksEnabled(False)
        for key in self.analysis_methods:
            method = self.analysis_methods[key][1]
            for entry in method:
                s = (key + entry).replace(" ", "_")
                setts.setValue(s, method[entry][1].text())

    def closeEvent(self, event):
        self.write_settings()
        event.accept()


def run():
    app = QApplication(argv)
    bw = EventViewer()
    bw.show()
    app.exec_()
