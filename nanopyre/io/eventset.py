#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Sep 12 22:18:22 2017


"""

from os.path import join, dirname
import h5py as h5
import numpy as np
import pandas as pd


class EventError(Exception):
    pass


class EventSet:
    def __init__(self, path, filterindex=True):
        try:
            self.file = h5.File(path, "r")
        except (OSError, ValueError):
            raise EventError
        else:
            self.path = path
            self.root = join(dirname(path), "")
            try:
                self.tag = [x for x in self.root.split("/") if x.find("_") != -1][0]
            except IndexError:
                self.tag = "NAN"
            self.eventlist = [x for x in self.file if x.startswith("T")]
            self.get_porechar(filterindex)
            self.noevents = self.findex.size
            _, attrs = self.get_event_data(self.noevents)
            try:
                self.laststart_ms = attrs["start_ms"]
            except KeyError:
                self.laststart_ms = -1

    # Aggregate values

    def get_porechar(self, filterindex):
        try:
            charfile = h5.File(join(self.root, "pore_characterisation.hdf5"), "r")
        except (FileNotFoundError, OSError):
            self.porechar = {}
        else:
            s = dict(charfile.items())
            self.porechar = {i[0]: i[1][...] for i in s.items()}
            charfile.close()
        finally:
            self.findex = self.porechar.get(
                "filtered_index", np.arange(len(self.eventlist))
            )
            if not filterindex:
                self.findex = np.arange(len(self.eventlist))
            self.levels = self.porechar.get("levels", np.empty(0))

    def get_event_frequency(self):
        try:
            sumfile = h5.File(join(self.root, "finder_summary.hdf5"), "r")
        except (FileNotFoundError, OSError):
            raise EventError
        else:
            out = sumfile["summary"].attrs["frequency_Hz"]
            sumfile.close()
            return out

    def get_finder_summary(self):
        try:
            with pd.HDFStore(join(self.root, "finder_summary.hdf5"), "r") as store:
                df = store["summary"]
        except (FileNotFoundError, OSError):
            raise EventError
        else:
            return df

    # Event data

    def get_event_index(self, no):
        return self.findex[no - 1]

    def get_event_data(self, no):
        try:
            ind = self.get_event_index(no)
            grp = self.file[self.eventlist[ind]]
        except IndexError:
            raise EventError
        else:
            current = grp["current_nA"][...]
            attrs = dict(grp.attrs.items())
            return current, attrs

    def get_dataframe(
        self,
        dsnames=["current_nA"],
        attributes=[
            "eventno",
            "start_ms",
            "duration_ms",
            "area_fC",
            "start_frame",
            "currentbaseline_nA",
            "rms_nA",
            "file",
        ],
        save=False,
    ):
        filteventlist = np.array(self.eventlist)[self.findex]
        dsdata = self.get_dataset_arrays(self.file, filteventlist, dsnames)
        dslist = zip(dsnames, dsdata)
        attdata = self.get_attr_arrays(self.file, filteventlist, attributes)
        attlist = zip(attributes, attdata)
        data = dict(dslist)
        data.update(attlist)
        data["group"] = filteventlist
        df = pd.DataFrame(data=data)
        if save:
            self.dataframe = df
        return df

    # Bayes file functions

    def get_bayes_dataframe(
        self,
        dsnames=[],
        rootdsnames=[
            "folded",
            "fit_duration_ms",
            "fit_area_fC",
            "fit_levels_nA",
            "fit_increments_ms",
            "fit_foldratio",
            "fit_foldpos",
            "area_fitduration_fC",
            "one_parameters",
            "one_parameter_errors",
            "twoone_parameters",
            "twoone_parameter_errors",
            "oneMinusTwoone_logz",
            "one_global_logz",
            "one_global_logzerr",
            "pcbelowthresh",
            "threshold",
            "twoone_global_logz",
            "twoone_global_logzerr",
            "eventno",
        ],
        attributes=[],
    ):
        try:
            bayesfile = h5.File(join(self.root, "bayes_folding.hdf5"), "r")
        except (FileNotFoundError, OSError):
            return pd.DataFrame()
        else:
            df = self.get_bayes_df_file(bayesfile, dsnames, rootdsnames, attributes)
            return df
        finally:
            bayesfile.close()

    def get_bayes_file(self):
        try:
            bayesfile = h5.File(join(self.root, "bayes_folding.hdf5"), "r+")
        except (FileNotFoundError, OSError):
            raise EventError
        return bayesfile

    def get_bayes_entry(self, bayesfile, no, col):
        try:
            ind = self.get_event_index(no)
            ind = np.where(bayesfile["eventno"][...] == (ind + 1))[0][0]
        except IndexError:
            raise EventError
        else:
            out = bayesfile.get(col, default=None)
            if out is not None:
                return out[ind][...]
            else:
                return None

    def get_bayes_fit(self, bayesfile, no):
        try:
            ind = self.get_event_index(no)
            grp = bayesfile[self.eventlist[ind]]
            ind = np.where(bayesfile["eventno"][...] == (ind + 1))[0][0]
        except IndexError:
            raise EventError
        else:
            if bayesfile["folded"][ind]:
                return grp["twoone_fit"][...]
            else:
                return grp["one_fit"][...]

    def get_bayes_root_attrs(self, attrs=["unfoldpc"]):
        with self.get_bayes_file() as file:
            return self.get_root_attrs(file, attrs)

    def get_bayes_df_file(self, bayesfile, dsnames, rootdsnames, attributes):
        grplist = [x for x in bayesfile]
        if rootdsnames == "full":
            rootdsnames = [x for x in grplist if not x.startswith("T")]
        if dsnames == "full":
            dsnames = [x for x in bayesfile[grplist[0]]]
        if attributes == "full":
            attributes = [x for x in bayesfile[grplist[0]].attrs]
        grplist = [x for x in grplist if x.startswith("T")]
        out = self.get_dataset_arrays(bayesfile, grplist, dsnames)
        data = dict(zip(dsnames, out))
        out, newrootdsnames = self.get_root_vectors(bayesfile, rootdsnames)
        data.update(zip(newrootdsnames, out))
        out = self.get_attr_arrays(bayesfile, grplist, attributes)
        data.update(zip(attributes, out))
        data["group"] = np.array(grplist)
        df = pd.DataFrame(data=data)
        return df

    # General file access

    def get_dataset_arrays(self, file, rows, dsnames):
        out = []
        for i, dsname in enumerate(dsnames):
            if file[rows[0]][dsname][...].size > 1:
                out.append(np.empty((len(rows),), dtype=object))
            else:
                out.append(np.empty((len(rows),)))

        for i, e in enumerate(rows):
            grp = file[e]
            for j, dsname in enumerate(dsnames):
                out[j][i] = grp.get(dsname, default=np.array([np.nan]))[...]
        return out

    def get_attr_arrays(self, file, rows, attributes):
        out = []
        for i, attrname in enumerate(attributes):
            if not type(file[rows[0]].attrs[attrname]) == np.string_:
                out.append(np.empty((len(rows),)))
            else:
                out.append(np.empty((len(rows),), dtype=object))
        for i, e in enumerate(rows):
            grp = file[e]
            for j, attrname in enumerate(attributes):
                out[j][i] = grp.attrs.get(attrname, default=np.nan)
        return out

    def get_root_vectors(self, file, rootdsnames):
        out = []
        newrootdsnames = []
        for dsname in rootdsnames:
            if len(file[dsname].shape) == 1:
                out.append(file[dsname][...])
                newrootdsnames.append(dsname)
            else:
                for i, col in enumerate(file[dsname][...].T):
                    out.append(col)
                    newrootdsnames.append(dsname + "_" + str(i))
        return out, newrootdsnames

    def get_root_arrays(self, file, rootdsnames):
        out = []
        for dsname in rootdsnames:
            out.append(file[dsname][...])
        return out

    def get_root_attrs(self, file, attrs=[]):
        if attrs == "full":
            return dict(file["/"].attrs.items())
        else:
            return {key: file["/"].attrs.get(key, default=None) for key in attrs}

    def get_concat_current(self):
        if hasattr(self, "dataframe"):
            ds = self.dataframe
        else:
            ds = self.get_dataframe(dsnames=["current_nA"], attributes=[])
        array = np.concatenate(ds["current_nA"].values)
        return array

    # Dataset writing

    def write_ds(self, no, name, array, maxshape):
        no = self.get_event_index(no) + 1
        array = np.array(array)
        if len(array.shape) == 1:
            array = array.reshape((1, -1))
        try:
            grp = self.file[self.make_outputname(no)]
        except KeyError:
            try:
                grp = self.file[self.make_outputname(no, padding=5)]
            except KeyError:
                raise EventError
        finally:
            try:
                ds = grp[name]
            except KeyError:
                ds = grp.create_dataset(name, data=array, maxshape=maxshape)
            else:
                ds.resize(array.shape)
                ds[...] = array

    # Misc

    def make_outputname(self, no, padding=6):
        add = (padding - len(str(no))) * "0"
        filename = "T" + add + str(no)
        return filename

    def close(self):
        try:
            self.file.close()
        except Exception as p:
            print(p)
            pass

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        if exc_type is not None:
            print(exc_type, exc_val, exc_tb)
        return True
