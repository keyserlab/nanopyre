#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Nov  9 17:22:29 2017


"""

import pyodbc as pyo
from nanopyre.io.configio import read_config


class SqlIO:
    def login(self, usn, pwd):
        self.usn = usn
        try:
            loginstr = (
                "DSN=" + read_config()["SQL"]["DNS"] + ";UID=" + usn + ";PWD=" + pwd
            )
            self.cxn = pyo.connect(loginstr)
        except (pyo.Error, pyo.ProgrammingError):
            raise SqlError

    def error(self, errortype, message):
        print(errortype + ": " + message)

    def insertorupdate(self, table, cols, lis, tag, newidfct=(True, None)):
        try:
            self.query(table, ["tag"], tag)
        except TOError:
            return -1
        #            print('Last entry not written to SQL database due to timeout')
        except SqlError:
            cols = cols[:]
            cols.insert(0, "tag")
            lis = lis[:]
            lis.insert(0, tag)
            if not newidfct[0]:
                try:
                    unid = self.query(newidfct[1], ["unid"], tag)[0]
                except SqlError:
                    print(
                        "\tTag to retrieve unid cannot be found in table " + newidfct[1]
                    )
                    return -2
                else:
                    cols.insert(0, "unid")
                    lis.insert(0, unid)
            try:
                self.insert(table, cols, lis)
            except SqlError:
                print("Could not access SQL database, check connection")
                return -2
            except TOError:
                return -1
            else:
                return 0
        else:
            try:
                self.update_(table, cols, lis, tag)
            except SqlError:
                print("Could not access SQL database, check connection")
                return -1
            except TOError:
                return -1
            else:
                return 0

    def query(self, table, cols, tag):
        colstr = cols[0]
        for x in cols[1:]:
            colstr += ", " + x
        qstr = "SELECT " + colstr + " FROM " + table + " WHERE tag = ?"
        try:
            crsr = self.execute(qstr, [tag])
            outputlist = list(crsr.fetchall()[0])
        except SqlError:
            pass
        except IndexError:
            raise SqlError
        #        except TOError:
        #            pass
        else:
            return outputlist

    def insert(self, table, cols, lis):
        colstr = cols[0]
        for x in cols[1:]:
            colstr += ", " + x
        qstr = (
            "INSERT INTO "
            + table
            + "("
            + colstr
            + ") VALUES ("
            + "?, " * (len(lis) - 1)
            + "?)"
        )
        self.execute(qstr, lis)
        self.db_commit()

    def update_(self, table, cols, lis, tag):
        lis = lis[:]
        lis.append(tag)
        qstr = "UPDATE " + table + " SET "
        for x in cols:
            qstr = qstr + x + "=?, "
        qstr = qstr[:-2] + " WHERE tag = ?"
        self.execute(qstr, lis)
        self.db_commit()

    def db_query(self, qstr):
        try:
            crsr = self.execute(qstr)
        except TOError:
            raise SqlError
        else:
            data = crsr.fetchall()
            if data:
                return data
            else:
                self.error("Database Error", "Query returned no entries")
                raise SqlError
            crsr.close()

    def db_input(self, qstr, lis):
        try:
            crsr = self.execute(qstr, lis)
        except TOError:
            raise SqlError
        else:
            crsr.close()

    def execute(self, qstr, *lis):
        crsr = self.cxn.cursor()
        try:
            if lis:
                crsr.execute(qstr, lis[0])
            else:
                crsr.execute(qstr)
        except (pyo.ProgrammingError, pyo.DataError) as p:
            print(p)
            self.error("SQL Database Error", "Not a valid SQL query")
            raise SqlError
        except pyo.Error:
            self.error("SQL Connection Error", "Session timed out")
            try:
                self.cxn.close()
            except:
                pass
            self.open()
            raise TOError
        else:
            return crsr

    # Override this in gui subclasses to reopen login window
    def open(self):
        pass

    def db_commit(self):
        self.cxn.commit()

    def db_rollback(self):
        self.cxn.rollback()

    def db_cols(self, table):
        crsr = self.cxn.cursor()
        info = crsr.columns(table).fetchall()
        cols = [i[3] for i in info]
        types = [i[5] for i in info]
        return cols, types

    def closeconn(self):
        try:
            self.cxn.close()
        except:
            pass


class SqlError(Exception):
    pass


class TOError(Exception):
    pass
