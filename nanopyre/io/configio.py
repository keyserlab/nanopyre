#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Aug  4 15:34:38 2017


"""

import pkg_resources
import json


def write_config(dictionary):
    path = pkg_resources.resource_filename("nanopyre", "config.txt")
    exprt(path, dictionary)


def read_config():
    path = pkg_resources.resource_filename("nanopyre", "config.txt")
    dictionary = imprt(path)
    return dictionary


def exprt(path, obj):
    string = json.dumps(obj)
    with open(path, "w") as f:
        f.write(string)


def imprt(path):
    with open(path, "r") as f:
        string = f.read()
    obj = json.loads(string)
    return obj
