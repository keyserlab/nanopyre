#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Oct 23 16:09:49 2017


"""

import numpy as np
from nptdms import TdmsFile
import h5py


def read_file(path):
    try:
        if path.endswith(".tmds"):
            file = TdmsFile(path)
            group = file.groups()[0]
            channels = file.group_channels(group)
            if "current" in [obj.channel for obj in channels]:
                data = file.channel_data(group, "current")
            else:
                data = channels[0].data
        elif path.endswith(".hdf5"):
            file = h5py.File(path, "r")
            data = file["data"][:, 0]
        else:
            data = np.empty((0,))
        if data.size == 0:
            data = np.empty((0,))
    except (KeyError, OSError):
        data = np.empty((0,))
    return data
