#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Nov 23 12:16:59 2017


"""

import numpy as np
from os.path import join, split
from datetime import date
import h5py as h5
import matplotlib.pyplot as plt
from glob import glob

from nanopyre.io.sqlio import SqlIO, SqlError
from nanopyre.analysis import current_trace
from nanopyre.analysis.ecd_levels import filter_ecd, detect_levels, EcdLevelError
from nanopyre.analysis.poresizer import poresizer, SizerError
from nanopyre.analysis.threshold_volume import percent_below
from nanopyre.io.eventset import EventSet, EventError
from nanopyre.io.configio import read_config
from nanopyre.tools import postproctools as pt


usn = read_config()["Tools"]["usn"]
pwd = read_config()["Tools"]["pwd"]
rawpath = read_config()["Tools"]["rawpath"]


def fix_frequency(taglist, overwrite):
    for tag in taglist:
        print("*******************************************")
        print(tag)
        sql = SqlIO()
        sql.login(usn, pwd)
        iptpath = sql.query("mlist", ["ppath"], tag)[0]
        iptpath = join(rawpath, iptpath[1:])
        sql.closeconn()

        es = EventSet(pt.get_tag_path(tag), False)
        kwargs = {key: value for key, value in es.file[es.eventlist[0]].attrs.items()}
        kwargs.update({"framerate_Hz": 250000, "watchlen_ms": 3000, "corrlen_ms": 1000})
        proceed = es.file["/"].attrs.get("netlength_frames", default=None)
        if proceed is not None:
            continue
        trace = current_trace.CurrentTrace(iptpath, "", **kwargs)
        trace.correct_one_window()
        while not trace.endreached:
            if trace.rms < float(kwargs["maxrms_nA"]):
                trace.correct_one_window()
            else:
                while (
                    trace.rms > float(float(kwargs["maxrms_nA"]))
                ) and not trace.endreached:
                    trace.correct_one_window(skipped=True)
        print("\tWatchedlen: ", str(trace.watchedlen))
        print(
            "\t\tWatchedlen frequ: ",
            str(es.noevents / (trace.frames2ms(trace.watchedlen) / 1000.0)),
        )
        print("\t\tRecorded frequ:", str(es.get_event_frequency()))
        print("\tNetlen: ", str(trace.netwatchedlen))
        print(
            "\t\tNetlen frequ: ",
            str(es.noevents / (trace.frames2ms(trace.netwatchedlen) / 1000.0)),
        )
        if proceed is None or overwrite:
            print("\tUpdating...")
            frequ = es.noevents / (trace.frames2ms(trace.netwatchedlen) / 1000.0)
            es.file["/"].attrs.create(
                "lastchange", np.array(str(date.today()), dtype="S")
            )
            es.file["/"].attrs.create("frequency_Hz", frequ)
            es.file["/"].attrs.create("totallength_frames", trace.watchedlen)
            es.file["/"].attrs.create("netlength_frames", trace.netwatchedlen)
            pt.sql_set_value(usn, pwd, "alist", tag, "frequency", frequ)
            pt.sql_set_value(usn, pwd, "alist", tag, "debug", 1)
            with h5.File(join(es.root, "finder_summary.hdf5"), "r+") as f:
                f["summary"].attrs.create(
                    "lastchange", np.array(str(date.today()), dtype="S")
                )
                f["summary"].attrs.create("frequency_Hz", frequ)
                f["summary"].attrs.create("totallength_frames", trace.watchedlen)
                f["summary"].attrs.create("netlength_frames", trace.netwatchedlen)
        trace.closefiles()
        es.close()


def check_rms_error(taglist):
    print("*****************************************")
    print("Need to correct the following tags:")
    correct = []
    for tag in taglist:
        with EventSet(pt.get_tag_path(tag)) as es:
            dw = es.get_dataframe(dsnames=[], attributes=["rms_nA"])
            if dw["rms_nA"].max() > 1.0:
                correct.append(tag)
                print(tag, dw["rms_nA"].max())
    return correct


def fix_rms_error(taglist, overwrite=False):
    print("*****************************************")
    print("Checking...")
    correctedtags = []
    for tag in taglist:
        with EventSet(pt.get_tag_path(tag), filterindex=False) as es:
            dw = es.get_dataframe(dsnames=[], attributes=["rms_nA"])
            if dw["rms_nA"].max() > 1.0:
                print("******************")
                print(tag)
                correctedtags.append(tag)
                wrongindex = dw.loc[dw["rms_nA"] > 1.0, "group"].index
                wronggroups = dw.loc[wrongindex, "group"]
                print(wronggroups)

                if overwrite:
                    correctwithindex = abs(wrongindex - 1)
                    correctwithgroups = dw.loc[correctwithindex, "group"]

                    for i, group in enumerate(wronggroups):
                        newval = es.file[correctwithgroups.iloc[i]].attrs["rms_nA"]
                        es.file[group].attrs.modify("rms_nA", newval)
    return correctedtags


def ecd_levels(path, write, axa, axb, sql):
    try:
        es = EventSet(path, filterindex=False)
        df = es.get_dataframe(dsnames=["current_nA"], attributes=["samplefreq_Hz"])
        ecd_gaussfit, filtered_index = filter_ecd(df, axes=axa)
    except EventError:
        print(
            "Can't find events.hdf5 file, make sure you have loaded the right output directory"
        )
    except EcdLevelError:
        print("Can't filter ECD, check the thresholds in file tf_ecd_levels.py")
    else:
        if write:
            _write_porechar_h5(
                join(es.root, "pore_characterisation.hdf5"),
                "ecd_gaussfit",
                ecd_gaussfit,
                '"ecd_gaussfit" entry already exists, overwrite?: ',
            )
            _write_porechar_h5(
                join(es.root, "pore_characterisation.hdf5"),
                "filtered_index",
                filtered_index,
                '"filtered_index" entry already exists, overwrite?: ',
                resizablelen=True,
            )
            try:
                lis = [date.today(), usn, True]
                lis.extend(ecd_gaussfit.tolist())
                sql.insertorupdate(
                    "alist",
                    ["ddatea", "whoa", "ecdfiltered", "ecdmean", "ecdamp", "ecdwidth"],
                    lis,
                    es.tag,
                    (False, "mlist"),
                )
            except TypeError as p:
                print(p)
        try:
            levels = detect_levels(df, filtered_index, False, axes=axb)
        except EcdLevelError:
            print("Can't detect levels, check the thresholds in file tf_ecd_levels.py")
        else:
            if write:
                _write_porechar_h5(
                    join(es.root, "pore_characterisation.hdf5"),
                    "levels",
                    levels,
                    '"levels" entry already exists, overwrite?: ',
                )
                lis = [date.today(), usn, True]
                lis.extend([y for x in levels[1:] for y in x])
                sql.insertorupdate(
                    "alist",
                    [
                        "ddatea",
                        "whoa",
                        "levelsfound",
                        "level1mean",
                        "level1amp",
                        "level1width",
                        "level2mean",
                        "level2amp",
                        "level2width",
                    ],
                    lis,
                    es.tag,
                    (False, "mlist"),
                )
                try:
                    sql.update_("alist", ["debug2"], [2], es.tag)
                except SqlError as p:
                    print(p)
    finally:
        try:
            es.close()
        except:
            pass


def poresize(tag, sql, writefile, writesql, ax):
    path = sql.query("mlist", ["ppath"], tag)[0]
    inputpath = join(rawpath, split(path[:-1])[0][1:])
    outputpath = pt.get_tag_path(tag).rsplit("/", 1)[0]
    print(inputpath)
    try:
        inputpath = glob(join(inputpath, "**/IV curve and noise.dat"), recursive=True)[
            0
        ]
    except IndexError:
        try:
            inputpath = glob(join(inputpath, "**/IV_curve.txt"), recursive=True)[0]
        except IndexError:
            print("Can't find IV curve data file")
            return
        else:
            skiprows = 0
    else:
        skiprows = 1
    pore, salt, salt_conc = sql.query("mlist", ["pore", "salt", "salt_conc"], tag)
    try:
        resohm, poresize, recratio = poresizer(
            inputpath, pore, salt, salt_conc, axes=ax, skiprows=skiprows
        )
    except SizerError:
        print("IV curve not found or errors in pore size estimation")
    else:
        if writefile:
            _write_porechar_h5(
                outputpath + "/pore_characterisation.hdf5",
                "resistance_ohm",
                resohm,
                '"resistance_ohm" entry already exists, overwrite?: ',
            )
            _write_porechar_h5(
                outputpath + "/pore_characterisation.hdf5",
                "poresize_nm",
                poresize,
                '"poresize_nm" entry already exists, overwrite?: ',
            )
            _write_porechar_h5(
                outputpath + "/pore_characterisation.hdf5",
                "recratio",
                recratio,
                '"recratio" entry already exists, overwrite?: ',
            )
        if writesql:
            sql.insertorupdate(
                "alist",
                ["ddatea", "whoa", "resistance"],
                [date.today(), sql.usn, resohm],
                tag,
                (False, "mlist"),
            )
            sql.insertorupdate(
                "alist",
                ["ddatea", "whoa", "poresize"],
                [date.today(), sql.usn, poresize],
                tag,
                (False, "mlist"),
            )
            outcome = sql.insertorupdate(
                "alist",
                ["ddatea", "whoa", "recratio"],
                [date.today(), sql.usn, recratio],
                tag,
                (False, "mlist"),
            )
            if outcome == -1:
                print("Last entry not written to SQL database: timeout")


def _write_porechar_h5(filepath, dspath, data, msg, resizablelen=False):
    write = 0
    try:
        file = h5.File(filepath, "r+")
    except IOError:
        file = h5.File(filepath, "a")
        write = 1
    else:
        try:
            file[dspath]
        except KeyError:
            write = 1
        else:
            write = 2
    if write == 1:
        if not resizablelen:
            dataset = file.create_dataset(dspath, data=data)
        else:
            dataset = file.create_dataset(dspath, data=data, maxshape=(None,))
    elif write == 2:
        dataset = file[dspath]
        if resizablelen:
            dataset.resize(data.shape)
        dataset[...] = data
    try:
        file.close()
    except NameError:
        pass
    return write


def fix_ecd_lev(taglist, write, plot):
    if write:
        sql = SqlIO()
        sql.login(usn, pwd)
    else:
        sql = None
    if plot:
        fig = plt.figure(77)
        axa = fig.add_subplot(211)
        axb = fig.add_subplot(212)
    else:
        axa = None
        axb = None
    for tag in taglist:
        print("*******************************************")
        print(tag)
        path = pt.get_tag_path(tag)
        ecd_levels(path, write, axa, axb, sql)
        if axa is not None:
            plt.draw()
            plt.waitforbuttonpress()


def fix_poresize(taglist, writefile, writesql, plot):
    sql = SqlIO()
    sql.login(usn, pwd)
    if plot:
        fig = plt.figure(77)
        ax = fig.add_subplot(111)
    else:
        ax = None
    for tag in taglist:
        print("*******************************************")
        print(tag)
        poresize(tag, sql, writefile, writesql, ax)
        if ax is not None:
            plt.draw()
            plt.waitforbuttonpress()


def update_classic_fold(es):
    l1 = es.porechar["levels"][1, 0]
    if es.porechar["levels"][2, 1] > 0:
        l2 = es.porechar["levels"][2, 0]
    else:
        l2 = 1.75 * es.porechar["levels"][1, 0]
    with es.get_bayes_file() as file:
        length = file["eventno"].size
        #        if file.get('pcbelowthresh', default=None) is None:
        rootdsnames = {
            "pcbelowthresh": (np.float, (length, 3)),
            "threshold": (np.float, (length, 3)),
        }
        for key, value in rootdsnames.items():
            del file[key]
            ds = file.create_dataset(key, shape=value[1], dtype=value[0])
        df = es.get_dataframe(dsnames=["current_nA"], attributes=[])
        i = 0
        for index, series in df.iterrows():
            y = series["current_nA"][...]
            file["pcbelowthresh"][i], file["threshold"][i] = percent_below(
                0, y, l1, l2, 1
            )
            i += 1


def update_tagged_classic_fold(taglist):
    for tag in taglist:
        print("***************************************************************")
        print(tag)
        with EventSet(pt.get_tag_path(tag), filterindex=True) as es:
            update_classic_fold(es)
