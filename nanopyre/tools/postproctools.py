#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Nov 17 09:33:45 2017


"""

import numpy as np
import pandas as pd
from os.path import join
from scipy.optimize import minimize_scalar, curve_fit

from nanopyre.io.sqlio import SqlIO, SqlError
from nanopyre.io.configio import read_config
from nanopyre.io.eventset import EventSet
from nanopyre.analysis.ecd_levels import fit_one_gaussian, fit_many_gaussians
from nanopyre.analysis.peakfinder import gauss


usn = read_config()["Tools"]["usn"]
pwd = read_config()["Tools"]["pwd"]
eventspath = read_config()["Tools"]["eventspath"]
use_sql = True

# Tag / SQL bits


def get_tag_path(tag):
    path = join(eventspath, tag, "events/events.hdf5")
    return path


def sql_get_taglist(where):
    sql = SqlIO()
    sql.login("Anon", "Trlcdc100")
    try:
        crsr = sql.execute(
            "select mlist.tag from mlist LEFT JOIN alist ON mlist.unid = alist.unid where "
            + where
        )
    except SqlError:
        print("SQL error for query ", where)
    else:
        l = crsr.fetchall()
        l = [x[0] for x in l]
        return l
    finally:
        sql.closeconn()


def sql_get_value(tag, column):
    sql = SqlIO()
    sql.login("Anon", "Trlcdc100")
    try:
        crsr = sql.execute(
            "select "
            + column
            + " from mlist LEFT JOIN alist ON mlist.unid = alist.unid where mlist.tag = ?",
            tag,
        )
    except SqlError:
        print("SQL error for tag ", tag)
    else:
        l = crsr.fetchval()
        return l
    finally:
        sql.closeconn()


def sql_set_value(usn, pwd, table, tag, col, val):
    if use_sql:
        sql = SqlIO()
        sql.login(usn, pwd)
        try:
            sql.update_(table, [col], [val], tag)
        except SqlError:
            print("SQL error for tag ", tag)
        finally:
            sql.closeconn()


def get_newer_tags(taglist, allbutzero=False):
    taglist = sorted(taglist)
    dropind = []
    droplist = []
    for i, tag in enumerate(taglist):
        if int(tag.rsplit("_", 1)[1]) == 0:
            dropind.append(i)
            droplist.append(tag.rsplit("_", 1)[0])
    for i, tag in enumerate(taglist):
        if tag.count("_") > 2:
            tag = tag.rsplit("_", 1)[0]
        for j, checktag in enumerate(taglist[i + 1 :]):
            if (checktag.find(tag) != -1) and not (allbutzero and tag in droplist):
                dropind.append(i)
                droplist.append(tag)
                break
    dropind = sorted(dropind, reverse=True)
    for i in dropind:
        taglist.pop(i)
    return taglist


def sql_to_container(taglist, collist):
    dataframe = pd.DataFrame()
    collist = collist.copy()
    collist.insert(0, "mlist.tag")
    make_unique(collist, ["tag"])
    sql = SqlIO()
    sql.login("Anon", "Trlcdc100")
    colstr = collist[0]
    for x in collist[1:]:
        colstr += ", " + x
    qstr = (
        "select "
        + colstr
        + " from mlist LEFT JOIN alist ON mlist.unid = alist.unid where mlist.tag = ?"
    )
    for tag in taglist:
        try:
            crsr = sql.execute(qstr, tag)
        except SqlError:
            print("-------------------------------------")
            print("Sql error on " + tag)
        else:
            colvals = crsr.fetchall()
            colvals = [x for x in colvals[0]]
            add = pd.DataFrame([colvals], columns=collist)
            dataframe = dataframe.append(add, ignore_index=True)
    sql.closeconn()
    dataframe.rename(columns={"mlist.tag": "tag"}, inplace=True)
    return dataframe


def make_unique(lis, droplist):
    remove = []
    for i, cola in enumerate(lis):
        if cola in droplist and i not in remove:
            remove.append(i)
            continue
        for j, colb in enumerate(lis[i + 1 :]):
            add = j + i + 1
            if (cola == colb) and add not in remove:
                remove.append(j + i + 1)
    remove = sorted(remove, reverse=True)
    for i in remove:
        lis.pop(i)


# Postprocessing calculations


def calc_logz_cutoff(es: EventSet, overwrite=False, axes=None):
    with es.get_bayes_file() as bayesfile:
        if overwrite or bayesfile["/"].attrs.get("logZ_cutoff", None) is None:
            df = es.get_bayes_df_file(
                bayesfile,
                dsnames=[],
                rootdsnames=["one_global_logz", "twoone_global_logz", "folded"],
                attributes=[],
            )
            df.loc[df["folded"], "total_logz"] = df.loc[
                df["folded"], "twoone_global_logz"
            ]
            df.loc[~df["folded"], "total_logz"] = df.loc[
                ~df["folded"], "one_global_logz"
            ]
            total_logz = df["total_logz"].values
            total_logz = total_logz[total_logz > -2500.0]
            (
                points,
                destimate,
                peakinds,
                ind,
                peakparams,
                peakdata,
                gaussfit,
                usinghwhm,
            ) = fit_one_gaussian(total_logz, method="kde")
            cutmultiplier = 2.0
            cutmultiplier = 1.5 if usinghwhm else cutmultiplier
            (mean, amplitude, width) = gaussfit
            logZ_cutoff = mean - cutmultiplier * width
            df["above_logZ_cutoff"] = df["total_logz"] > logZ_cutoff
            if axes:
                axes.clear()
                axes.set_autoscale_on(True)
                axes.set_xlabel("log Z")
                axes.set_ylabel("Prob. density")
                axes.plot(
                    points, destimate, "b", marker=".", markersize=3, linewidth=0.5
                )
                axes.plot(
                    points[ind],
                    destimate[ind],
                    marker="x",
                    markersize=5,
                    linestyle="None",
                    color="g",
                )
                axes.plot(
                    points[peakinds],
                    destimate[peakinds],
                    marker="x",
                    markersize=10,
                    linestyle="None",
                    color="m",
                    mew=3,
                )
                if peakparams is not None:
                    for i, arr in enumerate(peakdata):
                        axes.plot(
                            arr[0, :],
                            arr[1, :],
                            color="r",
                            marker=".",
                            markersize=3,
                            linewidth=0.5,
                        )
                axes.plot(
                    gaussfit[0],
                    gaussfit[1],
                    marker="x",
                    markersize=10,
                    linestyle="None",
                    color="k",
                    mew=3,
                )
                axes.axvline(logZ_cutoff, 0, 100, color="g", linewidth=2)
            bayesfile["/"].attrs.create("logZ_cutoff", logZ_cutoff)
            if bayesfile.get("above_logZ_cutoff", None) is None:
                ds = bayesfile.create_dataset(
                    "above_logZ_cutoff", data=df["above_logZ_cutoff"], dtype=bool
                )
            else:
                bayesfile["above_logZ_cutoff"][...] = df["above_logZ_cutoff"]
            print(bayesfile["/"].attrs.get("unfoldpc", None))
            unfoldpc = (
                df.loc[(~df["folded"]) & df["above_logZ_cutoff"], "folded"].size
                / df.loc[df["above_logZ_cutoff"], "folded"].size
                * 100.0
            )
            print("New: ", unfoldpc)
            bayesfile["/"].attrs.create("unfoldpc", unfoldpc)
            sql_set_value(usn, pwd, "alist", es.tag, "unfoldpc", unfoldpc)


def _penalty(threshold, label, inpt):
    result = inpt < threshold
    penalty = np.logical_xor(label, result).sum()
    return penalty


def calc_logz_thresh(es: EventSet, overwrite=False, axes=None):
    with es.get_bayes_file() as file:
        optThresh = file["/"].attrs.get("logZ_opt_threshold", default=None)
        pen = file["/"].attrs.get("logZ_penalty", default=None)
        pcError = file["/"].attrs.get("logZ_pc_error", default=None)
        if optThresh is None or overwrite:
            df = es.get_bayes_df_file(
                file,
                dsnames=[],
                rootdsnames=["pcbelowthresh", "oneMinusTwoone_logz"],
                attributes=[],
            )
            out = np.empty((1000, 2))
            min_thresh = -10000.0
            out[:, 0] = np.linspace(min_thresh, 50.0, out.shape[0])
            exception = False
            try:
                df.loc[:, "foldguess"] = ~(df["pcbelowthresh_1"] == 0.0)
                out[:, 1] = np.array(
                    [
                        _penalty(el, df.foldguess, df.oneMinusTwoone_logz)
                        for el in out[:, 0]
                    ]
                )
                MinResult = minimize_scalar(
                    _penalty,
                    bracket=(min_thresh, out[np.argmin(out[:, 1]), 0], 50.0),
                    args=(df.foldguess, df.oneMinusTwoone_logz),
                )
            except ValueError:
                print("Using level 2")
                try:
                    df.loc[:, "foldguess"] = ~(df["pcbelowthresh_2"] == 0.0)
                    out[:, 1] = np.array(
                        [
                            _penalty(el, df.foldguess, df.oneMinusTwoone_logz)
                            for el in out[:, 0]
                        ]
                    )
                    MinResult = minimize_scalar(
                        _penalty,
                        bracket=(min_thresh, out[np.argmin(out[:, 1]), 0], 50.0),
                        args=(df.foldguess, df.oneMinusTwoone_logz),
                    )
                except ValueError:
                    print("Assuming unfolded events")
                    exception = True
            finally:
                if axes:
                    axes.clear()
                    axes.plot(out[:, 0], out[:, 1])
            if not exception:
                optThresh = MinResult.x
                pen = _penalty(optThresh, df.foldguess, df.oneMinusTwoone_logz)
                pcError = pen / df.index.size * 100.0
            else:
                optThresh = df.oneMinusTwoone_logz.min()
                pen = _penalty(optThresh, df.foldguess, df.oneMinusTwoone_logz)
                pcError = -1
            file["/"].attrs.create("logZ_opt_threshold", optThresh)
            file["/"].attrs.create("logZ_penalty", pen)
            file["/"].attrs.create("logZ_pc_error", pcError)
            sql_set_value(usn, pwd, "alist", es.tag, "bayes_logZ_thresh", optThresh)
            sql_set_value(usn, pwd, "alist", es.tag, "bayes_pcerror", pcError)
    return optThresh, pen, pcError


def _calc_area_fit_duration(row, paramcol):
    if paramcol == "twoone_parameters":
        a = row["twoone_parameters_0"]
        b = (
            row["twoone_parameters_0"]
            + row["twoone_parameters_1"]
            + row["twoone_parameters_2"]
        )
    elif paramcol == "one_parameters":
        a = row["one_parameters_0"]
        b = row["one_parameters_0"] + row["one_parameters_1"]
    x = np.arange(row["current_nA"].size) / row["samplefreq_Hz"] * 1000.0
    return row["current_nA"][(x > a) & (x < b)].sum() * (x[1] - x[0]) * 1000.0


def calc_fold_stats(es: EventSet, overwrite=False):
    optThresh, pen, pcError = calc_logz_thresh(es, overwrite=overwrite)
    with es.get_bayes_file() as file:
        length = file["eventno"].size
        addfolded = False
        if file.get("folded", default=None) is None:
            ds = file.create_dataset("folded", shape=(length,), dtype=bool)
            addfolded = True
        if addfolded or overwrite:
            df = es.get_bayes_df_file(
                file,
                dsnames=[],
                rootdsnames=["oneMinusTwoone_logz", "twoone_parameters"],
                attributes=[],
            )
            cond = (df.loc[:, "oneMinusTwoone_logz"] < optThresh) & (
                df.loc[:, "twoone_parameters_3"] < df.loc[:, "twoone_parameters_4"]
            )
            cond = cond & ~(
                (
                    df.loc[:, "twoone_parameters_2"]
                    / (
                        df.loc[:, "twoone_parameters_1"]
                        + df.loc[:, "twoone_parameters_2"]
                    )
                    < 0.75
                )
                & (
                    df.loc[:, "twoone_parameters_3"]
                    > (es.levels[1, 0] + (es.levels[2, 0] - es.levels[1, 0]) / 2)
                )
            )
            file["folded"][...] = cond
            unfoldpc = (1 - cond.sum() / cond.size) * 100
            file["/"].attrs.create("unfoldpc", unfoldpc)
            sql_set_value(usn, pwd, "alist", es.tag, "unfoldpc", unfoldpc)
        rootdsnames = {
            "area_fitduration_fC": (np.float, (length,)),
            "fit_area_fC": (np.float, (length,)),
            "fit_duration_ms": (np.float, (length,)),
            "fit_levels_nA": (np.float, (length, 2)),
            "fit_increments_ms": (np.float, (length, 3)),
            "fit_foldpos": (np.float, (length,)),
            "fit_foldratio": (np.float, (length,)),
        }
        newrootdsnames = {
            key: value
            for key, value in rootdsnames.items()
            if file.get(key, default=None) is None
        }
        for key, value in newrootdsnames.items():
            ds = file.create_dataset(key, shape=value[1], dtype=value[0])
        if len(newrootdsnames) != 0 or overwrite:
            df = es.get_bayes_df_file(
                file,
                dsnames=[],
                rootdsnames=["eventno", "one_parameters", "twoone_parameters"],
                attributes=[],
            )
            evdf = es.get_dataframe(
                dsnames=["current_nA"], attributes=["eventno", "samplefreq_Hz"]
            )
            df = df.merge(evdf, on="group", suffixes=("_eventdf", "_bayesdf"))
            df["nan"] = np.ones((length,)) * np.nan
            df["twoone_parameters_0+1"] = (
                df["twoone_parameters_0"] + df["twoone_parameters_1"]
            )
            df["twoone_parameters_0+1+2"] = (
                df["twoone_parameters_0+1"] + df["twoone_parameters_2"]
            )
            df["one_parameters_0+1"] = df["one_parameters_0"] + df["one_parameters_1"]
            cond = file["folded"][...]
            if not cond.all():
                dfviewnofold = df.loc[~cond, :]
                file["fit_duration_ms"][~cond] = dfviewnofold["one_parameters_1"]
                file["fit_area_fC"][~cond] = (
                    dfviewnofold["one_parameters_1"]
                    * dfviewnofold["one_parameters_2"]
                    * 1000.0
                )
                file["fit_levels_nA"][~cond, :] = dfviewnofold.loc[
                    :, ["one_parameters_2", "nan"]
                ].values
                file["fit_increments_ms"][~cond, :] = dfviewnofold.loc[
                    :, ["one_parameters_0", "nan", "one_parameters_0+1"]
                ].values
                file["fit_foldpos"][~cond] = dfviewnofold["nan"]
                file["fit_foldratio"][~cond] = dfviewnofold["nan"]
                file["area_fitduration_fC"][~cond] = df.loc[~cond, :].apply(
                    _calc_area_fit_duration, axis=1, args=("one_parameters",)
                )
                fit_duration_mean_unfold_ms = file["fit_duration_ms"][~cond].mean()
                file["/"].attrs.create(
                    "fit_duration_mean_unfold_ms", fit_duration_mean_unfold_ms
                )
                sql_set_value(
                    usn,
                    pwd,
                    "alist",
                    es.tag,
                    "durationmean",
                    fit_duration_mean_unfold_ms,
                )
            else:
                file["/"].attrs.create("fit_duration_mean_unfold_ms", -1)
                sql_set_value(usn, pwd, "alist", es.tag, "durationmean", -1)
            if cond.any():
                dfviewfold = df.loc[cond, :]
                file["fit_duration_ms"][cond] = (
                    dfviewfold["twoone_parameters_1"]
                    + dfviewfold["twoone_parameters_2"]
                )
                file["fit_area_fC"][cond] = (
                    dfviewfold["twoone_parameters_1"]
                    * dfviewfold["twoone_parameters_3"]
                    + dfviewfold["twoone_parameters_2"]
                    * dfviewfold["twoone_parameters_4"]
                ) * 1000.0
                file["fit_levels_nA"][cond, :] = dfviewfold.loc[
                    :, ["twoone_parameters_4", "twoone_parameters_3"]
                ].values
                file["fit_increments_ms"][cond, :] = dfviewfold.loc[
                    :,
                    [
                        "twoone_parameters_0",
                        "twoone_parameters_0+1",
                        "twoone_parameters_0+1+2",
                    ],
                ].values
                file["fit_foldpos"][cond] = 1 / (
                    2
                    + dfviewfold["twoone_parameters_2"]
                    / dfviewfold["twoone_parameters_1"]
                )
                file["fit_foldratio"][cond] = dfviewfold["twoone_parameters_1"] / (
                    dfviewfold["twoone_parameters_1"]
                    + dfviewfold["twoone_parameters_2"]
                )
                file["area_fitduration_fC"][cond] = df.loc[cond, :].apply(
                    _calc_area_fit_duration, axis=1, args=("twoone_parameters",)
                )


def calc_fold_hist_stats(es: EventSet, overwrite=False, axes=None):
    with es.get_bayes_file() as bayesfile:
        if overwrite or bayesfile["/"].attrs.get("unfoldpc_hist", None) is None:
            unfoldpc_hist = 0
            df = es.get_dataframe(dsnames=["current_nA"], attributes=[])
            #            dfbay = es.getBayesDfFile(es.getBayesFile(), dsnames=[], rootdsnames=['above_logZ_cutoff'], attributes=[])
            df["current_min_nA"] = df["current_nA"].apply(np.min)
            (
                points,
                destimate,
                peakinds,
                ind,
                peakparams,
                peakdata,
                levels,
            ) = fit_many_gaussians(df.loc[:, "current_min_nA"].values, method="kde")
            mean, amplitude, width = levels[1]
            leftcut = mean - 2.0 * width
            rightcut = mean + 1.5 * width
            unfoldpc_hist = (
                1
                - (
                    destimate[(points > leftcut) & (points < rightcut)].sum()
                    * abs(points[1] - points[0])
                )
            ) * 100
            #            print(unfoldpc_hist)
            #            print(destimate.sum() * abs(points[1] - points[0]))
            if axes:
                axes.clear()
                axes.set_autoscale_on(True)
                axes.set_xlabel("Min. current (nA)")
                axes.set_ylabel("Prob. density")
                axes.plot(
                    points, destimate, "b", marker=".", markersize=3, linewidth=0.5
                )
                #                axes.hist(df['current_min_nA'].values, bins=70, density=True)
                axes.plot(
                    points[peakinds],
                    destimate[peakinds],
                    marker="x",
                    markersize=10,
                    linestyle="None",
                    color="m",
                    mew=3,
                )
                if peakparams is not None:
                    for i, arr in enumerate(peakdata):
                        axes.plot(
                            arr[0, :],
                            arr[1, :],
                            color="r",
                            marker=".",
                            markersize=3,
                            linewidth=0.5,
                        )
                axes.plot(
                    levels[:, 0],
                    levels[:, 1],
                    marker="x",
                    markersize=10,
                    linestyle="None",
                    color="k",
                    mew=3,
                )
                axes.axvline(leftcut, 0, 100, color="g", linewidth=2)
                axes.axvline(rightcut, 0, 100, color="g", linewidth=2)
            bayesfile["/"].attrs.create("unfoldpc_hist", unfoldpc_hist)


#            sqlSetValue(usn, pwd, 'alist', es.tag, 'unfoldpc_hist', unfoldpc_hist)


def calc_orientation_stats(es: EventSet, overwrite=False):
    with es.get_bayes_file() as bayesfile:
        if bayesfile.get("peak_pos", default=None) is not None:
            length = bayesfile["eventno"].size
            rootdsnames = {
                "no_peaks": (np.int, (length,)),
                "orientation": (np.int, (length,)),
            }
            newrootdsnames = {
                key: value
                for key, value in rootdsnames.items()
                if bayesfile.get(key, default=None) is None
            }
            for key, value in newrootdsnames.items():
                ds = bayesfile.create_dataset(key, shape=value[1], dtype=value[0])
            if len(newrootdsnames) != 0 or overwrite:
                df = es.get_bayes_df_file(
                    bayesfile,
                    dsnames=[],
                    rootdsnames=["folded", "above_logZ_cutoff"],
                    attributes=[],
                )
                fit_increments_ms, peak_pos, peak_mag = es.get_root_arrays(
                    bayesfile, ["fit_increments_ms", "peak_pos", "peak_mag"]
                )
                df["no_peaks"] = np.count_nonzero(peak_mag, axis=1)
                cond = (
                    (df["folded"] == False)
                    & (df["above_logZ_cutoff"] == True)
                    & (df["no_peaks"] == 1)
                )
                start = fit_increments_ms[:, 0]
                stop = fit_increments_ms[:, 2]
                duration = stop - start
                pos = peak_pos[:, 0]
                first = (pos > start) & (pos < (start + duration * 0.25))
                last = (pos > (stop - duration * 0.25)) & (pos < stop)
                df["orientation"] = 0
                df.loc[cond & first, "orientation"] = 1
                df.loc[cond & last, "orientation"] = 2
                counts = df["orientation"].value_counts()
                try:
                    anchorlastpc = (
                        counts.get(2, default=0)
                        / (counts.get(2, default=0) + counts.get(1, default=0))
                        * 100.0
                    )
                except ZeroDivisionError:
                    anchorlastpc = -1
                bayesfile["no_peaks"][...] = df["no_peaks"]
                bayesfile["orientation"][...] = df["orientation"]
                bayesfile["/"].attrs.create("anchorlastpc", anchorlastpc)
                bayesfile["/"].attrs.create(
                    "noevents_peakfilt",
                    counts.get(1, default=0) + counts.get(2, default=0),
                )


def calc_peak_stats(es: EventSet, overwrite=False, axes=None):
    with es.get_bayes_file() as bayesfile:
        if bayesfile.get("peak_pos", default=None) is not None:
            length = bayesfile["eventno"].size
            rootdsnames = {
                "no_peaks": (np.int, (length,)),
                "n_clustered_peaks": (np.int, (length,)),
                "clustered_peaks_bindex": (bool, (length, 10)),
                "peak_pos_norm": (np.float, (length, 10)),
            }
            newrootdsnames = {
                key: value
                for key, value in rootdsnames.items()
                if bayesfile.get(key, default=None) is None
            }
            for key, value in newrootdsnames.items():
                ds = bayesfile.create_dataset(key, shape=value[1], dtype=value[0])
            if len(newrootdsnames) != 0 or overwrite:
                df = es.get_bayes_df_file(
                    bayesfile,
                    dsnames=[],
                    rootdsnames=["folded", "above_logZ_cutoff"],
                    attributes=[],
                )
                (
                    fit_increments_ms,
                    fit_duration_ms,
                    peak_pos,
                    peak_mag,
                ) = es.get_root_arrays(
                    bayesfile,
                    ["fit_increments_ms", "fit_duration_ms", "peak_pos", "peak_mag"],
                )
                df["no_peaks"] = np.count_nonzero(peak_mag, axis=1)
                start = fit_increments_ms[:, 0]
                peak_pos_norm = np.zeros(peak_pos.shape)
                for k in range(peak_pos.shape[1]):
                    cond = peak_pos[:, k] != 0
                    peak_pos_norm[cond, k] = (
                        peak_pos[cond, k] - start[cond]
                    ) / fit_duration_ms[cond]
                diff = np.diff(peak_pos)
                cluster_bindex = np.zeros(peak_pos.shape, dtype=bool)
                override_diff_threshold = bayesfile["/"].attrs.get(
                    "override_diff_threshold", None
                )
                if override_diff_threshold is None or override_diff_threshold is False:
                    (
                        points,
                        destimate,
                        peakinds,
                        ind,
                        peakparams,
                        peakdata,
                        gaussfit,
                        usinghwhm,
                    ) = fit_one_gaussian(diff[diff > 0], method="kde")
                    argmax = destimate.argmax()
                    y_fit = destimate[: argmax * 2]
                    x_fit = points[: argmax * 2]
                    p0 = [points[argmax], destimate[argmax], points[argmax] * 2]
                    params, _ = curve_fit(gauss, x_fit, y_fit, p0=p0)
                    params[2] = abs(params[2])
                    (mean, amplitude, width) = params
                    cutmultiplier = 1.5
                    diff_threshold = mean + cutmultiplier * width
                    bayesfile["/"].attrs.create("diff_threshold_ms", diff_threshold)
                    if axes:
                        axes.clear()
                        axes.set_autoscale_on(True)
                        axes.set_xlabel("diff (ms)")
                        axes.set_ylabel("Prob. density")
                        axes.plot(
                            points,
                            destimate,
                            "b",
                            marker=".",
                            markersize=3,
                            linewidth=0.5,
                        )
                        axes.plot(
                            x_fit,
                            gauss(x_fit, *params),
                            "b",
                            marker=".",
                            markersize=3,
                            linewidth=0.5,
                            color="m",
                        )
                        axes.axvline(diff_threshold, 0, 100, color="g", linewidth=2)
                else:
                    diff_threshold = bayesfile["/"].attrs.get("diff_threshold_ms", None)
                    if diff_threshold is None:
                        print(
                            "override_diff_threshold is set to True, but not diff_threshold has been set. Using"
                            " 0.1 ms."
                        )
                        diff_threshold = 0.1
                diff_bindex = (diff > 0) & (diff < diff_threshold)
                for k in range(diff_bindex.shape[1]):
                    cluster_bindex[:, k] = diff_bindex[:, k] | cluster_bindex[:, k]
                    cluster_bindex[:, k + 1] = diff_bindex[:, k]
                n_clustered_peaks = np.count_nonzero(cluster_bindex, axis=1)
                if bayesfile["/"].attrs.get("n_peaks_expected", None) is None:
                    print(
                        "The number of expected peaks hasn't been added, can't calculate yield percentage"
                    )
                else:
                    n_exp = bayesfile["/"].attrs.get("n_peaks_expected", None)
                    # Take unfolded events that have the right number of peaks in a cluster
                    condBase = (
                        df["folded"] == False
                    )  # & (df['above_logZ_cutoff'] == True)
                    condCount = condBase & (n_clustered_peaks == n_exp)
                    peakpc = np.count_nonzero(condCount) / np.count_nonzero(condBase)
                    bayesfile["/"].attrs.create("peakpc", peakpc)
                bayesfile["no_peaks"][...] = df["no_peaks"]
                bayesfile["n_clustered_peaks"][...] = n_clustered_peaks
                bayesfile["clustered_peaks_bindex"][...] = cluster_bindex
                bayesfile["peak_pos_norm"][...] = peak_pos_norm


def get_agg_data(eslist, overwrite=False):
    attlist = [
        "noise_nA",
        "unfoldpc",
        "unfoldpc_hist",
        "anchorlastpc",
        "noevents_peakfilt",
        "logZ_opt_threshold",
        "logZ_penalty",
        "logZ_pc_error",
        "fit_duration_mean_unfold_ms",
        "diff_threshold_ms",
        "n_peaks_expected",
        "peakpc",
    ]
    d = {"tag": [], "noevents_filt": []}
    d.update({key: [] for key in attlist})
    for es in eslist:
        print("***************************************************************")
        print(es.tag)
        # The order of these is important
        calc_fold_stats(es, overwrite)
        calc_fold_hist_stats(es, overwrite)
        #        calcLogZcutoff(es, overwrite)
        #        calcOrientationStats(es, overwrite)
        calc_peak_stats(es, overwrite)
        d["tag"].append(es.tag)
        d["noevents_filt"].append(
            es.get_bayes_dataframe(
                dsnames=[], rootdsnames=["eventno"], attributes=[]
            ).index.size
        )
        rootatts = es.get_bayes_root_attrs(attrs="full")
        for att in attlist:
            d[att].append(rootatts.get(att, None))
        es.close()
    df = pd.DataFrame(data=d)
    return df


def get_tagged_df(taglist, overwrite=False):
    eslist = [EventSet(get_tag_path(tag), filterindex=True) for tag in taglist]
    dfFile = get_agg_data(eslist, overwrite=overwrite)
    collist = [
        "tag",
        "ddate",
        "who",
        "channel",
        "voltage",
        "salt",
        "salt_conc",
        "buffer",
        "sample",
        "sample_conc",
        "comment",
        "pH",
        "pH_trans",
        "noevents",
        "start_current",
        "frequency",
        "durationmean",
        "ecdmean",
        "level1mean",
        "level2mean",
        "resistance",
        "recratio",
        "poresize",
        "eventsanalysed",
        "ddatea",
        "unfoldpc",
    ]
    dfSql = sql_to_container(taglist, collist)
    dfSql["frequ_norm"] = (
        dfSql["frequency"]
        / dfSql["sample_conc"]
        * dfFile["noevents_filt"]
        / dfSql["noevents"]
    )
    df = dfSql.merge(dfFile, how="outer", on="tag", suffixes=("_sql", "_file"))
    return df


def get_tagged_full_df(taglist, overwrite=False, use_sql=True):
    eslist = [EventSet(get_tag_path(tag), filterindex=True) for tag in taglist]
    return _get_tagged_full_bayesdf(eslist, overwrite, use_sql)


def _get_tagged_full_bayesdf(eslist, overwrite=False, use_sql=True):
    dflist = []
    for es in eslist:
        print("***************************************************************")
        print(es.tag)
        calc_fold_stats(es, overwrite)
        # calc_logz_cutoff(es, overwrite)
        calc_peak_stats(es, overwrite)
        df = es.get_bayes_dataframe(
            dsnames=[],
            rootdsnames=[
                "eventno",
                "area_fitduration_fC",
                "fit_area_fC",
                "fit_duration_ms",
                "fit_foldpos",
                "fit_foldratio",
                "fit_increments_ms",
                "fit_levels_nA",
                "folded",
            ],
            attributes=[],
        )
        df["tag"] = es.tag
        if use_sql:
            for x in ["voltage", "pH", "salt", "salt_conc", "buffer", "poresize"]:
                df[x] = sql_get_value(es.tag, x)
        dflist.append(df)
        es.close()
    return pd.concat(dflist, ignore_index=True)


def get_full_df(es: EventSet):
    eventdf = es.get_dataframe()
    eventdf = eventdf.astype({"eventno": np.int})
    bayesdf = es.get_bayes_dataframe()
    df = eventdf.merge(
        bayesdf.loc[
            :, [x for x in bayesdf.columns if bayesdf.loc[:, x].dtype != object]
        ],
        on="eventno",
        suffixes=("_eventdf", "_bayesdf"),
    )
    return df


def split_tag_csv(tagcsv):
    l = tagcsv.split(",")
    l = [x.strip() for x in l]
    return l
