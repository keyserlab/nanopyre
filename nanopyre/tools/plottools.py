#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Nov 29 16:05:19 2017


"""

import numpy as np
import pandas as pd
from scipy.optimize import curve_fit
from datetime import date
from scipy.stats import gaussian_kde

from matplotlib import rc
from matplotlib import rcParams
from matplotlib import pyplot as plt
from matplotlib.ticker import MultipleLocator, MaxNLocator
import seaborn as sns

from nanopyre.tools import postproctools as pproc


# rc('font', family='serif')
# rc('font', serif='Times')

# rc('font', family='serif', serif='CMU Serif')
# rc('font', family='serif', serif='CMU Sans Serif')
rc("text", usetex=False)
rc("ps", usedistiller="xpdf")
rcParams["svg.fonttype"] = "none"

boxline = 0.5
linewidth = 1.5
errlinewidth = 1.0
# rc('axes', linewidth=boxline)
figsize = (12, 9)

labelsize = 10
smallsize = 10
mrkrsize = 4
# colors = ['b', 'k', 'r', 'g', 'c', 'm', 'y', 'g', 'burlywood']
colors = sns.color_palette()
markers = ["o", "^", "s", "p", "h", "1", "x", "*"]
# markers = ['o', 'x', '^', 's', 'p', 'h', '1', 'x', '*']


def _set_globals(linewidth, fontsize, markersize):
    global labelsize
    global smallsize
    global boxline
    global mrkrsize
    labelsize = fontsize
    smallsize = fontsize
    boxline = linewidth
    mrkrsize = markersize


def set_mode(mode):
    if mode == "paper":
        _set_globals(0.5, 10, 4)
    elif mode == "presentation":
        _set_globals(1.0, 14, 6)


def prettify_axes(ax, **kwargs):
    xlabel = kwargs.pop("xlabel", "X")
    ylabel = kwargs.pop("ylabel", "Y")
    noxlabel = kwargs.pop("noxlabel", False)
    noylabel = kwargs.pop("noylabel", False)
    xticks = kwargs.pop("xticks", None)
    yticks = kwargs.pop("yticks", None)
    xmin = kwargs.pop("xmin", ax.get_xlim()[0])
    xmax = kwargs.pop("xmax", ax.get_xlim()[1])
    ymin = kwargs.pop("ymin", ax.get_ylim()[0])
    ymax = kwargs.pop("ymax", ax.get_ylim()[1])
    xmult = kwargs.pop("xtickmult", None)
    ymult = kwargs.pop("ytickmult", None)
    ax.tick_params(
        axis="both",
        which="both",
        width=boxline,
        labelsize=smallsize,
        direction="in",
        top=True,
        right=True,
    )
    ax.tick_params(axis="both", which="major", length=smallsize * 0.4)
    ax.tick_params(axis="both", which="minor", length=smallsize * 0.2)
    _prettify_xaxis(ax, xlabel, noxlabel, xticks, xmin, xmax, xmult)
    _prettify_yaxis(ax, ylabel, noylabel, yticks, ymin, ymax, ymult)


def _prettify_xaxis(ax, xlabel, noxlabel, xticks, xmin, xmax, xtickmult):
    for axis in ["bottom", "top"]:
        ax.spines[axis].set_linewidth(boxline)
    if noxlabel:
        xlabel = " "
    ax.set_xlabel(xlabel, fontsize=labelsize)
    if xticks is not None:
        ax.set_xticks(xticks)
    else:
        ax.xaxis.set_major_locator(MaxNLocator(nbins=4, prune="both"))
    xticks = ax.get_xticks()
    xmult = xtickmult if xtickmult is not None else (xticks[1] - xticks[0]) / 2
    ax.xaxis.set_minor_locator(MultipleLocator(xmult))
    minorxticks = ax.get_xticks(minor=True)
    minorxticks = [
        tick
        for tick in minorxticks
        if not (tick == xmin) and not (tick == xmax) and not (tick in xticks)
    ]
    ax.set_xticks(minorxticks, minor=True)
    ax.set_xlim(xmin, xmax)
    dropindex = [i for i, tick in enumerate(xticks) if (tick == xmin) or (tick == xmax)]
    xticks = ax.xaxis.get_major_ticks()
    for i in dropindex:
        xticks[i].tick1On = False
        xticks[i].tick2On = False
        try:
            xticks[i].tick1line.remove()
        except:
            pass
        try:
            xticks[i].tick2line.remove()
        except:
            pass


def _prettify_yaxis(ax, ylabel, noylabel, yticks, ymin, ymax, ytickmult):
    for axis in ["left", "right"]:
        ax.spines[axis].set_linewidth(boxline)
    if noylabel:
        ylabel = " "
    ax.set_ylabel(ylabel, fontsize=labelsize)
    if yticks is not None:
        ax.set_yticks(yticks)
    else:
        ax.yaxis.set_major_locator(MaxNLocator(nbins=4, prune="both"))
    yticks = ax.get_yticks()
    ymult = ytickmult if ytickmult is not None else (yticks[1] - yticks[0]) / 2
    ax.yaxis.set_minor_locator(MultipleLocator(ymult))
    minoryticks = ax.get_yticks(minor=True)
    minoryticks = [
        tick
        for tick in minoryticks
        if not (tick == ymin) and not (tick == ymax) and not (tick in yticks)
    ]
    ax.set_yticks(minoryticks, minor=True)
    ax.set_ylim(ymin, ymax)
    dropindex = [i for i, tick in enumerate(yticks) if (tick == ymin) or (tick == ymax)]
    yticks = ax.yaxis.get_major_ticks()
    for i in dropindex:
        yticks[i].tick1On = False
        yticks[i].tick2On = False
        try:
            yticks[i].tick1line.remove()
        except:
            pass
        try:
            yticks[i].tick2line.remove()
        except:
            pass


def plot_lines(ax, xarray, yarray, **kwargs):
    yerrs = kwargs.pop("yerrs", [[None] * len(el) for el in xarray])
    annotate = kwargs.pop("annotate", [[None] * len(el) for el in xarray])
    linestyle = kwargs.pop("linestyle", [[""] * len(el) for el in xarray])
    cmap = kwargs.pop("cmap", [[colors[i]] * len(el) for i, el in enumerate(xarray)])
    mmap = kwargs.pop("mmap", [[markers[i]] * len(el) for i, el in enumerate(xarray)])
    markersize = kwargs.pop("markersize", [[mrkrsize] * len(el) for el in xarray])
    legend = kwargs.pop("legend", [])
    legloc = kwargs.pop("legloc", 0)
    plots = []
    for i, elx in enumerate(xarray):
        plot = None
        for k, x in enumerate(elx):
            y = yarray[i][k]
            if yerrs[i][k] is None:
                if markersize[i][k] == 0:
                    (plot,) = ax.plot(
                        x,
                        y,
                        marker=mmap[i][k],
                        markersize=0,
                        color=cmap[i][k],
                        linestyle=linestyle[i][k],
                        linewidth=linewidth,
                    )
                else:
                    (plot,) = ax.plot(
                        x,
                        y,
                        marker=mmap[i][k],
                        markersize=markersize[i][k],
                        color=cmap[i][k],
                        linestyle="",
                    )
                    ax.plot(
                        x,
                        y,
                        marker=mmap[i][k],
                        markersize=markersize[i][k],
                        color=cmap[i][k],
                        linestyle=linestyle[i][k],
                        linewidth=linewidth,
                    )
            else:
                (plot,) = ax.plot(
                    x,
                    y,
                    marker=mmap[i][k],
                    markersize=markersize[i][k],
                    color=cmap[i][k],
                    linestyle="",
                )
                ax.errorbar(
                    x,
                    y,
                    yerrs[i][k],
                    elinewidth=errlinewidth,
                    capthick=errlinewidth,
                    capsize=3,
                    marker=mmap[i][k],
                    mec=cmap[i][k],
                    markersize=markersize[i][k],
                    color=cmap[i][k],
                    linestyle=linestyle[i][k],
                    linewidth=linewidth,
                )
            if annotate[i][k] is not None:
                for j, ann in enumerate(annotate[i][k]):
                    ax.annotate(ann.replace("_", " "), (x[j] + 0.01, y[j] + 0.01))
        plots.append(plot)
    if legend:
        leg = ax.legend(
            plots,
            legend,
            numpoints=1,
            fontsize=smallsize,
            loc=legloc,
            fancybox=False,
            edgecolor="k",
            handlelength=0.8,
            handletextpad=0.3,
            borderaxespad=0.75,
            framealpha=1,
        )
        leg.get_frame().set_linewidth(boxline)
    prettify_axes(ax, **kwargs)
    return ax


def plot_bars(ax, xarray, yarray, xlabels=None, **kwargs):
    yerrs = kwargs.pop("yerrs", [[None] * len(el) for el in yarray])
    annotate = kwargs.pop("annotate", [[None] * len(el) for el in yarray])
    cmap = kwargs.pop("cmap", [[colors[i]] * len(el) for i, el in enumerate(yarray)])
    edgecolor = kwargs.pop(
        "edgecol", [[colors[i]] * len(el) for i, el in enumerate(yarray)]
    )
    linew = kwargs.pop(
        "linewidth", [[linewidth] * len(el) for i, el in enumerate(yarray)]
    )
    boxw = kwargs.pop("boxwidth", [[0.5] * len(el) for i, el in enumerate(yarray)])
    legend = kwargs.pop("legend", [])
    legloc = kwargs.pop("legloc", 0)
    hatch = kwargs.pop("hatch", [[None] * len(el) for el in yarray])
    if xlabels is None:
        xlabels = ([], [])
    bars = []
    for i, elx in enumerate(xarray):
        for k, x in enumerate(elx):
            bar = ax.bar(
                x,
                yarray[i][k],
                yerr=yerrs[i][k],
                width=boxw[i][k],
                linewidth=linew[i][k],
                color=cmap[i][k],
                edgecolor=[edgecolor[i][k]] * len(x),
                label=annotate[i][k],
                hatch=hatch[i][k],
            )
        bars.append(bar)
    if legend:
        leg = ax.legend(
            bars,
            legend,
            numpoints=1,
            fontsize=smallsize,
            loc=legloc,
            fancybox=False,
            edgecolor="k",
            handlelength=0.8,
            handletextpad=0.3,
            borderaxespad=0.75,
            align="center",
        )
        leg.get_frame().set_linewidth(boxline)
    ylabel = kwargs.pop("ylabel", "Y")
    noylabel = kwargs.pop("noylabel", False)
    yticks = kwargs.pop("yticks", None)
    ymin = kwargs.pop("ymin", ax.get_ylim()[0])
    ymax = kwargs.pop("ymax", ax.get_ylim()[1])
    ymult = kwargs.pop("ytickmult", None)
    ax.set_xticks(xlabels[0])
    ax.set_xticklabels(xlabels[1])
    ax.tick_params(axis="x", which="both", width=0, labelsize=smallsize, direction="in")
    ax.tick_params(
        axis="y",
        which="both",
        width=boxline,
        labelsize=smallsize,
        direction="in",
        top=True,
        right=True,
    )
    _prettify_yaxis(ax, ylabel, noylabel, yticks, ymin, ymax, ymult)
    return ax


def cluster_bars(labels, yarray, mask=None):
    xarray = []
    yarrayout = []
    xlabelpos = []
    if mask is None:
        mask = yarray
    pos = 1
    for i, elm in enumerate(mask):
        elx = []
        elyout = []
        for k, m in enumerate(elm):
            if len(m) != 0:
                elx.append(np.arange(pos, pos + len(m)))
                pos += len(m)
                y = yarray[i][k]
                elyout.append(
                    np.pad(y, (0, len(m) - len(y)), "constant", constant_values=-1)
                )
        pos += 2
        xlabelpos.append(np.median(np.concatenate(elx)))
        xarray.append(elx)
        yarrayout.append(elyout)
    return xarray, yarrayout, (xlabelpos, labels)


def plot_hist_layered(ax, array, **kwargs):
    cmap = kwargs.pop("cmap", [[colors[i]] * len(el) for i, el in enumerate(array)])
    legend = kwargs.pop("legend", [])
    legloc = kwargs.pop("legloc", 0)
    kwargs["ylabel"] = kwargs.pop("ylabel", "Normalized Count")
    patches = []
    for i, elx in enumerate(array):
        for k, x in enumerate(elx):
            n, bins, patch = ax.hist(x, bins=70, density=True, color=cmap[i][k])
        patches.append(patch[0])
    if legend:
        leg = ax.legend(
            patches,
            legend,
            numpoints=1,
            fontsize=smallsize,
            loc=legloc,
            fancybox=False,
            edgecolor="k",
            handlelength=0.8,
            handletextpad=0.3,
            borderaxespad=0.75,
        )
        leg.get_frame().set_linewidth(boxline)
    prettify_axes(ax, **kwargs)
    return ax


def plot_hist(ax, array, **kwargs):
    cmap = kwargs.pop("cmap", [colors[i] for i, elx in enumerate(array) for x in elx])
    legend = kwargs.pop("legend", [])
    legloc = kwargs.pop("legloc", 0)
    kwargs["ylabel"] = kwargs.pop("ylabel", "Normalized Count")
    array = [x for i, elx in enumerate(array) for x in elx]
    n, bins, patches = ax.hist(array, bins=70, density=True, color=cmap)
    if legend:
        leg = ax.legend(
            patches,
            legend,
            numpoints=1,
            fontsize=smallsize,
            loc=legloc,
            fancybox=False,
            edgecolor="k",
            handlelength=0.8,
            handletextpad=0.3,
            borderaxespad=0.75,
        )
        leg.get_frame().set_linewidth(boxline)
    prettify_axes(ax, **kwargs)
    return ax


def _cm_to_inches(cmwidth, cmheight):
    #    inchwid = cmwidth*0.3937*10.8/10.24
    inchwid = cmwidth * 0.393701
    #    inchheight = cmheight*0.3937*10.3/9.34
    inchheight = cmheight * 0.393701
    return (inchwid, inchheight)


def set_fig_size(fig, figsize):
    fig.set_size_inches(_cm_to_inches(*figsize))


def save_current_fig(**kwargs):
    path = kwargs.pop("path", "/data/")
    file = kwargs.pop("file", "unnamed")
    form = kwargs.pop("format", ".png")
    fig = plt.gcf()
    if not kwargs.pop("nolayout", False):
        set_fig_size(fig, figsize)
        fig.tight_layout()
    #    fig.savefig(path+file+form, bbox_inches='tight', dpi=96)
    fig.savefig(path + file + form, dpi=300, format=form[1:])


def exponential(x, amp, xoffset, yoffset):
    y = amp * np.exp(-x - xoffset) + yoffset
    return y


def fit_function(ax, xarray, yarray, func, **kwargs):
    xarrin = [[np.concatenate(elx)] for elx in xarray]
    yarrin = [[np.concatenate(ely)] for ely in yarray]
    param = [[curve_fit(func, x[0], yarrin[i][0])[0]] for i, x in enumerate(xarrin)]
    xmax = max([max(x[0]) for x in xarrin]) * 1.1
    xmin = min([min(x[0]) for x in xarrin]) * 0.5
    xarrout = [[np.linspace(xmin, xmax, 500)] for elx in xarrin]
    yarrout = [[func(x[0], *param[i][0])] for i, x in enumerate(xarrout)]
    kwargs["linestyle"] = [["-"] * len(el) for el in xarray]
    kwargs["markersize"] = [[0] * len(el) for el in xarray]
    plot_lines(ax, xarrout, yarrout, **kwargs)


def filter_full_df(df, where, wherefull, dffull=None):
    cond = pd.Series(np.ones(df.index.size) * True, dtype=bool)
    for w in where:
        cond = cond & (df[w[0]] == w[1])
    taglist = df.loc[cond, "tag"].tolist()
    if dffull is not None:
        dv = dffull.loc[dffull["tag"].isin(taglist), :]
    else:
        dv = pproc.get_tagged_full_df(taglist, False)
    cond = pd.Series(np.ones(dv.size) * True, dtype=bool)
    for w in wherefull:
        cond = cond & (dv[w[0]] == w[1])
    dv = dv.loc[cond, :]
    return dv


def plot_fold_distr(ax, dv, varcol, method):
    dv = dv.loc[dv["folded"] == True, :]
    xarrin, yarrin, annotate, yerrs, labels = dataselector(
        dv, "eventno", "fit_foldpos", varcol, "tag", errs=None, mean=False
    )
    xarray = [[] for ely in yarrin]
    yarray = [[] for ely in yarrin]
    for i, ely in enumerate(yarrin):
        for itemy in ely:
            if method == "kde":
                kde = gaussian_kde(itemy, bw_method="scott")
                points = np.linspace(-0.1, 0.6, 1000)
                y = kde(points)
            elif method == "hist":
                y, edges = np.histogram(itemy, bins=20, density=True)
                points = edges[:-1] + (edges[1] - edges[0]) / 2
            xarray[i].append(points)
            yarray[i].append(y)
    kwargs = {
        "xlabel": "Relative position of fold",
        "ylabel": "Probability density",
        "xmin": 0.0,
        "xmax": 0.5,
        "xticks": [0.0, 0.25, 0.5],
        "xtickmult": 0.1,
        "ymin": 0.0,
        "ymax": 10.0,
        "yticks": [0.0, 5, 10.0],
        "ytickmult": 1.0,
        "file": date.today().strftime("%y%m%d") + "_scatter",
        "legloc": 1,
        "legend": labels,
        # 'markersize': [[0]*len(el) for el in xarray],
        "linestyle": [["-"] * len(el) for el in xarray],
    }
    plot_lines(ax, xarray, yarray, **kwargs)


def plot_scatter_many(ax, dv, varcol, **kwargs):
    xarray, yarray, annotate, yerrs, labels = dataselector(
        dv, "fit_duration_ms", "fit_levels_nA_0", varcol, "tag", errs=None, mean=False
    )
    _plot_scatter(ax, xarray, yarray, labels, kwargs)


def plot_scatter(ax, dv, **kwargs):
    xarray, yarray, annotate, yerrs, labels = dataselector(
        dv, "fit_duration_ms", "fit_levels_nA_0", "tag", "tag", errs=None, mean=False
    )
    _plot_scatter(ax, xarray, yarray, labels, kwargs)


def _plot_scatter(ax, xarray, yarray, labels, kwargs):
    kwargs["xlabel"] = kwargs.pop("xlabel", "Event duration (ms)")
    kwargs["ylabel"] = kwargs.pop("ylabel", "Mean event current (nA)")
    kwargs["file"] = kwargs.pop("file", date.today().strftime("%y%m%d") + "_scatter")
    kwargs["legend"] = kwargs.pop("legend", labels)
    kwargs["xmin"] = kwargs.pop("xmin", 0.0)
    kwargs["xmax"] = kwargs.pop("xmax", 3.0)
    kwargs["xticks"] = kwargs.pop("xticks", [0.0, 1.0, 2.0])
    kwargs["xtickmult"] = kwargs.pop("xtickmult", 0.5)
    kwargs["ymin"] = kwargs.pop("ymin", -0.3)
    kwargs["ymax"] = kwargs.pop("ymax", 0.01)
    kwargs["yticks"] = kwargs.pop("yticks", [-0.3, -0.2, -0.1, 0.0])
    kwargs["ytickmult"] = kwargs.pop("ytickmult", 0.05)
    kwargs["legloc"] = kwargs.pop("legloc", 4)
    kwargs["markersize"] = kwargs.pop("markersize", [[3] * len(el) for el in xarray])
    kwargs["mmap"] = [
        ["o", "^", "s", "p", "h", "1", "2", "3", "4", "8", "x", "*"] for xel in xarray
    ]
    plot_lines(ax, xarray, yarray, **kwargs)


def dataselector(
    container, xcol, ycol, cat1, cat2, errs=None, mean=False, anncol="tag"
):
    xarray = []
    yarray = []
    annotate = []
    yerrs = []
    labels = []
    if type(cat1) is str:
        loop1 = np.sort(container[cat1].unique())
    else:
        loop1 = np.array(cat1[1])
        cat1 = cat1[0]
    for c1 in loop1:
        elx = []
        ely = []
        elz = []
        eld = []
        if not mean:
            blob = container.loc[container[cat1] == c1, :]
            if type(cat2) is str:
                loop2 = np.sort(blob[cat2].unique())
                cat2temp = cat2
            else:
                loop2 = np.array(cat2[1])
                cat2temp = cat2[0]
            for c2 in loop2:
                arrx = blob.loc[blob[cat2temp] == c2, xcol].values
                sort = arrx.argsort()
                elx.append(arrx[sort])
                ely.append(blob.loc[blob[cat2temp] == c2, ycol].values[sort])
                annotations = list(blob.loc[blob[cat2temp] == c2, anncol].values[sort])
                elz.append([str(ann) for ann in annotations])
                if errs is None:
                    eld.append(None)
                else:
                    eld.append(blob.loc[blob[cat2temp] == c2, errs].values[sort])
        else:
            if type(cat2) is str or cat2 is None:
                blob = container.loc[(container[cat1] == c1), :]
            else:
                c2 = np.array(cat2[1][0])
                cat2temp = cat2[0]
                blob = container.loc[
                    (container[cat1] == c1) & (container[cat2temp] == c2), :
                ]
            arrx = []
            arry = []
            arrz = []
            arrd = []
            for cx in np.sort(blob[xcol].unique()):
                arrx.append(cx)
                arry.append(blob.loc[blob[xcol] == cx, ycol].values.mean())
                arrz.append(str(cx))
                std = (
                    blob.loc[blob[xcol] == cx, ycol].values.std()
                    if (blob.loc[blob[xcol] == cx, ycol].values.std() != 0.0)
                    else np.nan
                )
                arrd.append(std)
            elx.append(np.array(arrx))
            ely.append(np.array(arry))
            elz.append(arrz)
            eld.append(np.array(arrd))
        xarray.append(elx)
        yarray.append(ely)
        annotate.append(elz)
        yerrs.append(eld)
        labels.append(c1)
    return xarray, yarray, annotate, yerrs, labels
