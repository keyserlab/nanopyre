#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue May  9 13:57:43 2017


"""

import matplotlib.pyplot as plt
import numpy as np
import h5py as h5

from os import makedirs
from os.path import exists, join
from shutil import rmtree
from datetime import date, datetime

from nanopyre.tools.parallels import Parallels
from nanopyre.io.eventset import EventSet, EventError
from nanopyre.io.configio import read_config, imprt
from nanopyre.io.sqlio import SqlIO, SqlError
from nanopyre.analysis.bayes_nested_sampling import BayesRun, BayesFuncs
from nanopyre.analysis.threshold_volume import percent_below
from nanopyre.analysis import peakfinder

import readline
from getpass import getpass


class EventAnalyser:
    def __init__(self):
        print()
        print("*********************************************************")
        print("Eventanalyser")
        print("*********************************************************")
        self.commands = {
            "status": (
                self.proc_status,
                "status \t\t\t\t prints the status of Bayesian analysis runs",
            ),
            "info": (self.inform, "info \t\t\t\t print this info message"),
            "exit": (self.close, "exit \t\t\t\t closes this program"),
            "bay": (
                self.bayes,
                "bay(tag1, tag2, ...) \t\t starts Bayesian analysis runs for measurements "
                "(tag1, tag2, ...) in the background, depending on the number of CPUs "
                "some may be put in a queue",
            ),
            "killall": (
                self.kill_all_procs,
                "killall \t\t\t terminates all running Bayesian analysis "
                "runs and empties the queue (this is NOT a clean kill, you "
                "will have to restart the runs)",
            ),
            "changemode": (
                self.change_mode,
                "changemode(newmode) \t\t changes analysis mode to either "
                '"folding" or "peaks"',
            ),
        }

        self.p = Parallels(self.process_finished, self.delete_when_stuck)
        self.proclengths = {}

        self.nsbasedir = read_config()["Eventanalyser"][
            "Nested sampling base directory"
        ]
        self.analysisroot = read_config()["Eventanalyser"]["Tag folder root path"]
        self.mode = "folding"
        print('Your nested sampling base directory is "' + self.nsbasedir + '"')
        print('Your current root path for tag-folders is "' + self.analysisroot + '"')
        print('You can change these values in "nanopyre/config.txt"')
        print("")
        print(
            'The analysis mode is "'
            + self.mode
            + '", change this with the "changemode(newmode)" command '
            "(newmode = folding or peaks)"
        )
        print("")
        self.sql = SqlIO()
        success = False
        while not success:
            self.usn = input("Input SQL user name: ")
            self.pwd = getpass("Input SQL password: ")
            try:
                self.sql.login(self.usn, self.pwd)
            except SqlError:
                print("Wrong login details, try again")
            else:
                success = True
        self.running = True
        self.loop()

    def inform(self):
        print()
        print("Eventanalyser info message:")
        print()
        for key in sorted(self.commands.keys()):
            print(self.commands[key][1])

    def change_mode(self, newmode):
        if newmode == "folding" or newmode == "peaks":
            self.mode = newmode
            print("Mode changed to " + newmode)
        else:
            print("Error: Unable to change mode, enter valid option (folding or peaks)")

    def close(self):
        p, q = self.p.get_info()
        if p or q:
            print(
                'Cannot exit while processes are still running, check with "status" or kill with "killall"'
            )
        else:
            print("Exiting...")
            self.sql.closeconn()
            self.running = False

    def loop(self):
        while self.running:
            print()
            ipt = input(">> ")
            cmd = ipt.split("(")[0]
            if not cmd:
                continue
            try:
                args = ipt.split("(")[1].rsplit(")", 1)[0]
                args = args.split(",")
                args = [x.strip() for x in args if not x.isspace()]
                args = [x.strip("'") for x in args]
            except IndexError:
                args = []
            try:
                func = self.commands[cmd][0]
            except KeyError:
                print("Not a command")
                continue
            try:
                func(*args)
            except TypeError:
                print("Wrong syntax, give right no of arguments")
                continue

    def tag_eventspath(self, tag):
        path = join(self.analysisroot, tag, "events", "")
        if exists(path):
            return path
        else:
            print('"' + path + '" does not exist')
            raise PathError

    def make_axes(self):
        fig = plt.figure()
        ax = fig.add_subplot(111)
        return ax

    def close_figures(self):
        plt.close("all")

    # Process management

    def proc_status(self):
        proclist, queuelist = self.p.get_info()
        print()
        print("The following processes are running: ")
        for item in proclist:
            total = self.proclengths.get(item[0], -1)
            print(
                item[0]
                + "\t"
                + "{:.1f}".format(item[1] / total * 100.0)
                + " % done, "
                + str(total)
                + " events to "
                "analyse in total"
            )
        print("The following tasks are waiting in line: ")
        for item in queuelist:
            print(
                item[0]
                + "\t"
                + str(self.proclengths.get(item[0], -1))
                + " events to analyse"
            )
        print()

    def kill_all_procs(self):
        self.p.kill_all()
        print("All processes killed")

    def bayes(self, *taglist):
        taglist = list(taglist)
        for tag in taglist:
            try:
                path, chaindir, es, sfrequency, peakfindersettings = self.bayes_prep(
                    tag, write=True, mode=self.mode
                )
            except BayesError:
                continue
            if self.mode == "folding":
                self.p.add_process(
                    self.bayes_folding, (path, chaindir, es, sfrequency), tag
                )
            elif self.mode == "peaks":
                self.p.add_process(
                    self.bayes_folding_peaks,
                    (path, chaindir, es, sfrequency, peakfindersettings),
                    tag,
                )

    def bayes_prep(self, tag, write=True, mode="folding"):
        if tag in self.p.active_procs() and write:
            print("A process for tag " + tag + " is already running")
            raise BayesError
        try:
            path = self.tag_eventspath(tag)
            es = EventSet(path + "events.hdf5", True)
        except (PathError, EventError):
            print("Cannot find " + tag + ", skipping file.")
            raise BayesError
        else:
            if write:
                if exists(path + "bayes_folding.hdf5"):
                    print(
                        '"' + path + 'bayes_folding.hdf5" already exists, overwriting'
                    )
                #                    inp = input('"' + path + 'bayes_folding.hdf5" already exists, overwrite? (y/n) ')
                #                    if inp == 'y':
                #                        remove(path + 'bayes_folding.hdf5')
                #                    else:
                #                        raise BayesError
                else:
                    pass
                chaindir = join(self.nsbasedir, "chains", tag, "")
            else:
                chaindir = join(self.nsbasedir, "chains", "preview", "")

            if exists(chaindir):
                rmtree(chaindir)
            makedirs(chaindir)
            self.proclengths[tag] = es.noevents
            sfrequency = read_config()["Eventanalyser"]["Sampling frequency (Hz)"]
            if mode == "folding":
                return path, chaindir, es, sfrequency, None
            elif mode == "peaks":
                try:
                    peakfindersettings = imprt(path + "peakfinder_settings.txt")[
                        "peaks"
                    ]
                except FileNotFoundError:
                    print(
                        "Cannot find peakfinder_settings.txt, check events.hdf5 directory"
                    )
                    raise BayesError
                else:
                    return path, chaindir, es, sfrequency, peakfindersettings

    def bayes_folding(self, queue, outpath, chaindir, es, sfrequency):
        df = es.get_dataframe(dsnames=["current_nA"], attributes=["eventno", "rms_nA"])
        porechar = es.porechar
        es.close()
        settings = read_config()["Bayes nested sampling"]["Algorithm settings"]
        settings["outputfiles_basename"] = join(chaindir, "result_")
        l1 = porechar["levels"][1, 0]
        if porechar["levels"][2, 1] > 0:
            l2 = porechar["levels"][2, 0]
        else:
            l2 = 1.75 * porechar["levels"][1, 0]
        bayrun = BayesRun(**settings)
        noise = df["rms_nA"].mean()
        bayfuncs = BayesFuncs(level1=l1, level2=l2, noise=noise)
        bayfuncnames = {
            "A": (
                (bayfuncs.OneEvent_base_, bayfuncs.OneEvent_, bayfuncs.OneEvent_prior_),
                3,
            ),
            "B": (
                (
                    bayfuncs.TwoOneEvent_base_,
                    bayfuncs.TwoOneEvent_,
                    bayfuncs.TwoOneEvent_prior_,
                ),
                5,
            ),
        }
        out = h5.File(outpath + "bayes_folding.hdf5", "w")
        out["/"].attrs.create("noise_nA", noise)
        h5convert = lambda x: x if type(x) is int else np.array(str(x), dtype="S")
        for key, value in settings.items():
            out["/"].attrs.create("alg_settings_" + key, h5convert(value))
        length = df.index.size
        rootdsnames = {
            "eventno": (np.int, (length,)),
            "oneMinusTwoone_logz": (np.float, (length,)),
            "one_global_logz": (np.float, (length,)),
            "one_global_logzerr": (np.float, (length,)),
            "pcbelowthresh": (np.float, (length, 3)),
            "threshold": (np.float, (length, 3)),
            "twoone_global_logz": (np.float, (length,)),
            "twoone_global_logzerr": (np.float, (length,)),
            "one_parameters": (np.float, (length, bayfuncnames["A"][1])),
            "one_parameter_errors": (np.float, (length, bayfuncnames["A"][1])),
            "twoone_parameters": (np.float, (length, bayfuncnames["B"][1])),
            "twoone_parameter_errors": (np.float, (length, bayfuncnames["B"][1])),
        }
        for key, value in rootdsnames.items():
            ds = out.create_dataset(key, shape=value[1], dtype=value[0])
        try:
            i = 0
            for index, series in df.iterrows():
                try:
                    queue.get(False)
                except:
                    pass
                finally:
                    queue.put(index)
                grp = out.create_group(series["group"])
                out["eventno"][i] = int(series["eventno"])
                y = series["current_nA"][...]
                out["pcbelowthresh"][i], out["threshold"][i] = percent_below(
                    y, l1, l2, 1
                )
                bayfuncs.data = y
                bayfuncs.xvalues = np.arange(bayfuncs.data.size) / sfrequency * 1000.0
                bayfuncs.update()
                bayrun.run(*bayfuncnames["A"][0])
                for key, value in bayrun.result.items():
                    out["one_" + key][i] = value
                ds = grp.create_dataset(
                    "one_fit",
                    data=bayrun.lastrun_basefunc(
                        bayfuncs.xvalues, *bayrun.result["parameters"]
                    ),
                )
                grp.attrs.create(
                    "one_func", np.array(bayrun.lastrun_basefunc.__name__, dtype="S")
                )
                AlogZ = bayrun.result["global_logz"]
                bayrun.run(*bayfuncnames["B"][0])
                for key, value in bayrun.result.items():
                    out["twoone_" + key][i] = value
                ds = grp.create_dataset(
                    "twoone_fit",
                    data=bayrun.lastrun_basefunc(
                        bayfuncs.xvalues, *bayrun.result["parameters"]
                    ),
                )
                grp.attrs.create(
                    "twoone_func", np.array(bayrun.lastrun_basefunc.__name__, dtype="S")
                )
                BlogZ = bayrun.result["global_logz"]
                out["oneMinusTwoone_logz"][i] = AlogZ - BlogZ
                i += 1
        except Exception as p:
            print(p)
        finally:
            out.close()

    def bayes_folding_peaks(
        self, queue, outpath, chaindir, es, sfrequency, peakfsettings
    ):
        df = es.get_dataframe(dsnames=["current_nA"], attributes=["eventno", "rms_nA"])
        porechar = es.porechar
        es.close()
        settings = read_config()["Bayes nested sampling"]["Algorithm settings"]
        settings["outputfiles_basename"] = join(chaindir, "result_")
        l1 = porechar["levels"][1, 0]
        if porechar["levels"][2, 1] > 0:
            l2 = porechar["levels"][2, 0]
        else:
            l2 = 1.75 * porechar["levels"][1, 0]
        bayrun = BayesRun(**settings)
        noise = df["rms_nA"].mean()
        bayfuncs = BayesFuncs(level1=l1, level2=l2, noise=noise)
        bayfuncnames = {
            "A": (
                (bayfuncs.OneEvent_base_, bayfuncs.OneEvent_, bayfuncs.OneEvent_prior_),
                3,
            ),
            "B": (
                (
                    bayfuncs.TwoOneEvent_base_,
                    bayfuncs.TwoOneEvent_,
                    bayfuncs.TwoOneEvent_prior_,
                ),
                5,
            ),
        }
        out = h5.File(outpath + "bayes_folding.hdf5", "w")
        out["/"].attrs.create("noise_nA", noise)
        h5convert = lambda x: x if type(x) is int else np.array(str(x), dtype="S")
        for key, value in settings.items():
            out["/"].attrs.create("alg_settings_" + key, h5convert(value))
        for key, value in peakfsettings.items():
            out["/"].attrs.create("peakfinder_settings_" + key, value)
        maxpeakno = 150
        length = df.index.size
        rootdsnames = {
            "eventno": (np.int, (length,)),
            "oneMinusTwoone_logz": (np.float, (length,)),
            "one_global_logz": (np.float, (length,)),
            "one_global_logzerr": (np.float, (length,)),
            "pcbelowthresh": (np.float, (length, 3)),
            "threshold": (np.float, (length, 3)),
            "twoone_global_logz": (np.float, (length,)),
            "twoone_global_logzerr": (np.float, (length,)),
            "one_parameters": (np.float, (length, bayfuncnames["A"][1])),
            "one_parameter_errors": (np.float, (length, bayfuncnames["A"][1])),
            "twoone_parameters": (np.float, (length, bayfuncnames["B"][1])),
            "twoone_parameter_errors": (np.float, (length, bayfuncnames["B"][1])),
            "peak_inds": (np.int, (length, maxpeakno)),
            "peak_leftinds": (np.int, (length, maxpeakno)),
            "peak_rightinds": (np.int, (length, maxpeakno)),
            "peak_pos": (np.float, (length, maxpeakno)),
            "peak_mag": (np.float, (length, maxpeakno)),
        }
        for key, value in rootdsnames.items():
            ds = out.create_dataset(key, shape=value[1], dtype=value[0])
        try:
            i = 0
            for index, series in df.iterrows():
                try:
                    queue.get(False)
                except:
                    pass
                finally:
                    queue.put(index)
                grp = out.create_group(series["group"])
                out["eventno"][i] = int(series["eventno"])
                y = series["current_nA"][...]
                x = np.arange(y.size) / sfrequency * 1000.0
                (
                    peakinds,
                    left_valleyinds,
                    right_valleyinds,
                    magnitudes,
                    ind,
                    peakloc,
                ) = peakfinder.peakfinder(
                    x,
                    y,
                    peakfsettings.get("sel", 0.05),
                    peakfsettings.get("threshold", 0.0),
                    peakfsettings.get("extrema", -1),
                    peakfsettings.get("maxhrztlinddistance", 2),
                    peakfsettings.get("maxhrztldistance", 0.05),
                    peakfsettings.get("maxvertdistance", 0.2),
                )
                peakparams, peakdata, leftlist, rightlist, blines = peakfinder.peakmask(
                    peakfinder.gauss,
                    x,
                    y,
                    peakinds,
                    ind,
                    peakloc,
                    peakfsettings.get("masksize", 5),
                    peakfsettings.get("extrema", -1),
                )
                peakno = peakinds.size
                out["peak_inds"][i, 0:peakno] = peakinds[:maxpeakno]
                out["peak_leftinds"][i, 0:peakno] = np.array(leftlist)[:maxpeakno]
                out["peak_rightinds"][i, 0:peakno] = np.array(rightlist)[:maxpeakno]
                out["peak_mag"][i, 0:peakno] = magnitudes[:maxpeakno]
                out["peak_pos"][i, 0:peakno] = x[peakinds[:maxpeakno]]
                for k, arr in enumerate(peakdata):
                    y[leftlist[k] : rightlist[k] + 1] -= (
                        y[leftlist[k] : rightlist[k] + 1] - blines[k]
                    )
                out["pcbelowthresh"][i], out["threshold"][i] = percent_below(
                    y, l1, l2, 1
                )
                bayfuncs.data = y
                bayfuncs.xvalues = x
                bayfuncs.update()
                bayrun.run(*bayfuncnames["A"][0])
                for key, value in bayrun.result.items():
                    out["one_" + key][i] = value
                ds = grp.create_dataset(
                    "one_fit",
                    data=bayrun.lastrun_basefunc(
                        bayfuncs.xvalues, *bayrun.result["parameters"]
                    ),
                )
                grp.attrs.create(
                    "one_func", np.array(bayrun.lastrun_basefunc.__name__, dtype="S")
                )
                AlogZ = bayrun.result["global_logz"]
                bayrun.run(*bayfuncnames["B"][0])
                for key, value in bayrun.result.items():
                    out["twoone_" + key][i] = value
                ds = grp.create_dataset(
                    "twoone_fit",
                    data=bayrun.lastrun_basefunc(
                        bayfuncs.xvalues, *bayrun.result["parameters"]
                    ),
                )
                grp.attrs.create(
                    "twoone_func", np.array(bayrun.lastrun_basefunc.__name__, dtype="S")
                )
                BlogZ = bayrun.result["global_logz"]
                out["oneMinusTwoone_logz"][i] = AlogZ - BlogZ
                i += 1
        except Exception as p:
            print(p)
        finally:
            out.close()

    def process_finished(self, tag):
        print(str(datetime.now()) + ": \t" + tag + " has finished successfully")
        rmtree(join(self.nsbasedir, "chains", tag, ""))
        lis = [date.today(), self.sql.usn, True]
        outcome = self.sql.insertorupdate(
            read_config()["SQL"]["Analysis table"],
            ["ddatea", "whoa", "eventsanalysed"],
            lis,
            tag,
            (False, read_config()["SQL"]["Measurement table"]),
        )
        if outcome == -1:
            print("Logging back in")
            print("Success")
            self.sql.login(self.usn, self.pwd)
            self.sql.insertorupdate(
                read_config()["SQL"]["Analysis table"],
                ["ddatea", "whoa", "eventsanalysed"],
                lis,
                tag,
                (False, read_config()["SQL"]["Measurement table"]),
            )

    def delete_when_stuck(self, tag):
        print(str(datetime.now()) + ": \t" + tag + " has timed out and been restarted")


#        path = self.tag_eventspath(tag) + 'bayes_folding.hdf5'
#        remove(path)


class PathError(IOError):
    pass


class BayesError(Exception):
    pass


if __name__ == "__main__":
    e = EventAnalyser()
