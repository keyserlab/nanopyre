#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed May 10 12:29:07 2017

"""

from multiprocessing import Process, Queue, cpu_count
from queue import Empty
from concurrent.futures import ThreadPoolExecutor
from threading import RLock
from collections import deque
import time as t


class Parallels:
    def __init__(self, fiaction=None, tkaction=None):
        self.procs = deque()
        self.executor = ThreadPoolExecutor(max_workers=1)
        self.qs = deque()
        self.tabs = deque()
        self.lastmessage = deque()
        self.lock = RLock()
        self.stucklog = deque()

        self.queue = deque()
        self.log = {}
        self.maxprocs = cpu_count() - 1
        self.updatesleep = 10
        self.checksleep = 60
        self.fiaction = fiaction
        self.tkaction = tkaction
        self.watching = False

    #        self.watchman = self.executor.submit(self.watch)

    def add_process(self, target, args, tag):
        with self.lock:
            self.queue.appendleft((target, args, tag))
            self.log[tag] = (target, args, tag)
            if not self.watching:
                self.watchman = self.executor.submit(self.watch)

    def new_process(self, target, args, name=None):
        q = Queue(1)
        args = [x for x in args]
        args.insert(0, q)
        args = tuple(args)
        proc = Process(target=target, args=args, name=name)
        proc.start()
        self.procs.append(proc)
        self.qs.append(q)
        self.tabs.append(0)
        self.lastmessage.append(0)

    def start_pv_proc(self, target, args):
        with self.lock:
            self.pvproc = Process(target=target, args=args)
            self.pvproc.start()

    def stop_pv_proc(self):
        with self.lock:
            self.pvproc.terminate()

    def watch(self):
        self.watching = True
        timepassed = 0
        while self.procs or self.queue:
            with self.lock:
                droplist = []
                for i, proc in enumerate(self.procs):
                    try:
                        self.tabs[i] = self.qs[i].get(False)
                    except Empty:
                        pass
                    if not proc.is_alive():
                        droplist.append(i)
                        self.finished_clean(proc)
                if (timepassed % self.checksleep) == 0:
                    #                    print('Checking timeout...')
                    #                    print(self.procs)
                    #                    print(self.tabs)
                    #                    print(self.lastmessage)
                    for i, proc in enumerate(self.procs):
                        stuck = self.check_if_stuck(i)
                        if stuck and i not in droplist:
                            droplist.append(i)
                            self.queue.append(self.log[proc.name])
                            self.stucklog.append(proc.name)
                            self.timeout_kill(proc)
                #                            duration, getstuck, stickh = randomizer(proc.name)
                self.drop(droplist)
                while (len(self.procs) < self.maxprocs) and (len(self.queue) > 0):
                    item = self.queue.pop()
                    #                    print('adding from queue')
                    #                    print(item[2])
                    self.new_process(*item)
            t.sleep(self.updatesleep)
            timepassed += self.updatesleep
        else:
            print("Nothing left to watch")
            self.watching = False

    def check_if_stuck(self, i):
        #        try:
        #            last = self.qs[i].get(True, 0.25)
        #            self.tabs[i] = last
        #            return False
        #        except Empty:
        #            return True
        if self.tabs[i] == self.lastmessage[i]:
            return True
        else:
            self.lastmessage[i] = self.tabs[i]
            return False

    def drop(self, droplist):
        with self.lock:
            l = len(self.procs)
            rot = 0
            for i in sorted(droplist, reverse=True):
                n = l - i - 1 - rot
                self.procs.rotate(n)
                proc = self.procs.pop()
                proc.terminate()
                self.qs.rotate(n)
                q = self.qs.pop()
                q.close()
                self.tabs.rotate(n)
                self.tabs.pop()
                self.lastmessage.rotate(n)
                self.lastmessage.pop()
                rot += n
                l -= 1

    def kill_all(self):
        with self.lock:
            for proc in self.procs:
                proc.terminate()
            self.drop(list(range(len(self.procs))))
            self.queue = deque()

    def timeout_kill(self, proc):
        #        proc.terminate()
        if self.tkaction is not None:
            self.tkaction(proc.name)

    def finished_clean(self, proc):
        if self.fiaction is not None:
            self.fiaction(proc.name)

    def active_procs(self):
        with self.lock:
            return [proc.name for proc in self.procs]

    def get_info(self):
        with self.lock:
            proclist = []
            for i, proc in enumerate(self.procs):
                proclist.append((proc.name, self.tabs[i]))
            queuelist = []
            for item in self.queue:
                queuelist.append((item[2], 0))
            return proclist, queuelist
