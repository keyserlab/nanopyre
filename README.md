# NANOPYRE #

Data organisation and analysis for nanopore translocation measurements

**sqlconnector** - access to a cloud database to manage and synchronise measurement information

**translocationfinder** - identify events from raw current traces

**eventviewer** - interface to view and analyse identified events along with their parameters

**eventanalyser** - command-line tool for parallelised Bayesian analysis of events

## Installation ##

Written in Python3, see setup.py file for dependencies. Nanopyre has been tested on Windows and Linux, it should work on Mac, too, but you'll have to change the setup.py file accordingly.

**set up time:** 5-15 minutes
** requirements **: can be found in the requirements.txt

expected output will vary depending on the experiment and python code ran. Files which can be used for demo in relation to the associated project can be found in the repository described on the paper associated with the code. Feel free to contact the corresponding authors with questions in relation to running the code.

#### Windows ####

The below instructions show an installation from scratch, including Python3:

* Download the latest Python3 version from https://www.python.org/downloads/windows/
* Start the installation, choose "Customize installation"
* In "Optional Features", tick "pip" and "py launcher", click "Next"
* In "Advanced Options", tick "Associate files with Python" and "Precompile standard library", click "Install"
* Once the Python installation has finished, open the command line by typing "cmd" in the start menu
* Navigate to the nanopyre base directory containing the setup.py file and run "pip install .". This will install the nanopyre package into the currently active python environment, you can then use it with the "import nanopyre" statement. If you'd like your changes to the source files to be reflected in the package, install in developer mode with "pip install -e ."
* If "py launcher" has been installed, sqlconnector, translocationfinder and eventviewer can now opened by double-clicking the respective *_run.pyw files in the nanopyre base directory. You can place shortcuts to these files on the desktop or similar. Irrespective of "py launcher", "sqlconnector", "translocationfinder" and "eventviewer" are made availabe as commands to start the respective program from the command line.

##### Connect to the Azure SQL database #####
* Download and install the "Microsoft ODBC Driver for SQL Server" from https://www.microsoft.com/en-us/download/details.aspx?id=53339
* Open the "ODBC Data Source Administrator" by typing "data sources" in the start menu and selecting "Data Sources (ODBC)"
* Under "User DSN" or "System DSN", select "Add...", then "ODBC Driver 13 for SQL Server", then "Finish". In the "Name" field enter "nanopore" and in the "Server" 
field enter "nanopore.database.windows.net", click "Next". In the next window, check "With SQL Server authentication using a login ID and password entered by the 
user" and click "Next". In the following window, tick "Change the default database to:" and enter "nanoporedb" in the field. Leave the other options as they are and click "Next". Leave everything unchanged in the next window, click "Finish" and "Ok" in the last window. You should be good to go.

### Linux ###

* Assuming you have a functioning Python distribution, navigate to the nanopyre base directory containing the setup.py file and run "pip install .". This will install the nanopyre package and all dependencies into the currently active python environment, you can then use it with the "import nanopyre" statement. If you'd like your changes to the source files to be reflected in the package, install in developer mode with "pip install -e ."
* "sqlconnector", "translocationfinder" and "eventviewer" are made availabe as commands to start the respective program from the command line. The respective *_run.pyw files in the nanopyre directory do the same thing if executed.