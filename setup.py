#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Oct 23 11:40:28 2017


"""

from setuptools import setup, find_packages


setup(
    name="nanopyre",
    version="0.1.dev3",
    description="Data organisation and analysis for nanopore translocation measurements",
    # The project's main homepage.
    #    url='https://github.com/pypa/sampleproject',
    # Choose your license
    license="MIT",
    # See https://pypi.python.org/pypi?%3Aaction=list_classifiers
    classifiers=[
        # How mature is this project? Common values are
        #   3 - Alpha
        #   4 - Beta
        #   5 - Production/Stable
        "Development Status :: 3 - Alpha",
        # Pick your license as you wish (should match "license" above)
        "License :: OSI Approved :: MIT License",
        # Specify the Python versions you support here. In particular, ensure
        # that you indicate whether you support Python 2, Python 3 or both.
        "Programming Language :: Python :: 3",
    ],
    python_requires=">=3",
    # What does your project relate to?
    keywords="nanopores",
    # You can just specify the packages manually here if your project is
    # simple. Or you can use find_packages().
    packages=find_packages(),
    # List run-time dependencies here.  These will be installed by pip when
    # your project is installed. For an analysis of "install_requires" vs pip's
    # requirements files see:
    # https://packaging.python.org/en/latest/requirements.html
    install_requires=[
        "matplotlib>=2.1.0",
        "scipy",
        "numpy",
        "pandas",
        "scikit-learn",
        "h5py",
        "nptdms",
        "pyodbc",
        "tables",
        "PyQt5",
    ],
    # If there are data files included in your packages that need to be
    # installed, specify them here.  If using Python 2.6 or less, then these
    # have to be included in MANIFEST.in as well.
    package_data={"": ["config.txt", "conductivity.csv"]},
    include_package_data=True,
    # To provide executable scripts, use entry points in preference to the
    # "scripts" keyword. Entry points provide cross-platform support and allow
    # pip to create the appropriate form of executable for the target platform.
    entry_points={
        "gui_scripts": [
            "translocationfinder=nanopyre.gui.translocationfinder:run",
            "eventviewer=nanopyre.gui.eventviewer:run",
            "sqlconnector=nanopyre.gui.sqlconnector:run",
        ],
    },
)
